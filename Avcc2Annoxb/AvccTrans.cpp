#include "AvccTrans.h"
#include <iostream>
#include <WinSock2.h>
using namespace std;
#define NALU_TYPE_SLICE 1
#define NALU_TYPE_DPA 2
#define NALU_TYPE_DPB 3
#define NALU_TYPE_DPC 4
#define NALU_TYPE_IDR 5
#define NALU_TYPE_SEI 6
#define NALU_TYPE_SPS 7
#define NALU_TYPE_PPS 8
#define NALU_TYPE_AUD 9
#define NALU_TYPE_EOSEQ 10
#define NALU_TYPE_EOSTREAM 11
#define NALU_TYPE_FILL 12

#define SPS_CNT_MASK 0x1F
int AvccTrans::parse_nalu_hdr(uint8_t * nal_hdr, int nal_len)
{
    const char* type_str = NULL;
    H264Hdr* hdr = (H264Hdr*)nal_hdr;
    switch (hdr->nal_type)
    {
    case NALU_TYPE_SLICE:
        type_str = "P/B Slice";
        break;
    case NALU_TYPE_AUD:
        type_str = "AUD Slice";
        break;
    case NALU_TYPE_DPA:
        type_str = "DPA Slice";
        break;
    case NALU_TYPE_DPB:
        type_str = "DPB Slice";
        break;
    case NALU_TYPE_DPC:
        type_str = "DPC Slice";
        break;
    case NALU_TYPE_EOSEQ:
        type_str = "EOS seq Slice";
        break;
    case NALU_TYPE_EOSTREAM:
        type_str = "EOS stream Slice";
        break;
    case NALU_TYPE_FILL:
        type_str = "FILL Slice";
        break;
    case NALU_TYPE_IDR:
        type_str = "IDR Slice";
        break;
    case NALU_TYPE_PPS:
        type_str = "PPS Slice";
        break;
    case NALU_TYPE_SPS:
        type_str = "SPS Slice";
        break;
    case NALU_TYPE_SEI:
        type_str = "SEI Slice";
        break;
    default:
        type_str = "Unknow Slice";
        printf("Warn: nal type:%d\n", hdr->nal_type);
        break;
    }
    return 0;
}

int AvccTrans::parse_avcc_extra(char * buff, int size)
{
    uint8_t sps_cnt;
    uint16_t sps_size;
    uint8_t pps_cnt;
    uint16_t pps_size;
    uint8_t *offset = NULL;
    uint8_t extra_to_anexb[1024] = { 0x00, 0x00, 0x00, 0x01, 0x00 };

    if (!buff || (size < 7))
        return -1;

    AvccExtraHdr *hdr = (AvccExtraHdr *)buff;
    FILE *file_out = fopen("annexb.h264", "wb");
    if (!file_out) {
        printf("ERROR: open annexb.h264 failed!\n");
        return -1;
    }
    fseek(file_out, 0, SEEK_SET);

    offset = (uint8_t *)buff + sizeof(AvccExtraHdr);
    sps_cnt = *offset & SPS_CNT_MASK;
    offset++;
    printf("sps_cnt:%d\n", sps_cnt);
    for (int i = 0; i < sps_cnt; i++) {
        // 网络字节序转为主机字节序
        sps_size = ntohs(*((uint16_t *)offset));
        offset += 2;
        printf("+++ FLC-DBG: write anexb hdr to annexb.h264...\n");
        fwrite(extra_to_anexb, 1, 4, file_out);
        printf("+++ FLC-DBG: write sps:0x%02x to annexb.h264...\n", *offset);
        fwrite(offset, 1, sps_size, file_out);
        printf("[%d] sps_size:%d, sps_data:", i, sps_size);
        for (int j = 0; j < sps_size; j++)
            printf("0x%02x ", *(offset++));
        printf("\n");
    }
    pps_cnt = *(offset++);
    printf("pps_cnt:%d\n", pps_cnt);
    for (int i = 0; i < sps_cnt; i++) {
        // 网络字节序转为主机字节序
        pps_size = ntohs(*((uint16_t *)offset));
        offset += 2;
        printf("+++ FLC-DBG: write anexb hdr to annexb.h264...\n");
        fwrite(extra_to_anexb, 1, 4, file_out);
        printf("+++ FLC-DBG: write sps:0x%02x to annexb.h264...\n", *offset);
        fwrite(offset, 1, pps_size, file_out);
        printf("[%d] pps_size:%d, pps_data:", i, pps_size);
        for (int j = 0; j < pps_size; j++)
            printf("0x%02x ", *(offset++));
        printf("\n");
    }

    fclose(file_out);
    return 0;
}
