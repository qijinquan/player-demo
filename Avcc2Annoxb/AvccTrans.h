#pragma once
#include <cinttypes>
/*
H264帧由NALU头和NALU主体组成。
NALU头由一个字节组成,它的语法如下:

  +---------------+
  |0|1|2|3|4|5|6|7|
  +-+-+-+-+-+-+-+-+
  |F|NRI|  Type   |
  +---------------+

F: 1个比特.
  forbidden_zero_bit. 在 H.264 规范中规定了这一位必须为 0.

NRI: 2个比特.
  nal_ref_idc. 取00~11,似乎指示这个NALU的重要性,
  如00的NALU解码器可以丢弃它而不影响图像的回放,0～3,
  取值越大，表示当前NAL越重要，需要优先受到保护.
  如果当前NAL是属于参考帧的片，或是序列参数集，
  或是图像参数集这些重要的单位时，本句法元素必需大于0。

Type: 5个比特.
  nal_unit_type. 这个NALU单元的类型,1～12由H.264使用，
  24～31由H.264以外的应用使用.
 */
typedef struct {
    uint8_t nal_type : 5;
    uint8_t nal_ref_idc : 2;
    uint8_t forbidden_zero_bit : 1;
} H264Hdr;

/*
 bits:
    8   version ( always 0x01 )
    8   avc profile ( sps[0][1] )
    8   avc compatibility ( sps[0][2] )
    8   avc level ( sps[0][3] )
    6   reserved ( all bits on )
    2   NALULengthSizeMinusOne  // 这个值是（前缀长度-1），值如果是3，那前缀就是4，因为4-1=3
    3   reserved ( all bits on )
    5   number of SPS NALUs (usually 1)
repeated once per SPS:
    16     SPS size
    variable   SPS NALU data
    8   number of PPS NALUs (usually 1)
repeated once per PPS
    16    PPS size
    variable PPS NALU data
 */

typedef struct {
    uint8_t version;
    uint8_t avc_profile;
    uint8_t avc_compatibility;
    uint8_t avc_level;
    uint8_t reserved0 : 6;
    uint8_t nalu_len : 2;
} AvccExtraHdr;

class AvccTrans
{
public:
    AvccTrans() = default;
    ~AvccTrans() = default;
    int parse_nalu_hdr(uint8_t* nal_hdr, int nal_len);
    int parse_avcc_extra(char* buff, int size);
};

