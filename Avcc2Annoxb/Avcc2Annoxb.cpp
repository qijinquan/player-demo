﻿// Avcc2Annoxb.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "AvccTrans.h"
#include <cinttypes>
#include <WinSock2.h>
using namespace std;
#define PARASE_MODE_EXTRA 1
#define PARASE_MODE_STREAM 2
int main(int argc, char *argv[])
{
    int mode = 0;
    if (argc < 3) {
        cout << "ERROR: Invalid args\n" << endl;
        printf("Usage: \n\t1.%s -extra avcc_extra.bin\n", argv[0]);
        printf("\t2.%s -stream avcc_stream.bin\n", argv[0]);
        return -1;
    }
    if (!strcmp(argv[1], "-extra"))
        mode = PARASE_MODE_EXTRA;
    else if (!strcmp(argv[1], "-stream"))
        mode = PARASE_MODE_STREAM;
    else {
        printf("ERROR: Mode invalid\n");
        return -1;
    }
    AvccTrans trans;
    printf("# InFile:%s\n", argv[2]);
    FILE *file = fopen(argv[2], "rb");
    if (!file) {
        printf("ERROR: open %s failed!\n", argv[2]);
        return -1;
    }

    fseek(file, 0, SEEK_END);
    int flen = ftell(file);
    if (flen <= 0) {
        printf("ERROR: file length invalid! flen:%d\n", flen);
        fclose(file);
        return -1;
    }

    uint8_t *buffer = (uint8_t *)malloc(flen + 1);
    if (!buffer) {
        printf("ERROR: malloc %d Bytes failed!\n", flen);
        fclose(file);
        return -1;
    }

    fseek(file, 0, SEEK_SET);
    int ret = fread(buffer, 1, flen, file);
    if (ret <= 0) {
        printf("ERROR: read file %d Bytes error!\n", flen);
        fclose(file);
        free(buffer);
        return -1;
    }
    fclose(file);

    if (mode == PARASE_MODE_EXTRA) {
        ret = trans.parse_avcc_extra((char*)buffer, ret);
        if (ret < 0)
            printf("ERROR: parase avcc data failed!\n");
    }
    else {
        printf("#### PARASE AVCC STREAM ####\n");
        // 解析avcc h264码流，长度默认：4字节，可根据extra data的nalu_len来计算。
        int offset = 0;
        int nal_len = 0;
        while (offset < flen) {
            nal_len = ntohl(*((uint32_t *)(buffer + offset)));
            // 将avcc的4字节长度转换为annex b的“00 00 00 01”分隔符。
            //TODO: 转化中只是将nalu len转化成了start code, 没有将数据中的0x00 00 xx转化成0x00 00 30 xx， 但不影响播放
            *(buffer + offset) = 0;
            *(buffer + offset + 1) = 0;
            *(buffer + offset + 2) = 0;
            *(buffer + offset + 3) = 1;
            offset += 4;
            trans.parse_nalu_hdr((uint8_t *)(buffer + offset), nal_len);
            offset += nal_len;
        }
        // 追加形式打开文件，保存转换后的Stream。
        FILE *file_out = fopen("annexb.h264", "a+b");
        if (!file_out) {
            printf("ERROR: open annexb.h264 failed!\n");
            fclose(file);
            free(buffer);
            return -1;
        }

        fwrite(buffer, 1, flen, file_out);
        fclose(file_out);
    }

    free(buffer);
    std::cout << "Hello World!\n";
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
