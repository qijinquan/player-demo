#pragma once
#include <queue>
#include <memory>
#include <condition_variable>
#include <mutex>
#include "timesync.h"
extern "C"
{
    #include <libavutil/frame.h>
    #include <libavcodec/avcodec.h>
    #include <SDL/SDL.h>
}
class AudioPlay
{
public:
    AudioPlay(TimeSync::Ptr timeSync);
    void Push(std::shared_ptr<AVFrame> frame);
    void Init(std::shared_ptr<AVCodecContext> ctx);
    static void ReadAudio(void *userdata, Uint8 * stream, int len);
protected:
    std::shared_ptr<AVFrame> Pop();
    int MixAudioFrame(Uint8* stream, int pos, int len);
private:
    std::queue<std::shared_ptr<AVFrame>> frames_;
    std::mutex frameMutex_;
    std::condition_variable frameSignal_;
    std::shared_ptr<AVCodecContext> ctx_;
    std::shared_ptr<AVFrame> curFrame_;
    uint32_t curReadSize = 0;
    TimeSync::Ptr timeSync_;
    uint32_t audioHwBufSize = 0;
    int freq_ = 0; 
    int bytesPerSec_ = 0;
};

