#include "TimeSync.h"

void TimeSync::SetTimeStamp(uint64_t timestamp)
{
    std::unique_lock<std::shared_mutex> lk(mutex_);
    timestamp_ = timestamp;
}

uint64_t TimeSync::GetTimeStamp()
{
    std::shared_lock<std::shared_mutex> lk(mutex_);
    return timestamp_;
}
