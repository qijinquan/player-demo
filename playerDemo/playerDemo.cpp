﻿// playerDemo.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <cstdio>
#include <fstream>
#include <cstdint>
#include<Winsock2.h>
#include "TimeSync.h"
#include "audioplay.h"
#define PIX_FORMAT AV_PIX_FMT_YUV420P //AV_PIX_FMT_BGR24//AV_PIX_FMT_RGBA

std::fstream fio;

extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libpostproc/postprocess.h>
#include <libavfilter/avfilter.h>
#include <SDL/SDL.h>
}

std::shared_ptr<AudioPlay> audioPlayer = nullptr;
SDL_Window* window;
SDL_Renderer* renderer;
SDL_Texture* texture;

static int MAX_AUDIO_FRAME_SIZE = 192000;
int interrupt_callback(void* data)
{
    return false;
}
enum class MetaType
{
    META_VIDEO,
    META_AUDIO,
    META_INVALID,
};
class MetaData {
public:
    MetaData(std::shared_ptr<AVFormatContext>& ic, std::shared_ptr<AVCodecContext>& ctx, int index, const MetaType& type, TimeSync::Ptr timeSync) :ic_(ic), ctx_(ctx), index_(index), type_(type), timeSync_(std::move(timeSync))
    {
        thread = std::thread([](void* data) {
            MetaData* meta = (MetaData*)data;
            meta->decoderRun();
        }, this);
    }
    ~MetaData(){}

    void InsertPkt(std::shared_ptr<AVPacket>& pkt)
    {
        std::unique_lock<std::mutex> lk(mutex);
        pktQueue.emplace(pkt);
        sign_.notify_one();
    }
    
    void config_audio_filter()
    {
        AVFilterContext *filt_asrc = NULL, *filt_asink = NULL;
        
        av_get_channel_layout_nb_channels(ctx_->channel_layout);
        ctx_->sample_fmt;
        static const enum AVSampleFormat sample_fmts[] = { AV_SAMPLE_FMT_S16 , AV_SAMPLE_FMT_NONE };
        int sample_rates[] = { 0,-1 };
        int64_t channel_layouts[] = { 0, -1 };
        int channels[] = { 0, -1 };
        AVFilterGraph * graph = avfilter_graph_alloc();
        char asrc_args[256] = { 0 };
        int ret = snprintf(asrc_args, sizeof(asrc_args),
            "sample_rate=%d:sample_fmt=%s:channels=%d:time_base=%d/%d:channel_layout=0x%" PRIx64,
            ctx_->sample_rate, av_get_sample_fmt_name(ctx_->sample_fmt),
            ctx_->channels, 1, ctx_->sample_rate, ctx_->channel_layout);

        ret = avfilter_graph_create_filter(&filt_asrc,
            avfilter_get_by_name("abuffer"), "my_abuffer",
            asrc_args, NULL, graph);

        ret = avfilter_graph_create_filter(&filt_asink,
            avfilter_get_by_name("abuffersink"), "my_abuffersink",
            NULL, NULL, graph);

    }
    void decoderRun()
    {
        if (type_ == MetaType::META_AUDIO)
        {
            // config_audio_filter();
        }
        while (true)
        {
            {
                std::unique_lock<std::mutex> lk(mutex);
                sign_.wait(lk, [this] {return pktQueue.size() > 0; });
            }
            while (pktQueue.size() > 0)
            {
                std::shared_ptr<AVPacket> pkt = pktQueue.front();
                if (!pkt)
                {
                    continue;
                }
                //std::cout << "pkt:stream:" << pkt->stream_index << ",pts:" << pkt->pts << ",dts:" << pkt->dts << std::endl;
                pktQueue.pop();
                if (type_ == MetaType::META_VIDEO)
                {
                    // for h264
                    /*
                    static std::once_flag once;
                    std::call_once(once, [&]() {
                        fio.open("avcc.h264", std::ios::binary | std::ios::trunc | std::ios::out);
                        bool isOpen = fio.is_open();
                        int extraSize = ntohl(ctx_->extradata_size);
                        fio.write((char*)&extraSize, sizeof(int));
                        fio.write((char*)ctx_->extradata, ctx_->extradata_size);
                        fio.flush();
                    });
                    int size = ntohl(pkt->size);
                    fio.write((char*)&size, sizeof(int));
                    fio.write((char*)pkt->data, pkt->size);
                    fio.flush();
                    */

                    /*
                    static std::once_flag once;
                    std::call_once(once, [&]() {
                        fio.open("test.vp9", std::ios::binary | std::ios::trunc | std::ios::out);
                    });
                    int size = ntohl(pkt->size);
                    fio.write((char*)&size, sizeof(int));
                    fio.write((char*)pkt->data, pkt->size);
                    fio.flush();
                    */
                }
                auto err = avcodec_send_packet(ctx_.get(), pkt.get());
                if (err != AVERROR(EAGAIN)) {
                    // 每次申请新的frame， 可以修改下
                    std::shared_ptr<AVFrame> frame(av_frame_alloc(), [](AVFrame* frame) { av_frame_free(&frame); });
                    err = avcodec_receive_frame(ctx_.get(), frame.get());
                    if (err != 0) {
                        continue;
                    }
                    if (type_ == MetaType::META_AUDIO)
                    {
                        //std::cout << "AUDIO:";
                        // TODO: 先打开设备，看设备支持的格式，然后再考虑swr_convert等
                        uint64_t outChannelLayout = AV_CH_LAYOUT_STEREO;
                        int outNbSamples = ctx_->frame_size;
                        int outSampleRate = ctx_->sample_rate;
                        int outChannels = av_get_channel_layout_nb_channels(outChannelLayout);
                        int outBufferSize = av_samples_get_buffer_size(NULL, outChannels, outNbSamples, AV_SAMPLE_FMT_FLT, 1);
                        uint8_t *outBuffer = (uint8_t*)av_malloc(outBufferSize);
                        int64_t inChannelLayout = av_get_default_channel_layout(ctx_->channels);
                        //申请2倍空间
                        std::shared_ptr<AVFrame> audioFrame(av_frame_alloc(), [](AVFrame* frame) { av_frame_free(&frame); });
                        av_samples_fill_arrays(audioFrame->data, audioFrame->linesize, outBuffer, outChannels, outNbSamples, AV_SAMPLE_FMT_FLT, 1);
                        static SwrContext * swrContext = nullptr;
                        if (swrContext == nullptr)
                        {
                            swrContext = swr_alloc();
                            swrContext = swr_alloc_set_opts(swrContext, outChannelLayout, AV_SAMPLE_FMT_FLT, outSampleRate, inChannelLayout,
                                ctx_->sample_fmt, ctx_->sample_rate, 0, NULL);
                            swr_init(swrContext);
                        }
                        auto samples = swr_convert(swrContext, &outBuffer, MAX_AUDIO_FRAME_SIZE, (const uint8_t**)frame->data, frame->nb_samples);
                        // 保存音频pts和时基
                        audioFrame->pts = frame->pts;
                        audioFrame->time_base = ctx_->pkt_timebase;
                        if (!audioPlayer)
                        {
                            config_audio_filter();
                            audioPlayer = std::make_shared<AudioPlay>(timeSync_);
                            audioPlayer->Init(ctx_);
                        }

                        if (audioPlayer)
                        {
                            audioPlayer->Push(audioFrame);
                        }
                        
                    }
                    else
                    {   
                        
                        static std::once_flag once;
                        std::call_once(once, [this]{
                            window = SDL_CreateWindow("Player", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, ctx_->width, ctx_->height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
                            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
                            texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_YV12, SDL_TEXTUREACCESS_STREAMING, ctx_->width, ctx_->height);
                        });
                        
                        static SwsContext* swsCtx = nullptr;
                        swsCtx = sws_getCachedContext(swsCtx, frame->width, frame->height, ctx_->pix_fmt, frame->width, frame->height, PIX_FORMAT, SWS_X, nullptr, nullptr, nullptr);
                        static AVFrame* pFrameYUV = nullptr;
                        if (!pFrameYUV)
                        {
                            pFrameYUV = av_frame_alloc();
                            int numBytes = av_image_get_buffer_size(PIX_FORMAT, ctx_->width, ctx_->height,1);
                            uint8_t* buffer = (uint8_t*)av_malloc(numBytes * sizeof(uint8_t));
                            // Assign appropriate parts of buffer to image planes in pFrameRGB
                            av_image_fill_arrays(pFrameYUV->data, pFrameYUV->linesize, buffer, PIX_FORMAT, ctx_->width, ctx_->height,1);
                        }
                        int height = sws_scale(swsCtx, frame->data, frame->linesize, 0, ctx_->height, pFrameYUV->data, pFrameYUV->linesize);
                        uint64_t curTime = 1000 * av_q2d(ctx_->pkt_timebase) * frame->pts;
                        static uint64_t difTime = 20;//1000 * av_q2d(ctx_->time_base) * 2;
                        auto frameDelay = av_q2d(ctx_->time_base);
                        frameDelay += frame->repeat_pict *(frameDelay*0.5);
                        uint64_t timeSleep = 0;
                        // 音视频同步check， 实际效果反了
                        // 音频慢了
                        auto audioTime = timeSync_->GetTimeStamp();
                        /*
                        if (audioTime < curTime)
                        {
                            timeSleep = curTime - audioTime;
                            std::cout << " sleep :" << timeSleep<<std::endl;
                        }
                        // 音频快了,快得比较多，这时候不等待
                        else if (audioTime > curTime + difTime)
                        {
                            std::cout << " 2" << "\t";
                            timeSleep = 0;
                        }
                        // 音频快了， 快得不多
                        else {
                            std::cout << " 3" << "\t";
                            timeSleep = audioTime - curTime;
                        }*/
#define AV_SYNC_THRESHOLD 0.01 // 同步最小阈值
                        frameDelay *= 1000;
                        int32_t sync_threshold = int32_t((frameDelay > AV_SYNC_THRESHOLD*1000) ? frameDelay : AV_SYNC_THRESHOLD*1000);
                        int32_t delay = audioTime - curTime;
                        
                        if (delay > sync_threshold)
                        {
                            frameDelay = frameDelay * 2;
                        }
                        else
                        {
                            frameDelay  = 0;
                        }
                        std::cout << "delay :" << delay<<" frameDelay"<< frameDelay << std::endl;
                        SDL_Rect rect;
                        rect.x = 0;
                        rect.y = 0; 
                        rect.w = frame->width;
                        rect.h = frame->height;
                        SDL_UpdateYUVTexture(texture, &rect, pFrameYUV->data[0], pFrameYUV->linesize[0], pFrameYUV->data[1], pFrameYUV->linesize[1], pFrameYUV->data[2], pFrameYUV->linesize[2]);
                        SDL_RenderClear(renderer);
                        SDL_RenderCopy(renderer, texture, &rect, &rect);
                        SDL_RenderPresent(renderer);
                        static std::once_flag once1;
                        std::call_once(once1, [&]() {
                            fio.open("test.yuv", std::ios::binary | std::ios::trunc | std::ios::out);
                        });
                        int size = ntohl(pkt->size);
 
                        fio.write((const char*)pFrameYUV->data[0], pFrameYUV->linesize[0]* frame->height);
                        for (size_t i = 0; i < frame->height/2; i++)
                        {
                            for (size_t j = 0; j < pFrameYUV->linesize[1]; j++)
                            {
                                fio.write((const char*)pFrameYUV->data[2] + i * pFrameYUV->linesize[1] + j, 1);
                                fio.write((const char*)pFrameYUV->data[1] + i * pFrameYUV->linesize[1] + j, 1);
                            }
                            
                        }
                        fio.flush();
                        //Sleep(frameDelay);
                        
                    }
                    //std::cout << "err:" << err << ",frame: pts:" << frame->pts << std::endl;
                }
                else
                {
                    std::cout << "err:" << err << std::endl;
                }
            }
        }
    }


    void Exit() {
        thread.join();
    }
private:
    std::shared_ptr<AVFormatContext> ic_;
    std::shared_ptr<AVCodecContext> ctx_;
    int index_;
    std::queue<std::shared_ptr<AVPacket>> pktQueue;
    std::queue< std::shared_ptr<AVFrame>> frameQueue;
    std::mutex mutex;
    std::condition_variable sign_;
    std::thread thread;
    MetaType type_;
    TimeSync::Ptr timeSync_;
};

// SDL main defined main again, so we need undef it first
#ifdef main
#undef main
#endif //main

int main()
{
    std::string input = "D:\\OMX_IL_rk3568_ohos_so\\video\\H264_AAC.mp4";
    //std::string input = "D:\\OMX_IL_rk3568_ohos_so\\video\\vp8_test.mkv";
    //std::string input = "shuimenqiao.mp4";
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    avformat_network_init();
    AVFormatContext* temp = avformat_alloc_context();
    std::shared_ptr<AVFormatContext> ic(temp, avformat_free_context);
    if (ic == nullptr)
    {
        std::cout << "avformat_alloc_context failed" << std::endl;
        return -1;
    }
    //可以指定callback
    ic->interrupt_callback.callback = interrupt_callback;
    ic->interrupt_callback.opaque = 0;
    AVDictionary* format_opts = nullptr;
    av_dict_set(&format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);
    char buf[1024] = { 0 };
    
    auto err = avformat_open_input(&temp, input.c_str(), NULL, &format_opts);
    if (0 != err) {
        av_strerror(err, buf, 1024);
        std::cout << "avformat_open_input:" << buf<< std::endl;
        return -1;
    }
    err = avformat_find_stream_info(ic.get(), nullptr);
    if (err < 0)
    {
        std::cout << "avformat_find_stream_info:"<<err << std::endl;
        return -1;
    }
    int videoIndex = -1;
    int autioIndex = -1;
    av_dump_format(temp, 0, input.c_str(), 0);
    std::shared_ptr<MetaData> video = nullptr;// std::make_shared<MetaData>(ic, videoIndex);
    std::shared_ptr<MetaData> audio = nullptr;// std::make_shared<MetaData>(ic, autioIndex);
    auto timeSync = std::make_shared<TimeSync>();
    for (unsigned int i = 0; i < ic->nb_streams; i++)
    {
        AVStream* st = ic->streams[i];
        if (st->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoIndex = i;
            AVCodecContext* ctx = avcodec_alloc_context3(nullptr);
            std::shared_ptr<AVCodecContext> avctx(ctx, [](AVCodecContext* ctx) {avcodec_free_context(&ctx); });
            const AVCodec *codec;
            auto ret = avcodec_parameters_to_context(avctx.get(), ic->streams[videoIndex]->codecpar);
            codec = avcodec_find_decoder(avctx->codec_id);
            avctx->pkt_timebase = ic->streams[videoIndex]->time_base;
            avcodec_open2(avctx.get(), codec, nullptr);
            video = std::make_shared<MetaData>(ic, avctx, videoIndex, MetaType::META_VIDEO, timeSync);
        }
        else if (st->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            autioIndex = i;
            
            AVCodecContext* ctx = avcodec_alloc_context3(nullptr);
            std::shared_ptr<AVCodecContext> avctx(ctx, [](AVCodecContext* ctx) {avcodec_free_context(&ctx); });
            const AVCodec *codec;
            auto ret = avcodec_parameters_to_context(avctx.get(), ic->streams[autioIndex]->codecpar);
            avctx->pkt_timebase = ic->streams[autioIndex]->time_base;
            codec = avcodec_find_decoder(avctx->codec_id);
            avcodec_open2(avctx.get(), codec, nullptr);
            //audio = std::make_shared<MetaData>(ic, avctx, autioIndex, MetaType::META_AUDIO, timeSync);
            
        }
    }

    
    int ret = 0;
    do
    {
        auto pkt = std::shared_ptr<AVPacket>(av_packet_alloc(), av_packet_unref);

        ret = av_read_frame(ic.get(), pkt.get());
        if (ret < 0)
        {
            std::cout << "frame read failed" << std::endl;
            break;
        }

        if (pkt->stream_index == videoIndex)
        {
            video->InsertPkt(pkt);
        }
        else if (pkt->stream_index == autioIndex && audio != nullptr)
        {
            audio->InsertPkt(pkt);
        }

    } while (true);
    
    //MetaData autio = {.ic = ic, .st = ic->streams[autioIndex], .stream_index = autioIndex};
    auto t1 = std::chrono::system_clock::now();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    auto t2 = std::chrono::system_clock::now();
    std::chrono::duration<double> dif = t2 - t1;
    
    auto t = dif.count();
    std::cout << "file:" << __FILE__ <<":"<<__LINE__ << ":" << __DATE__ << ":" << __TIME__ << std::endl;
    video->Exit();
    audio->Exit();
    std::cout << "Hello World!\n";
}
