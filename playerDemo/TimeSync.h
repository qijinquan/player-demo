#pragma once
#include <memory>
#include <shared_mutex>
class TimeSync
{
public:
using Ptr = std::shared_ptr<TimeSync>;
TimeSync()= default;
~TimeSync() = default;
void SetTimeStamp(uint64_t timestamp);
uint64_t GetTimeStamp();
private:
   std::shared_mutex mutex_;
   uint64_t timestamp_ = 0;
};

