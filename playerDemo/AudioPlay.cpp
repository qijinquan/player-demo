#include "AudioPlay.h"
#include <iostream>
extern "C"
{
    #include <libavutil/time.h>
}
using namespace std;
AudioPlay::AudioPlay(TimeSync::Ptr timeSync)
{
    timeSync_ = std::move(timeSync);
}
void AudioPlay::Push(std::shared_ptr<AVFrame> frame)
{
    lock_guard<mutex> lk(frameMutex_);
    frames_.push(std::move(frame));
}

void AudioPlay::Init(std::shared_ptr<AVCodecContext> ctx)
{
    ctx_ = ctx;
    SDL_AudioSpec spec;
    SDL_AudioSpec obtained;
    spec.freq = ctx->sample_rate;
    spec.format = AUDIO_F32SYS;//S16SYS?
    spec.silence = 0;
    spec.samples = 1024*4*10;
    spec.userdata = this;
    spec.channels = 2;
    spec.callback = (AudioPlay::ReadAudio);
    auto err = SDL_OpenAudio(&spec, &obtained);
    if (err != 0)
    {
        cout << "err = " << SDL_GetError() << std::endl;
    }
    audioHwBufSize = obtained.size;
    freq_ = obtained.freq;
    bytesPerSec_ = av_samples_get_buffer_size(NULL, 2, freq_, AV_SAMPLE_FMT_S32, 1);
    SDL_PauseAudio(0);
}
void AudioPlay::ReadAudio(void *userdata, Uint8 * stream, int len)
{
    AudioPlay* player = (AudioPlay*)userdata;
    SDL_memset(stream, 0, len);
    int filledLen = 0;
    while (filledLen < len)
    {
        filledLen += player->MixAudioFrame(stream, filledLen, len - filledLen);
    };
}

std::shared_ptr<AVFrame> AudioPlay::Pop()
{
    unique_lock<mutex> lk(frameMutex_);
    frameSignal_.wait(lk, [this]{return frames_.size() > 0;});
    if (frames_.size() > 0)
    {
        auto frame = frames_.front();
        frames_.pop();
        return frame;
    }
    return nullptr;
}

int AudioPlay::MixAudioFrame(Uint8 * stream, int pos, int len)
{
    int filled = 0;
    auto time = av_gettime_relative();
    if (curFrame_ == nullptr)
    {
        curFrame_ = AudioPlay::Pop();
        uint64_t timestamp = 1000*av_q2d(curFrame_->time_base)*curFrame_->pts;//单位ms
        timeSync_->SetTimeStamp(timestamp);
    }
    else
    {
        auto timeStamp = timeSync_->GetTimeStamp();
        timeStamp += len/bytesPerSec_ * 1000;//ms
        timeSync_->SetTimeStamp(timeStamp);
        // 缓冲区数据未填满时，怎么处理
    }
    if (curFrame_)
    {
        // SYS16， 占2字节
        filled = curFrame_->linesize[0] - curReadSize;
        // 当前读的位置
        uint8_t* data = curFrame_->data[0] + curReadSize;
        // 这一帧剩下的数据比较充裕
        if (filled > len)
        {
            filled = len;
            curReadSize += len;
            SDL_MixAudio(stream + pos, data, filled, 100);
        }
        else 
        {   
            // 这一帧数据读完了，直接结束，等下次继续
            SDL_MixAudio(stream + pos, data, filled, 100);
            curFrame_ = nullptr;
            curReadSize = 0;
        }
        
    }
    return filled;
}


