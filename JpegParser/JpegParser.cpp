﻿// JpegParser.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <fstream>
#include <sstream>
bool bigEnd = true;
using namespace std;
typedef struct{
    uint16_t align;
    uint16_t tag;
    uint32_t offsetIFD0; // MM 时是网络序
}TiffHeader;
typedef struct{
    uint16_t exifTag;
    uint16_t ComponentType;// 转序
    uint32_t ComponentNum; //转序
    uint32_t dataLen;
    const uint8_t* data;

}DirectoryEntry;
/*         TAG ID           type                 GROUP                      values**/
enum ExifTag
{
    TAG_INTEROP_IDNEX = 0x1,//string            InteropIFD          "R03","R09","THM"

    TAG_MAKE   = 0x010f,    //string            IFD0
    TAG_MODEL = 0x0110,               //string            IFD0
    TAG_STRIP_OFFSETS = 0x0111,
    TAG_ORIENTATION = 0x0112,

    TAG_SAMPLES_PER_PIXEL = 0x0115,//int16u      IFD0
    TAG_ROWS_PER_STRIP = 0x0116,    //int32u     IFD0
    TAG_STRIP_BYTE_COUNTS = 0x0117,
};
//UB, AS, US, UL, UR, SB, Und, SS, SL, SR,SF, DF
uint16_t componentTypeLen[] = {0,1,1,3,4,8,1,1,2,3,8,4,8};
enum ComponentType : uint16_t
{
    TYPE_UNSIGNED_BYTE =1,
    TYPE_ASSCII_STRING,
    TYPE_UNSIGNED_SHORT,
    TYPE_UNSIGNED_LONG,
    TYPE_UNSIGNED_RATIONAL, //前4字节为分子，后4字节为分母
    TYPE_SIGNED_BYTE,
    TYPE_UNDEFINE,
    TYPE_SIGNED_SHORT,
    TYPE_SIGNED_LONG,
    TYPE_SIGNED_RATIONAL, //前4字节为分子，后4字节为分母
    TYPE_SINGLE_FLOAT,
    TYPE_DOUBLE_FLOAT,
};
enum JpegMarker {
    /* start of frame */
    SOF0 = 0xc0, /* baseline */
    SOF1 = 0xc1, /* extended sequential, huffman */
    SOF2 = 0xc2, /* progressive, huffman */
    SOF3 = 0xc3, /* lossless, huffman */

    SOF5 = 0xc5,  /* differential sequential, huffman */
    SOF6 = 0xc6,  /* differential progressive, huffman */
    SOF7 = 0xc7,  /* differential lossless, huffman */
    JPG = 0xc8,   /* reserved for JPEG extension */
    SOF9 = 0xc9,  /* extended sequential, arithmetic */
    SOF10 = 0xca, /* progressive, arithmetic */
    SOF11 = 0xcb, /* lossless, arithmetic */

    SOF13 = 0xcd, /* differential sequential, arithmetic */
    SOF14 = 0xce, /* differential progressive, arithmetic */
    SOF15 = 0xcf, /* differential lossless, arithmetic */

    DHT = 0xc4, /* define huffman tables */

    DAC = 0xcc, /* define arithmetic-coding conditioning */

    /* restart with modulo 8 count "m" */
    RST0 = 0xd0,
    RST1 = 0xd1,
    RST2 = 0xd2,
    RST3 = 0xd3,
    RST4 = 0xd4,
    RST5 = 0xd5,
    RST6 = 0xd6,
    RST7 = 0xd7,

    SOI = 0xd8, /* start of image */
    EOI = 0xd9, /* end of image */
    SOS = 0xda, /* start of scan */
    DQT = 0xdb, /* define quantization tables */
    DNL = 0xdc, /* define number of lines */
    DRI = 0xdd, /* define restart interval */
    DHP = 0xde, /* define hierarchical progression */
    EXP = 0xdf, /* expand reference components */

    APP0 = 0xe0,
    APP1 = 0xe1,
    APP2 = 0xe2,
    APP3 = 0xe3,
    APP4 = 0xe4,
    APP5 = 0xe5,
    APP6 = 0xe6,
    APP7 = 0xe7,
    APP8 = 0xe8,
    APP9 = 0xe9,
    APP10 = 0xea,
    APP11 = 0xeb,
    APP12 = 0xec,
    APP13 = 0xed,
    APP14 = 0xee,
    APP15 = 0xef,

    JPG0 = 0xf0,
    JPG1 = 0xf1,
    JPG2 = 0xf2,
    JPG3 = 0xf3,
    JPG4 = 0xf4,
    JPG5 = 0xf5,
    JPG6 = 0xf6,
    SOF48 = 0xf7,  ///< JPEG-LS
    LSE = 0xf8,    ///< JPEG-LS extension parameters
    JPG9 = 0xf9,
    JPG10 = 0xfa,
    JPG11 = 0xfb,
    JPG12 = 0xfc,
    JPG13 = 0xfd,

    COM = 0xfe, /* comment */

    TEM = 0x01, /* temporary private use for arithmetic coding */

    /* 0x02 -> 0xbf reserved */
};
static uint16_t GetUInt16(const uint8_t *&buf, bool bigEnd = true)
{
    if (bigEnd)
    {
        return (buf[0] << 8) | buf[1];
    }
    else
    {
        return (buf[1] << 8) | buf[0];
    }

}
static uint64_t GetUInt64(const uint8_t *&buf, bool bigEnd = true)
{
    if (bigEnd)
    {
        return (buf[7] << 56) | (buf[6] << 48) | (buf[5] << 40) | (buf[4] << 32) | (buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0];
    }
    else
    {
        return (buf[0] << 56) | (buf[1] << 48) | (buf[2] << 40) | (buf[3] << 32) | (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7];
    }
}
static double GetDouble(const uint8_t *&buf, bool bigEnd = true)
{
    auto val = GetUInt64(buf, bigEnd);
    double ret = 0;
    memcpy(&ret, &val, sizeof(val));
    return ret;
}
static uint32_t GetUInt32(const uint8_t *&buf, bool bigEnd = true)
{
    if (bigEnd)
    {
        return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
    }
    else
    {
        return (buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0];
    }
}
static float GetFloat(const uint8_t *&buf, bool bigEnd = true)
{
    float ret = 0;
    auto val = GetUInt32(buf, bigEnd);
    memcpy(&ret, &val, sizeof(float));
    return ret;
}

static uint32_t BigEnd(uint32_t val)
{
    uint8_t* data = (uint8_t*)&val;
    return (data[0] << 24) | (data[1]<<16) | (data[2]<<8) | data[3];
}
int32_t FindMarker(const uint8_t *&buf, const uint8_t *bufEnd)
{
    unsigned int v = 0, v2 = 0;
    int val = 0, skipped = 0;
    const uint8_t *bufPtr = buf;
    bool find = false;
    while (bufEnd - bufPtr > 1) {
        v = *bufPtr++;
        v2 = *bufPtr;
        if ((v == 0xff) && (v2 >= SOF0) && (v2 <= COM) && (bufPtr < bufEnd)) {
            val = *bufPtr++;
            find = true;
            break;
        }
        //  else if ((v == 0x89) && (v2 == 0x50)) {
        //     // may not png format
        // }
        skipped++;
    }
    if (find) {
        buf = bufPtr;
    }
    else {
        buf = bufEnd;
        val = -1;
    }
    return val;
}
bool DecodeSof(const uint8_t *&buf, uint32_t size, uint32_t &width, uint32_t &height)
{
    if (size <= 8) {
        cerr << "buf is too short, size = " << size << endl;
        return false;
    }
    const uint8_t *bufPtr = buf;
    uint16_t len = GetUInt16(bufPtr);
    bufPtr += 2;
    if (len > size) {
        cerr << "len is too large, len = " << len << endl;
        return false;
    }

    uint8_t bits = *bufPtr++;
    if (bits > 16 || bits < 1) {
        // bits == 8
        return false;
    }
    width = GetUInt16(bufPtr);
    bufPtr += 2;
    height = GetUInt16(bufPtr);
    bufPtr += 2;
    cout << "picture width:" << width << ", height:" << height << endl;
    uint8_t components = *bufPtr++;
    if ((components != 1) && (components != 3)) {
        return false;
    }
    if (len != (8 + (3 * components))) {
        return false;
    }
    buf += len;
    return true;
}

void EntryDataPrint(DirectoryEntry &entry)
{
    switch (entry.ComponentType)
    {
    case TYPE_UNSIGNED_BYTE:
    case TYPE_SIGNED_BYTE:
    {
        stringstream ss;
        ss << std::hex;
        for (size_t i = 0; i < entry.dataLen; i++)
        {
            ss << std::hex << entry.data[i];
        }
        cout << ss.str();
        break;
    }
    case TYPE_ASSCII_STRING:
    {
        string ss((char*)entry.data, entry.dataLen);
        cout << ss << endl;
        break;
    }
    case TYPE_UNSIGNED_SHORT:
    case TYPE_SIGNED_SHORT:
    {
        cout << GetUInt16(entry.data, bigEnd);
        break;
    }
    case TYPE_UNSIGNED_LONG:
    case TYPE_SIGNED_LONG:
    {
        cout << GetUInt32(entry.data, bigEnd);
        break;
    }
    case TYPE_UNSIGNED_RATIONAL:
    case TYPE_SIGNED_RATIONAL:
    {
        uint32_t den = GetUInt32(entry.data, bigEnd);
        entry.data += 4;
        uint32_t fen = GetUInt32(entry.data, bigEnd);
        cout << den << "/" << fen;
        break;
    }
    case TYPE_SINGLE_FLOAT:
    {
        cout << GetFloat(entry.data, bigEnd);
        break;
    }
    case TYPE_DOUBLE_FLOAT:
        cout << GetDouble(entry.data, bigEnd);
        break;
    default:
        break;
    }
    cout << endl;
}
bool PrintGPS(DirectoryEntry& entry)
{
    switch (entry.exifTag)
    {
    case 0x0:
        cout<<"GPSVersionID: ";
        break;
    case 0x1:
        cout << "GPSLatitudeRef: ";
        break;
    case 0x2:
        cout << "GPSLatitude: ";
        break;
    case 0x3:
        cout << "GPSLongitudeRef: ";
        break;
    case 0x4:
        cout << "GPSLongitude: ";
        break;
    case 0x5:
        cout << "GPSAltitudeRef: ";
        break;
    case 0x6:
        cout << "GPSAltitude: ";
        break;
    case 0x1b:
        cout << "GPSProcessingMethod: ";
        break;
    case 0x1d:
        cout << "GPSDateStamp: ";
        break;
    default:
        cout << "Unknown GPS " << hex << entry.exifTag << " ";
        break;
    }
    EntryDataPrint(entry);
    return true;
}
bool PrintEntry(DirectoryEntry& entry){
    static bool isGps = false;
    if (isGps)
    {
        return PrintGPS(entry);
    }
    switch (entry.exifTag)
    {
    case 0x1:
        cout<<"InteropIndex:";
        break;
    case 0x2:
        cout<<"InteropVersion:";
        break;
    case 0x0100:
        cout << "ImageWidth:";
        break;
    case 0x0101:
        cout << "ImageHeight:";
        break;
    case 0x010f://Make //string type
        cout << "Make:";
        break;
    case 0x0110://model, string
        cout << "Model:";
        break;
    case 0x0111:
        cout << "StripOffsets:";
        break;
    case 0x0112:
        cout << "Orientation:";
        break;
    case 0x0115:
        cout << "SamplesPerPixel:";
        break;
    case 0x0116:
        cout << "SamplesPerPixel:";
        break;
    case 0x011a:
        cout << "XResolution:";
        break;
    case 0x011b:
        cout << "YResolution:";
        break;
    case 0x0128:
        cout << "ResolutionUnit:";
        break;
    case 0x0213:
        cout << "YCbCrPositioning:"; //1 = Centered,2 = Co-sited, 1表示中心，2表示基准点
        break;
    case 0x8769:
        cout << "SubExifTag IFD:";
        isGps = false;
        return false;
        break;
    case 0x8825:
        cout << "GPS Info IFD:";//tag
        isGps = true;
        return false;
        break;
    //相机信息
    case 0x0131:
        cout<<"SoftWare:";
        break;
    case 0x0132:
        cout << "ModifyDate:";
        break;
    case 0x829a:
        cout << "ExposureTime:";
        break;
    case 0x829d:
        cout << "FNumber:";
        break;
    case 0x8822:
        cout << "ExposureProgram:";
        break;
    case 0x8827:
        cout << "ISO:";
        break;
    case 0x9000:
        cout << "ExifVersion:";
        break;
    case 0x9003:
        cout << "DateTimeOriginal:";
        break;
    case 0x9004:
        cout << "CreateDate:";
        break;
    case 0x9101:
        cout << "ComponentsConfiguration:";
        break;
    case 0x9201:
        cout << "ShutterSpeedValue:";
        break;
    case 0x9202:
        cout << "ApertureValue:";
        break;
    case 0x9203:
        cout << "BrightnessValue:";
        break;
    case 0x9204:
        cout << "ExposureCompensation:";
        break;
    case 0x9205:
        cout << "MaxApertureValue:";
        break;
    case 0x9207:
        cout << "MeteringMode:";
        break;
    case 0x9208:
        cout << "LightSource:";
        break;
    case 0x9209:
        cout << "Flash:";
        break;
    case 0x920a:
        cout << "FocalLength:";
        break;
    case 0x927c:
        cout << "MakeNode:";
        break;
    case 0x9286:
        cout << "UserComment:";
        break;
    case 0x9290:
        cout << "SubSecTime:";
        break;
    case 0x9291:
        cout << "SubSecTimeOriginal:";
        break;
    case 0x9292:
        cout << "SubSecTimeDigitized:";
        break;
    case 0xa000:
        cout << "FlashpixVersion:";
        break;
    case 0xa001:
        cout << "ColorSpace:";
        break;
    case 0xa002:
        cout << "ExifImageWidth:";
        break;
    case 0xa003:
        cout << "ExifImageHeight:";
        break;
    case 0xa005:
        cout << "Interop IFD:";
        isGps = false;
        return false;
    case 0xa217:
        cout << "SensingMethod:";
        break;
    case 0xa301:
        cout << "SceneType:";
        break;
    case 0xa402:
        cout << "ExposureMode:";
        break;
    case 0xa403:
        cout << "WhiteBalance:";
        break;
    case 0xa405:
        cout << "FocalLengthIn35mmFormat:";
        break;
    case 0xa406:
        cout << "SceneCaptureType:";
        break;
    case 0xa420:
        cout << "ImageUniqueID:";
        break;
    default:
        cout << "Unknown "<<hex<< entry.exifTag<<" ";
        break;
    }
    EntryDataPrint(entry);
    return true;
}
//大端
bool ParseIFD0(uint8_t *&buf, uint8_t* tiffHeader) {
    //directory entry 数量
    const uint8_t *bufPtr = buf;
    uint16_t count = GetUInt16(bufPtr, bigEnd);
    bufPtr += 2;
    
    for (uint16_t i = 0; i < count; i++)
    {
        DirectoryEntry entry;
        entry.exifTag = GetUInt16(bufPtr, bigEnd);
        bufPtr += 2;
        entry.ComponentType = GetUInt16(bufPtr, bigEnd);
        bufPtr += 2;
        entry.ComponentNum = GetUInt32(bufPtr, bigEnd);
        bufPtr += 4;
        entry.data = bufPtr;
        bufPtr += 4;
        entry.dataLen = entry.ComponentNum * (componentTypeLen[entry.ComponentType]);
        // 超过4 data为偏移，计算出真正的数据起始地址
        if (entry.dataLen > 4)
        {
            entry.data = tiffHeader + GetUInt32(entry.data, bigEnd);
        }
        if (!PrintEntry(entry)) {
            //sub IFD
            cout<<endl;
            uint8_t *subIFD = tiffHeader + GetUInt32(entry.data, bigEnd);
            ParseIFD0(subIFD, tiffHeader);
        }
    }
    //next IFD
    uint32_t offset = GetUInt32(bufPtr, bigEnd);
    
    if (offset == 0)
    {
        return true;
    }
    uint8_t *nextIFD = tiffHeader + offset;
    return ParseIFD0(nextIFD, tiffHeader);
}
bool DecodeApp1(const uint8_t *&buf, uint32_t size)
{
    //APP1 长度
    const uint8_t *bufPtr = buf;
    uint16_t LenApp0 = GetUInt16(bufPtr);
    bufPtr += 2;
    
    //"Exif" 45 78 69 66 00 00 00
    uint8_t marker[6] = {0x45, 0x78,0x69, 0x66, 0x00,0x00};
    auto ret = memcmp(marker, bufPtr, 6);
    bufPtr += 6;
    TiffHeader* header = (TiffHeader*)bufPtr;
    uint32_t offset = header->offsetIFD0;
    if (header->align == 0x4d4d) // MM, offset 网络序
    {
        offset = BigEnd(header->offsetIFD0);
    }
    uint8_t* IFD0 = (uint8_t*)header + offset;
    ParseIFD0(IFD0, (uint8_t*)header);

    return true;
}
int SkipSection(const uint8_t *&buf, uint32_t size)
{
    if (size < 2) {
        return -1;
    }

    auto len = GetUInt16(buf);
    buf += 2;
    //skip len-2
    cout<<"len = "<<len << endl;
    buf += len - 2;
    return 0;
}
int main()
{
    auto stride = ((1920 + 255) & (~255)) | (256);
    ifstream fin("D:\\OMX_IL_rk3568_ohos_so\\image_exif_byte_order_ii.jpg", ios::in | ios::binary);
    bigEnd = false;
    size_t size = fin.seekg(0, ios::end).tellg();
    fin.seekg(ios::beg);
    uint8_t* buf = new uint8_t[size];
    bool ret = fin.read((char*)buf, size).gcount() == size;
    cout<<"ret = "<<ret<<endl;
    uint8_t* bufEnd = buf + size;
    const uint8_t* bufPtr = buf;
    uint32_t width = 0, height = 0;
    while (bufPtr < bufEnd)
    {
        bool secFinish = false;
        int startCode = FindMarker(bufPtr, bufEnd);
        if (startCode < 0) {
            cout << "start code not found" << endl;
        }
        switch (startCode) {
        case SOI:
        case EOI:
            /* nothing to do */
            break;
        case SOF0:
        case SOF2:
        case SOF3:
        case SOF5:
        case SOF48:
            DecodeSof(bufPtr, bufEnd - bufPtr, width, height);
            // find widht and height
            break;
        case SOF6:
        case SOF7:
        case SOF9:
        case SOF10:
        case SOF11:
        case SOF13:
        case SOF14:
        case SOF15:
        
        case LSE:
        case JPG: {
            secFinish = true;
            cout << "jpeg: unsupported coding type :" << startCode << endl;
            break;
        }
        case DHT:
        {
            //DecodecHHT();
            break;
        }
        case DQT:
        case COM:
        case SOS:
        case DRI:
            secFinish = true;
            break;
        case APP1:
            DecodeApp1(bufPtr, bufEnd - bufPtr);
            break;
        default: {
            secFinish = true;
            cout << "jpeg: skip coding type :" << startCode << endl;
            break;
        }
        }
        if (secFinish) {
            SkipSection(bufPtr, bufEnd - bufPtr);
        }
    }
    return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
