﻿// encodeDemo.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <fstream>
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libavformat/avformat.h>
}
#define WIDTH 640
#define HEIGHT 480
int main()
{
    
    std::ifstream io("640x480.yuv", std::ios::binary);
    std::ofstream outIo("out.h264", std::ios::trunc | std::ios::binary);
    if (!io.good() || !outIo.good()) {
        std::cout << "open file err" << std::endl;
        return -1;
    }


    auto codec = avcodec_find_encoder_by_name("libopenh264");
    if (codec == nullptr) {
        std::cout << "find codec libopenh264 err:" << std::endl;
        return -1;
    }

    auto codecCtx = avcodec_alloc_context3(codec);
    
    auto pixFmts = codec->pix_fmts;
    AVPixelFormat pixFmt = *pixFmts++;
    while (pixFmt != AV_PIX_FMT_NONE )
    {
        pixFmt = *pixFmts++;
    }
    // 设置编码参数
    codecCtx->pix_fmt = AV_PIX_FMT_YUV420P;
    codecCtx->width = WIDTH; // 编码需要设置
    codecCtx->height = HEIGHT;
    codecCtx->time_base = { 1, 25 };
    codecCtx->framerate = { 25, 1 };
    codecCtx->gop_size = 10;
    codecCtx->max_b_frames = 0;
    codecCtx->bit_rate = 400000;
    codecCtx->slices = 1; // 方法1：设置slice num， 更通用一点
    // libopenh264编码slice_mode 设置，0表示关键帧不分片
    AVDictionary* dict = nullptr;
   // av_dict_set_int(&dict, "slice_mode", 0, 0); 
    //av_dict_set(&dict, "slice_mode", "0", 0); // 方法2：设置slice num, 版本高就不行了
    
    // 方法3：高版本建议使用
    //av_dict_set(&dict, "slices", "1", 0);
    //av_dict_set(&dict, "max_nal_size", "0", 0);
    auto err = avcodec_open2(codecCtx, codec, &dict);
    if (err != 0) {
        //std::cout << "avcodec_open2 err:" << av_err2str(err) << std::endl;
        return -1;
    }
    auto pkt = av_packet_alloc();
    auto frame = av_frame_alloc();
    auto bufferSize = av_image_get_buffer_size(AV_PIX_FMT_YUV420P, codecCtx->width, codecCtx->height, 1);
    uint8_t* buffer = reinterpret_cast<uint8_t*>(av_malloc(bufferSize));
    err = av_image_fill_arrays(frame->data, frame->linesize, buffer, AV_PIX_FMT_YUV420P, codecCtx->width, codecCtx->height, 1);

    // 设置frame参数
    frame->format = codecCtx->pix_fmt;
    frame->width = codecCtx->width;
    frame->height = codecCtx->height;
    while (!io.eof()) {
        auto count = io.read(reinterpret_cast<char*>(buffer), bufferSize).gcount();
        std::cout << "count :" << count << std::endl;
        if (!count) {
            err = avcodec_send_frame(codecCtx, nullptr);
        }
        err = avcodec_send_frame(codecCtx, frame);
        if (err < 0) {
            
            if (AVERROR_EOF == err)
            {
                std::cerr << "AVERROR_EOF!" << std::endl;
            }
            std::cerr << "Error sending a packet for encoding" << std::endl;
        }

        while (true) {
            err = avcodec_receive_packet(codecCtx, pkt);
            if (err == AVERROR(EAGAIN) || err == AVERROR_EOF) {
                break;
            }
            else if (err < 0) {
                fprintf(stderr, "Error during decoding %d\n", err);
                break;
            }
           /* std::cout << "frame:" << frame->format << ", width:" << frame->width << ",height" << frame->height
                << std::endl;*/
            
            outIo.write(reinterpret_cast<char*>(pkt->data), pkt->size);
        }

    }
    av_freep(&buffer);
    avcodec_close(codecCtx);
    if (dict) {
        av_dict_free(&dict);
        dict = nullptr;
    }
    //return 0;
}
