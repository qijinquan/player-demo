#pragma once
#include "RtpPacket.h"
#include <memory>
#define RTP_SEQUENTIAL 3
// 可能不同的源有不同的，同一个rtpsession 应该只有1个ssrc
class RtpQueue
{
public:
    RtpQueue();
    int Write(std::shared_ptr<RtpPacket> pkt);
    std::shared_ptr<RtpPacket> Read();
private:
    void Reset();
    int Find(uint16_t seq);
    int Insert(int position, std::shared_ptr<RtpPacket> pkt);
    void ResetBadItems();
private:
    std::shared_ptr<RtpPacket> items[20];
    int capacity = 20;
    int size;
    int pos;
    int probation = 3;
    int cycles = 0;
    uint16_t lastSeq;
    uint16_t firstSeq;

    int badCount = 0;
    uint16_t badSeq = 0;
    std::shared_ptr<RtpPacket> badItems[RTP_SEQUENTIAL+1];
};

