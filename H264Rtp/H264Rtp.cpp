﻿// H264Rtp.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <mutex>
#include "RtpPacket.h"
#include "RtpQueue.h"
#include "Util.h"

using namespace std;
//H264 RTP 封包传输
uint16_t seq = 0;
std::once_flag once;
void OnRecv(std::shared_ptr<RtpPacket> pkt);
int main()
{
    std::cout << "Hello World!\n";
    ifstream fio("h264.rtpdump", ios::binary| ios::in);
    uint8_t udpHeader[8] = { 0, };
    uint16_t udpLen = 0;
    shared_ptr<RtpQueue> queue = make_shared<RtpQueue>();
    while (!fio.eof())
    {
        if (fio.read((char*)udpHeader, 8).gcount() != 8)
        {
            cerr << "read udp header err" << endl;
            break;
        }
        udpLen = Util::GetUInt16(udpHeader);
        if (udpLen <= 8)
        {
            cerr << "udpLen is too short" << udpLen << endl;
            break;
        }
        vector<uint8_t> rtpPacket;
        rtpPacket.resize(udpLen - 8);
        if (fio.read((char*)rtpPacket.data(), udpLen - 8).gcount() != (udpLen - 8))
        {
            cerr << "read rtp packet err" << endl;
            break;
        }
        auto rtpPkt = make_shared<RtpPacket>(rtpPacket);
        rtpPkt->Deserialize();
        queue->Write(rtpPkt);
        auto tmpPkt = queue->Read();
        while (tmpPkt)
        {
            OnRecv(tmpPkt);
            tmpPkt = queue->Read();
        }
        cout<<"read rtp data len = "<<udpLen - 8 <<endl;
    };
    
}
void OnRecv(std::shared_ptr<RtpPacket> pkt)
{
    call_once(once, [&] {
        seq = pkt->header.seq - 1;
    });
    if (pkt->header.seq != seq + 1)
    {
        //lost
        return;
    }
    bool next = true;
    uint8_t naluType = *pkt->payload;
    switch (naluType & 0x1F)
    {
    case 0:
    case 31:
        return ;
    case 24:
        next = false;
    case 25:
        //stap
        return ;

    case 26:
        next = false;
    case 27:
        //mtap
    case 28:
        next = false;
    case 29:
        //fu A/B
        return;
    default://1-23
        return;
    }
    // 根据不同类型去解析packet
    
}

// 5.7.1. Single-Time Aggregation Packet (STAP) (p23)
/*
 0               1               2               3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                           RTP Header                          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|STAP-B NAL HDR |            DON                |  NALU 1 Size  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| NALU 1 Size   | NALU 1 HDR    |         NALU 1 Data           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               +
:                                                               :
+               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|               | NALU 2 Size                   |   NALU 2 HDR  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                            NALU 2 Data                        |
:                                                               :
|                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               :    ...OPTIONAL RTP padding    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
/*
 0               1               2               3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                          RTP Header                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|MTAP16 NAL HDR |   decoding order number base  |  NALU 1 Size  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| NALU 1 Size   | NALU 1 DOND   |         NALU 1 TS offset      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| NALU 1 HDR    |                NALU 1 DATA                    |
+-+-+-+-+-+-+-+-+                                               +
:                                                               :
+               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|               | NALU 2 SIZE                   |   NALU 2 DOND |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| NALU 2 TS offset              | NALU 2 HDR    |  NALU 2 DATA  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+               |
:                                                               :
|                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               :    ...OPTIONAL RTP padding    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
