#pragma once
#include <cinttypes>
#include <memory>
#include <vector>
using namespace std;
// RFC3550 RTP: A Transport Protocol for Real-Time Applications
// 5.1 RTP Fixed Header Fields (p12)
/*
 0               1               2               3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|V=2|P|X|   CC  |M|     PT      |      sequence number          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                           timestamp                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                synchronization source (SSRC) identifier       |
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
|                 contributing source (CSRC) identifiers        |
|                               ....                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
#define LITTLE_END
// alignas(32)
typedef struct
{
    uint32_t v:2;//Protocl Version
    uint32_t p:1;// Padding falg
    uint32_t x:1;//Header Extension Flag
    uint32_t cc:4; //CSRC count
    uint32_t m:1;//Marker bit
    uint32_t pt:7;//Payload Type
    uint32_t seq:16;//seq
    uint32_t timestamp;//timestame
    uint32_t ssrc;//Synchronization Source
}RtpHeader;
#define RTP_V(v)	((v >> 30) & 0x03) /* protocol version */
#define RTP_P(v)	((v >> 29) & 0x01) /* padding flag */
#define RTP_X(v)	((v >> 28) & 0x01) /* header extension flag */
#define RTP_CC(v)	((v >> 24) & 0x0F) /* CSRC count */
#define RTP_M(v)	((v >> 23) & 0x01) /* marker bit */
#define RTP_PT(v)	((v >> 16) & 0x7F) /* payload type */
#define RTP_SEQ(v)	((v >> 00) & 0xFFFF) /* sequence number */
#define RTP_FIXED_HEADER 12
class RtpPacket
{
public:
    RtpPacket(const vector<uint8_t>& data);// 总长度没有
    int Deserialize();//解
public:
    RtpHeader header;
    uint32_t csrc[16];
    const void* extension;
    uint16_t extLen;
    uint16_t reserved;
    int payloadLen;
    const uint8_t* payload;
    const vector<uint8_t>& data_;
};

