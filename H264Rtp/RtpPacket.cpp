#include "RtpPacket.h"
#include "Util.h"
RtpPacket::RtpPacket(const vector<uint8_t>& data) :data_(std::move(data))
{

}
int RtpPacket::Deserialize()
{
    const uint8_t* data = data_.data();
    auto v = Util::GetUInt32(data);
    header.v = RTP_V(v);
	header.p = RTP_P(v);
	header.x = RTP_X(v);
	header.cc = RTP_CC(v);
	header.m = RTP_M(v);
	header.pt = RTP_PT(v);
	header.seq = RTP_SEQ(v);
    header.timestamp = Util::GetUInt32(data+4);
    header.ssrc = Util::GetUInt32(data + 8);
    uint32_t hdrLen = RTP_FIXED_HEADER + header.cc*4;
    //csrc
    for (size_t i = 0; i < header.cc; i++)
    {
        this->csrc[i] = Util::GetUInt32(data + 12 + i*4);
    }
    payload = data+hdrLen;
    payloadLen = data_.size() - hdrLen;
    if (header.x == 1)
    {
        const uint8_t *rtpExt = payload;
        this->extension = rtpExt + 4;
        this->reserved = Util::GetUInt16(rtpExt);
        this->extLen = Util::GetUInt16(rtpExt + 2)*4;
        payload = rtpExt + extLen + 4;
        payloadLen -= extLen + 4;
    }
    return 0;
}
