#pragma once
#include <cinttypes>
class Util
{
public:
    static inline uint32_t GetUInt32(const uint8_t* buf)
    {
        return (((uint32_t)buf[0]) << 24) | (((uint32_t)buf[1]) << 16) | (((uint32_t)buf[2]) << 8) | buf[3];
    }
    static inline uint32_t GetUInt16(const uint8_t* buf)
    {
        return (((uint32_t)buf[0]) << 8) | buf[1];
    }
};

