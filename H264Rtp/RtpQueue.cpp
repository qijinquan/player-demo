#include "RtpQueue.h"
#include <iostream>
using namespace std;
#define RTP_SEQMOD	 (1 << 16)
#define RTP_MISORDER 300
RtpQueue::RtpQueue()
{
    Reset();
}

void RtpQueue::Reset()
{
    ResetBadItems();
    for (size_t i = 0; i < capacity; i++)
    {
        items[i] = nullptr;
    }
    pos = 0;
    size = 0;
    probation = RTP_SEQUENTIAL;
}

int RtpQueue::Find(uint16_t seq)
{
    return 0;
}

void RtpQueue::ResetBadItems()
{
    badCount = 0;
    badSeq = 0;
    for (size_t i = 0; i < badCount; i++)
    {
        badItems[i] = nullptr;
    }
}
int RtpQueue::Insert(int position, std::shared_ptr<RtpPacket> pkt)
{
    if (size >= capacity)
    {
        cerr<<" rtp queue is full";
        return 0;
    }

    items[position % capacity] = std::move(pkt);
    size++;
    return 1;
}

int RtpQueue::Write(std::shared_ptr<RtpPacket> pkt)
{
    if (probation)
    {
        if (size >0 && pkt->header.seq == lastSeq + 1)
        {
            probation--;
            if (probation == 0)
            {
                firstSeq = items[pos]->header.seq;
            }
        }
        else
        {
            Reset();
        }
        lastSeq = pkt->header.seq;
        return Insert(pos + size, pkt);
    }
    else
    {
        uint16_t delta = pkt->header.seq - lastSeq;
        if (delta > 0 && delta < 1000)
        {
            if (pkt->header.seq < lastSeq)
                cycles += RTP_SEQMOD;

            ResetBadItems();
            lastSeq = pkt->header.seq;
            return Insert(pos + size, pkt);
        }
        else if ((int16_t)delta <= 0 && (int16_t)delta >= (int16_t)(firstSeq - lastSeq))
        {
            auto idx = Find(pkt->header.seq);
            ResetBadItems();
            return Insert(idx, pkt);
        }
        else if ((uint16_t)(firstSeq - pkt->header.seq) < RTP_MISORDER)
        {
            // too late:pkt->req.seq < q->first_seq
            return -1;
        }
        else 
        {
            if (badCount > 0 && badSeq == pkt->header.seq)
            {
                if (badCount >= RTP_SEQUENTIAL)
                {
                    for (size_t i = 0; i < badCount; i++)
                    {
                        Insert(pos+size, badItems[i]);
                    }
                    badCount = 0;
                    lastSeq = pkt->header.seq;
                    return Insert(pos + size, pkt);
                }
            }
            else
            {
                ResetBadItems();
            }
            badSeq = (pkt->header.seq + 1) % (RTP_SEQMOD - 1);
            badItems[badCount++] = pkt;
            return 1;
        }
    }
    return 0;
}

std::shared_ptr<RtpPacket> RtpQueue::Read()
{
    //Ԥ��probation ����
    if (size < 1 || probation)
    {
        return nullptr;
    }
    shared_ptr<RtpPacket> pkt = items[pos];
    if (!pkt)
    {
        return pkt;
    }
    /*
    if (firstSeq == pkt->header.seq)
    {
        firstSeq++;
        size--;
        pos = (pos + 1) % capacity;
    }
    else {
        // ��ֵ����
    }
    */
    firstSeq = pkt->header.seq + 1;
    size--;
    pos = (pos + 1) % capacity;
    return pkt;
}
