﻿// encodeDemo.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <fstream>
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavformat/avformat.h>
}
int main()
{

    std::ifstream io("640x480.h264", std::ios::binary);
    std::ofstream outIo("640x480.yuv", std::ios::trunc | std::ios::binary);
    if (!io.good() || !outIo.good()) {
        std::cout << "open file err" << std::endl;
        return -1;
    }
    io.seekg(0, std::ios::end);
    auto count = io.tellg();
    std::cout << "file count :" << count << std::endl;

    io.seekg(0, std::ios::beg);

    char* buf = new char[count];
    io.read(buf, count);

    auto codec = avcodec_find_decoder_by_name("libopenh264");
    if (codec == nullptr) {
        std::cout << "find codec libopenh264 err:" << std::endl;
        return -1;
    }

    auto codecCtx = avcodec_alloc_context3(codec);
    // codec->pix_fmts
    codecCtx->pix_fmt = AV_PIX_FMT_YUV420P;
    //codecCtx->width = 640; //可以不设置，解码时
    //codecCtx->height = 480;
    auto err = avcodec_open2(codecCtx, codec, nullptr);
    if (err != 0) {
        //std::cout << "avcodec_open2 err:" << av_err2str(err) << std::endl;
        return -1;
    }
    auto pkt = av_packet_alloc();
    auto frame = av_frame_alloc();
    auto parser = av_parser_init(codec->id);
    // int32_t bufOffSet = 0;
    int32_t bufferCount = count;
    while (bufferCount >= 0) {
        av_parser_parse2(parser, codecCtx, &pkt->data, &pkt->size, (uint8_t*)(buf), bufferCount, AV_NOPTS_VALUE,
            AV_NOPTS_VALUE, 0);

        bufferCount -= pkt->size;
        buf += pkt->size;
        //std::cout << "pkt size" << pkt->size << ",bufferCount:" << bufferCount << std::endl;
        auto ret = avcodec_send_packet(codecCtx, pkt);
        if (ret < 0) {
            fprintf(stderr, "Error sending a packet for decoding\n");
            exit(1);
        }

        while (true) {
            ret = avcodec_receive_frame(codecCtx, frame);
            if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                fprintf(stderr, "avcodec_receive_frame break.\n");
                break;
            }
            else if (ret < 0) {
                fprintf(stderr, "Error during decoding %d\n", ret);
                break;
            }
            /* std::cout << "frame:" << frame->format << ", width:" << frame->width << ",height" << frame->height
                 << std::endl;*/

            outIo.write(reinterpret_cast<char*>(frame->data[0]), frame->linesize[0] * frame->height);
            outIo.write(reinterpret_cast<char*>(frame->data[1]), frame->linesize[1] * frame->height / 2);
            outIo.write(reinterpret_cast<char*>(frame->data[2]), frame->linesize[2] * frame->height / 2);
            // av_frame_unref(frame);
        }

        if (pkt->size == 0)
        {
            break;
        }

    }

    avcodec_close(codecCtx);
    //return 0;
}
