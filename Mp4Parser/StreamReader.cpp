#include "StreamReader.h"
#include <cassert>
FileStreamerReader::FileStreamerReader(std::unique_ptr<std::istream>&& tempIstr):istr(std::move(tempIstr))
{
    istr->seekg(0, std::ios_base::end);
    length = istr->tellg();
    istr->seekg(0, std::ios_base::beg);
}

int64_t FileStreamerReader::GetPosition() const
{
    return istr->tellg();
}

StreamReader::GrowStatus FileStreamerReader::WaitForFileSize(int64_t target_size)
{
    return target_size > length ? GrowStatus::SIZE_READCHED : GrowStatus::SIZE_BEYOND_EOF;
}

bool FileStreamerReader::Read(void * data, size_t size)
{
    int64_t endPos = GetPosition();
    if ((endPos > length) || (data == nullptr))
    {
        return false;
    }
    istr->read(static_cast<char*>(data), size);
    return true;
}

bool FileStreamerReader::Seek(int64_t postion)
{
    if (postion > length)
    {
        return false;
    }
    istr->seekg(postion, std::ios_base::beg);
    return true;
}

void BitsStreamRange::SkipWithoutAdvancingFilePos(int64_t nBytes)
{
    assert(nBytes <= remaining);
    remaining -= nBytes;
    if (parent)
    {
        parent->SkipWithoutAdvancingFilePos(nBytes);
    }
}
                            
BitsStreamRange::BitsStreamRange(std::shared_ptr<StreamReader> stream, uint64_t length, std::shared_ptr<BitsStreamRange> parent)
{
    remaining = length;

    this->stream = stream;
    this->parent = parent;

    if (parent) {
        nestingLevel = parent->nestingLevel + 1;
    }
}

StreamReader::GrowStatus BitsStreamRange::WaitUntilRangeIsAvaliable()
{
    return stream->WaitForFileSize(stream->GetPosition() + remaining);
}

uint8_t BitsStreamRange::ReadUChar()
{
    if (!PrepareRead(1)) {
        return 0;
    }

    uint8_t buf = 0;
    bool success = stream->Read((char*)buf, 1);

    if (!success) {
        SetEofWhileReading();
        return 0;
    }

    return buf;
}

uint16_t BitsStreamRange::ReadUShort()
{
    if (!PrepareRead(2)) {
        return 0;
    }

    uint8_t buf[2];
    bool success = stream->Read((char*)buf, 2);

    if (!success) {
        SetEofWhileReading();
        return 0;
    }

    return static_cast<uint16_t>((buf[0] << 8) | (buf[1]));
}

uint32_t BitsStreamRange::ReadUInt()
{
    if (!PrepareRead(4)) {
        return 0;
    }

    uint8_t buf[4];
    bool success = stream->Read((char*)buf, 4);

    if (!success) {
        SetEofWhileReading();
        return 0;
    }

    return ((buf[0] << 24) |
        (buf[1] << 16) |
        (buf[2] << 8) |
        (buf[3]));
}

bool BitsStreamRange::Read(uint8_t * data, int64_t n)
{
    if (!PrepareRead(n)) {
        return false;
    }

    bool success = stream->Read(data, n);
    if (!success) {
        SetEofWhileReading();
    }
    return success;
}

bool BitsStreamRange::PrepareRead(int64_t nBytes)
{
    if (nBytes < 0) {
        assert(false);
        return false;
    }
    else if (remaining < nBytes) {
        // --- not enough data left in box -> move to end of box and set error flag
        SkipToEndOfBox();
        error = true;
        return false;
    }
    else {
        // --- this is the normal case (m_remaining >= nBytes)
        if (parent) {
            if (!parent->PrepareRead(nBytes)) {
                return false;
            }
        }
        remaining -= nBytes;
        return true;
    }
}

StreamReader::GrowStatus BitsStreamRange::WaitForAvaliableBytes(int64_t nBytes)
{
    int64_t target_size = stream->GetPosition() + nBytes;

    return stream->WaitForFileSize(target_size);
}
