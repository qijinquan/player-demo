#pragma once
#include <inttypes.h>
#include <memory>
#include <iostream>
class StreamReader
{
public:
    StreamReader() = default;
    virtual ~StreamReader() = default;
    virtual int64_t GetPosition() const = 0;
    enum class GrowStatus {
        SIZE_READCHED,  // request size has been reached
        TIMEOUT,        // size has not been reached yet, but it may still grow further
        SIZE_BEYOND_EOF // END
    };
    virtual GrowStatus WaitForFileSize(int64_t target_size) = 0;
    virtual bool Read(void* data, size_t size) = 0;
    virtual bool Seek(int64_t postion) = 0;
    bool SeekCur(int64_t offset)
    {
        return Seek(GetPosition() + offset);
    }
};
class FileStreamerReader : public StreamReader
{
public:
    FileStreamerReader(std::unique_ptr<std::istream> &&tempIstr);
    ~FileStreamerReader() = default;
    int64_t GetPosition() const override;
    GrowStatus WaitForFileSize(int64_t target_size) override;
    bool Read(void* data, size_t size) override;
    bool Seek(int64_t postion) override;
private:
    std::unique_ptr<std::istream> istr;
    int64_t length;
};
class BitsStreamRange
{
 public:
     BitsStreamRange(std::shared_ptr<StreamReader> stream, uint64_t length, std::shared_ptr<BitsStreamRange> parent = nullptr);
    StreamReader::GrowStatus WaitUntilRangeIsAvaliable();
    uint8_t ReadUChar();
    uint16_t ReadUShort();
    uint32_t ReadUInt();
    bool Read(uint8_t* data, int64_t n);
    bool PrepareRead(int64_t nBytes);
    StreamReader::GrowStatus WaitForAvaliableBytes(int64_t nBytes);
    void SkipToEndOfFile()
    {
        remaining = 0;
        if (parent) {
            parent->SkipToEndOfFile();
        }
    }
    void SkipToEndOfBox()
    {
        if (remaining > 0)
        {
            if (parent) {
                parent->SkipWithoutAdvancingFilePos(remaining);
            }
        }
        stream->SeekCur(remaining);
        remaining = 0;
    }
    void SetEofWhileReading()
    {
        remaining = 0;
        if (parent)
        {
            parent->SetEofWhileReading();
        }
        error = true;
    }

    bool Eof() const
    {
        return  remaining == 0;
    }

    bool Error() const
    {
        return error;
    }
    /*
    Error GetError() const
    {

    }*/
    int GetNestingLevel()
    {
        return nestingLevel;
    }

    int64_t GetRemainingBytes() const
    {
        return remaining;
    }
private:
        std::shared_ptr<StreamReader> stream;
        std::shared_ptr<BitsStreamRange> parent = nullptr;
        int nestingLevel = 0;
        int64_t remaining;
        bool error = false;

        // Note: 'nBytes' may not be larger than the number of remaining bytes
        void SkipWithoutAdvancingFilePos(int64_t nBytes);
};

