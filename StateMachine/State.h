#pragma once
#include <memory>
class Executor
{
public:
    virtual void DoA();
    virtual void DoB();
    virtual void DoC();
};
class Context;
class State
{
public:
virtual void Handle(std::shared_ptr<Context> ctx) = 0;
virtual void Enter() = 0;
virtual void Exit() = 0;
virtual ~State();
protected:
    State(std::shared_ptr<Executor>);
protected:
    std::weak_ptr<Executor> executor_;
};

class StateA : public State
{
public:
    StateA(std::shared_ptr<Executor> executor);
    ~StateA();
    void Handle(std::shared_ptr<Context> ctx) override;
    void Enter() override;
    void Exit()override;
};

class StateB : public State
{
public:
    StateB(std::shared_ptr<Executor> executor);
    ~StateB();
    void Handle(std::shared_ptr<Context> ctx) override;
    void Enter() override;
    void Exit()override;
};

class StateC : public State
{
public:
    StateC(std::shared_ptr<Executor> executor);
    ~StateC();
    void Handle(std::shared_ptr<Context> ctx) override;
    void Enter() override;
    void Exit()override;
};

class Context : public std::enable_shared_from_this<Context>
{
 public:
    Context(std::shared_ptr<State> state);
    ~Context();
    void Request();
    void ChangeState(std::shared_ptr<State> state);
 private:
    std::shared_ptr<State> state_;
};