#include "State.h"
#include <iostream>
#include <memory>
using namespace std;
State::State(std::shared_ptr<Executor> executor):executor_(executor)
{

}

State::~State()
{

}

StateA::StateA(std::shared_ptr<Executor> executor) : State(executor)
{

}

StateA::~StateA()
{

}
void StateA::Handle(std::shared_ptr<Context> ctx)
{
    //cout << "StateA Handle" << endl;
    auto exe = executor_.lock();
    if (exe)
    {
        exe->DoA();
    }
    ctx->ChangeState(std::make_shared<StateB>(exe));
}

void StateA::Enter()
{
    cout << "StateA Enter" << endl;
}

void StateA::Exit()
{
    cout << "StateA Exit" << endl;
}

StateB::StateB(std::shared_ptr<Executor> executor) : State(executor)
{

}

StateB::~StateB()
{

}

void StateB::Enter()
{
    cout << "StateB Enter" << endl;
}

void StateB::Exit()
{
    cout << "StateB Exit" << endl;
}
void StateB::Handle(std::shared_ptr<Context> ctx)
{
    //cout << "StateB" << endl;
    auto exe = executor_.lock();
    if (exe)
    {
        exe->DoB();
    }
    ctx->ChangeState(std::make_shared<StateC>(exe));
}

StateC::StateC(std::shared_ptr<Executor> executor) : State(executor)
{

}

StateC::~StateC()
{

}

void StateC::Handle(std::shared_ptr<Context> ctx)
{
    //cout << "StateC" << endl;
    auto exe = executor_.lock();
    if (exe)
    {
        exe->DoC();
    }
    ctx->ChangeState(std::make_shared<StateA>(exe));
}

void StateC::Enter()
{
    cout << "StateC Enter" << endl;
}

void StateC::Exit()
{
    cout << "StateC Exit" << endl;
}

Context::Context(std::shared_ptr<State> state)
{
    state_ = state;
}

Context::~Context()
{

}

void Context::Request()
{
    if (state_)
    {
        state_->Handle(shared_from_this());
    }
}

void Context::ChangeState(std::shared_ptr<State> state)
{
    if (state_)
    {
        state_->Exit();
    }
    state_ = state;
    // 进入转态，等待处理
    if (state_)
    {
        state_->Enter();
    }
}

void Executor::DoA()
{
    cout<<"Do something in StateA"<<endl;
}

void Executor::DoB()
{
    cout << "Do something in StateB" << endl;
}

void Executor::DoC()
{
    cout << "Do something in StateC" << endl;
}
