#include "ObjectCollector.h"
ObjectCollector* ObjectCollector::instance_ = new ObjectCollector();
ObjectDelegator<MyTest> MyTest::deletor;
ObjectCollector * ObjectCollector::GetInstance()
{
    return instance_;
}

bool ObjectCollector::ConstructorRegister(const std::string & desc, const Construcor & ctor)
{
    ctorMap_[desc] = std::move(ctor);
    return false;
}

void ObjectCollector::ConstructorUnRegister(const std::string & desc)
{
    auto iter = ctorMap_.find(desc);
    if (iter != ctorMap_.end())
    {
        ctorMap_.erase(iter);
    }
}
