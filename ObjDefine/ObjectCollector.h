#pragma once
#include <functional>
#include <mutex>
#include <map>
class Nonecoyable {
public:
    Nonecoyable() = default;
    virtual ~Nonecoyable() = default;
private:
    Nonecoyable(const Nonecoyable&other) = delete;
    Nonecoyable(const Nonecoyable&&other) = delete;
    Nonecoyable& operator=(const Nonecoyable&other) = delete;
    Nonecoyable& operator=(const Nonecoyable&&other) = delete;
};
class BaseObj:public Nonecoyable
{
public:
    using Ptr = std::shared_ptr<BaseObj>;
    BaseObj() = default;
    virtual ~BaseObj() = default;
};
#define DECLARE_DESC(DESC)                                  \
    const static inline std::string metaDesc_ = DESC;     \
    const static inline std::string& GetDesc()              \
    {                                                       \
        return metaDesc_;                                   \
    }

class ObjectCollector
{
public:
    using Construcor = std::function<BaseObj::Ptr()>;
    static ObjectCollector* GetInstance();

    bool ConstructorRegister(const std::string& desc, const Construcor& ctor);
    void ConstructorUnRegister(const std::string& desc);
    template<class T>
    std::shared_ptr<T> NewObject()
    {
        auto iter = ctorMap_.find(T::GetDesc());
        if (iter != ctorMap_.end())
        {
            return std::dynamic_pointer_cast<T>(iter->second());
        }
        return nullptr;
    }
    BaseObj::Ptr NewObject(const std::string& desc) {
        auto iter = ctorMap_.find(desc);
        if (iter != ctorMap_.end())
        {
            return iter->second();
        }
        return nullptr;
    }
    ~ObjectCollector() = default;
    private:
        ObjectCollector() = default;
        
        static ObjectCollector* instance_;
        std::map<const std::string, Construcor> ctorMap_;

        std::mutex mutex_;
};
template<class T>
class ObjectDelegator :public Nonecoyable {
public:
    ObjectDelegator();
    ~ObjectDelegator();
};

template<class T>
inline ObjectDelegator<T>::ObjectDelegator()
{
    ObjectCollector::GetInstance()->ConstructorRegister(T::GetDesc(),  []()->BaseObj::Ptr {
        return std::make_shared<T>();
    });
}
template<class T>
inline ObjectDelegator<T>::~ObjectDelegator()
{
    ObjectCollector::GetInstance()->ConstructorUnRegister(T::GetDesc());
}

class MyTest : public BaseObj
{
public:
    using Ptr = std::shared_ptr<MyTest>;
    MyTest() = default;
    ~MyTest() = default;
    DECLARE_DESC("v1.0_mytest");
private:
    static ObjectDelegator<MyTest> deletor;
};