﻿// ObjDefine.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <memory>
#include <any>
#include <fstream>
#include "cJSON.h"
#include "ObjectCollector.h"
#define MEMBER(type, name)							\
private : type m_##name;							\
public: const type& name() { return m_##name; }		\
public: void name(const type& arg) { m_##name = arg; }	



#define CONFIG_DEFINE(className)		\
class Config##className {                          \
public:                                             \
    using Ptr = std::shared_ptr<Config##className>; \
                                                    \
 }
typedef enum {
  TrackInvalid = -1,
  TrackVideo = 0,
  TrackAudio,
  TrackTitle,
  TrackApplication,
  TrackMax
} TrackType;


#define RTP_PT_MAP(XX)                                                         \
  XX(PCMU, TrackAudio, 0, 8000, 1, CodecG711U)                                 \
  XX(GSM, TrackAudio, 3, 8000, 1, CodecInvalid)                                \
  XX(G723, TrackAudio, 4, 8000, 1, CodecInvalid)                             

//枚举定义
typedef enum {
#define ENUM_DEF(name, type, value, clock_rate, channel, codec_id)             \
  PT_##name = value,                                                           
  RTP_PT_MAP(ENUM_DEF)
#undef ENUM_DEF
      PT_MAX = 128
} PayloadType;

#define WRITEINT32(data, value) \
if (data)\
{\
    printf("write to data %p: value : %d, func: %s, line:%d", data, value, __FUNCTION__, __LINE__);\
}

int main() {
  auto ptr = ObjectCollector::GetInstance();
  MyTest::Ptr objtestPtr = ptr->NewObject<MyTest>();
  BaseObj::Ptr objPtr = ptr->NewObject(MyTest::GetDesc());
 
  int a = 1;
  int b=2;
  WRITEINT32(&a, b);
  return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
