#pragma once
#include <inttypes.h>
#include <iostream>
#include <memory>
#include <list>
#include <map>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include "Pool.h"

enum class EventType : uint32_t
{
    EVENT_TICK = 0,
};

struct Event
{
    Event(const EventType& type) :type_(type){}
    ~Event() = default;
    EventType type_;
};


constexpr uint32_t MAX_THREADS = 1;
class IEventHandler
{
public:
    IEventHandler() = default;
    virtual ~IEventHandler() = default;
    virtual int HandleEvent(std::shared_ptr<Event>& event) = 0;
};
class EventManager
{
public:
    typedef std::list<std::shared_ptr<IEventHandler>> HandlerList;
    typedef std::map<EventType, HandlerList> IdHandleMap;
    typedef std::queue<std::shared_ptr<Event>> EventQ;
public:
    static EventManager& GetInstance();
    ~EventManager() = default;
    
    void AddEventHandler(EventType id, std::shared_ptr<IEventHandler>& handle);
    void RemoveEventHandle(EventType id, std::shared_ptr<IEventHandler>& handle);
    int32_t DispatchEvent(std::shared_ptr<Event> event);
    void PushEvent(std::shared_ptr<Event> event);
    int32_t PushSync(std::shared_ptr<Event> event);
    void Exit();
protected:
    EventManager() = default;
private:
    void Start();
private:
    static EventManager instance_;
    IdHandleMap eventMap_;
    //EventQ  evtQ_;
    std::atomic<bool> exit_ = false;
    
    std::mutex lock_;
    std::condition_variable signal_;
    //std::vector<std::thread> threads_;
    uint32_t maxCount_;
    std::shared_ptr<ThreadPool> pool_ = nullptr;
    
};

