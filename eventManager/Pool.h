#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>
#include <atomic>

class ThreadPool {
public:
    using Task = std::function<void()>;
public:
    ThreadPool(size_t);
    template<class F, class... Args>
    auto enqueue(F&& f, Args&&... args)
        ->std::future<typename std::result_of<F(Args...)>::type>;
    ~ThreadPool();
    void Start();
    void Exit();
private:
    std::vector< std::thread > workers_;
    std::queue<Task> tasks_;

    // synchronization
    std::mutex queue_mutex;
    std::condition_variable condition;
    std::atomic<bool> running_;
};

// the constructor just launches some amount of workers
inline ThreadPool::ThreadPool(size_t threads)
    : running_(true)
{
    for (size_t i = 0; i < threads; ++i)
        workers_.emplace_back(
            [this]
    {
        for (;;)
        {
            std::function<void()> task;

            {
                std::unique_lock<std::mutex> lock(this->queue_mutex);
                this->condition.wait(lock, [this] { return !this->running_ && !this->tasks_.empty(); });
                if (!this->running_)
                {
                    continue;
                }
                    
                task = std::move(this->tasks_.front());
                this->tasks_.pop();
            }

            task();
        }
    }
    );
}

// add new work item to the pool
template<class F, class... Args>
auto ThreadPool::enqueue(F&& f, Args&&... args)
-> std::future<typename std::result_of<F(Args...)>::type>
{
    using return_type = typename std::result_of<F(Args...)>::type;

    auto task = std::make_shared< std::packaged_task<return_type()> >(
        std::bind(std::forward<F>(f), std::forward<Args>(args)...)
        );

    std::future<return_type> res = task->get_future();
    {
        std::unique_lock<std::mutex> lock(queue_mutex);

        // don't allow enqueueing after stopping the pool
        if (!running_)
            throw std::runtime_error("enqueue on stopped ThreadPool");

        tasks_.emplace([task]() { (*task)(); });
    }
    condition.notify_one();
    return res;
}
inline void ThreadPool::Start()
{
    if (running_ == false)
    {
        running_.store(true);
    }
}
inline void ThreadPool::Exit()
{
    if (running_ == true)
    {
        {
            std::unique_lock<std::mutex> lock(queue_mutex);
            running_.store(false);
            condition.notify_all();
        }
        for (std::thread &worker : workers_)
        {
            worker.join();
        }
    }
    while (!tasks_.empty())
    {
        tasks_.pop();
    }
}
// the destructor joins all threads
inline ThreadPool::~ThreadPool()
{
    {
        std::unique_lock<std::mutex> lock(queue_mutex);
        running_.store(false);
    }
    condition.notify_all();
    for (std::thread &worker : workers_)
        worker.join();
}

#endif