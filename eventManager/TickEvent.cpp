#include "TickEvent.h"
#include <iostream>
std::atomic<int> TimeEventHandler::count_ = 0;
int TimeEventHandler::HandleEvent(std::shared_ptr<Event>& event)
{
    std::shared_ptr<TickEvent> ticker = std::static_pointer_cast<TickEvent>(event);
    std::cout << "time now: " << ticker->time_ << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    return 0;
}