#include <iostream>
#include <vector>
#include <chrono>
#include <functional>
//#include "ThreadPool.h"
#include "Pool.h"

std::string MyFuncAdd(std::string&a, std::string&b)
{
    return a + b;
}
int main2()
{

    ThreadPool pool(4);
    std::vector< std::future<int> > results;

    for (int i = 0; i < 8; ++i) {
        results.emplace_back(
            pool.enqueue([i] {
            std::cout << "hello " << i << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout << "world " << i << std::endl;
            return i * i;
        })
        );
    }

    for (auto && result : results)
        std::cout << result.get() << ' ';
    std::cout << std::endl;
    std::string a = "123";
    std::string b = "456";
    auto ret = pool.enqueue([&a, &b] {return a + b; });
    std::cout << ret.get().c_str() << std::endl;

    std::function<std::string(std::string&, std::string&)> myF = MyFuncAdd;

    auto ret2 = pool.enqueue(std::bind(myF, a, b));
    std::cout << ret2.get().c_str() << std::endl;
    return 0;
}