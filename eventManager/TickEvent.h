#pragma once
#include "EventManager.h"
#include <atomic>
struct TickEvent :  Event
{
    TickEvent(time_t time = 0) :Event(EventType::EVENT_TICK), time_(time)
    {

    }
    ~TickEvent() = default;
    time_t time_;
};
class TimeEventHandler : public IEventHandler
{
public:
    TimeEventHandler() = default;
    virtual ~TimeEventHandler() = default;
    virtual int HandleEvent(std::shared_ptr<Event>& event);
private:
    static std::atomic<int> count_;
};



