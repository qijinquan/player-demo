#include "EventManager.h"
#include <mutex>
#include <future>
EventManager EventManager::instance_;

EventManager& EventManager::GetInstance()
{
    static std::once_flag flag;
    std::call_once(flag, [&]() {
        instance_.maxCount_ = std::thread::hardware_concurrency();
        instance_.Start();
    });
    return instance_;
}
void EventManager::Start()
{
    auto count = 1;// std::thread::hardware_concurrency();
    pool_ = std::make_shared<ThreadPool>(count);
    pool_->Start();
    
}

void EventManager::Exit()
{
    if (pool_)
    {
        pool_->Exit();

    }
}

void EventManager::AddEventHandler(EventType id, std::shared_ptr<IEventHandler>& handle)
{
    eventMap_.emplace(std::make_pair(id, HandlerList()));
    eventMap_.find(id)->second.push_back(handle);
}


void EventManager::RemoveEventHandle(EventType id, std::shared_ptr<IEventHandler>& handle)
{
    auto it = eventMap_.find(id);
    if (it == eventMap_.end())
    {
        return;
    }

    auto listIt = std::find(it->second.begin(), it->second.end(), handle);
    if (listIt != it->second.end())
    {
        it->second.erase(listIt);
    }
}


int32_t EventManager::DispatchEvent(std::shared_ptr<Event> event)
{
    auto it = eventMap_.find(event->type_);
    if (it == eventMap_.end())
    {
        return 0;
    }
    
    auto& handles = it->second;
    for (auto listIt = handles.begin(); listIt != handles.end(); listIt++)
    {
        (*listIt)->HandleEvent(event);
    }
    return 0;
}

void EventManager::PushEvent(std::shared_ptr<Event> event)
{
    /*
    pool_->enqueue([this, event] {
        return DispatchEvent(event);
    });
    */
    pool_->enqueue(std::bind(&EventManager::DispatchEvent, this, event));
}
int32_t EventManager::PushSync(std::shared_ptr<Event> event)
{
    auto res = pool_->enqueue(std::bind(&EventManager::DispatchEvent, this, event));
    return res.get();
}