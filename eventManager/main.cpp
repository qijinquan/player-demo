﻿// eventManager.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "EventManager.h"
#include "TickEvent.h"
#include <thread>
#include <any>
struct A
{
    int a;
};
int main()
{
    std::cout << "Hello World!\n";
    {
        A a;
        a.a = 1;
        std::any any = "123";
        std::any any2 = a;
        std::cout << any2.type().name();
        std::map<int, std::any> anyMap;
        auto s = std::any_cast<struct A>(any2);
        anyMap.emplace(std::make_pair(1,1));
    }
    std::shared_ptr<IEventHandler> timeHandler = std::make_shared<TimeEventHandler>();
    EventManager::GetInstance().AddEventHandler(EventType::EVENT_TICK, timeHandler);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::thread t1([&]() {
        /*
        for (size_t i = 0; i < 1000; i++)
        {
            std::shared_ptr<Event> tickEvt = std::make_shared<TickEvent>(i);
            EventManager::GetInstance().PushEvent(tickEvt);
        }
        */
        //std::this_thread::sleep_for(std::chrono::seconds(15));
       
        
    });
    t1.join();

    //std::this_thread::sleep_for(std::chrono::seconds(3));
    EventManager::GetInstance().RemoveEventHandle(EventType::EVENT_TICK, timeHandler);
    
    EventManager::GetInstance().Exit();
    
    return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
