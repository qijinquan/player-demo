#pragma once
#include <memory>
#include <string>
#include"iclient_callback.h"
#include <WinSock2.h>
class TcpClient : std::enable_shared_from_this<TcpClient>
{
public:
    using Ptr = std::shared_ptr<TcpClient>;
    TcpClient(const std::string& peer, int32_t port, IClientCallback::Ptr callback);
    ~TcpClient() = default;
    static TcpClient::Ptr CreateTcpClient(const std::string& peer, int32_t port, IClientCallback::Ptr callback);
    void Send(const std::string& buf);
 protected:
    bool Connect();
private:
    IClientCallback::Ptr callback_;
    const std::string peer_;
    int32_t port_;
    SOCKET socket_;
};

