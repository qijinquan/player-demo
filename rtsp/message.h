#pragma once
#include <string>
#include <map>
#include <inttypes.h>
class Message
{
public:
    Message();
    ~Message();
    virtual int32_t GetStatusCode() = 0;
    virtual std::string& GetMethod() = 0;
    virtual void Parse(std::string& msg) = 0;
    virtual std::string& GetUrl();
    virtual std::string& GetFullUrl();
    bool IsRequest();
protected:
    std::string sdp_;
    std::string url_;
    std::string protocol_;
    std::map<std::string, std::string> headers_;
    std::map<std::string, std::string> urlArgs_;
    bool isRequest_;
    
};
class RequestMsg : public Message
{
public:
    RequestMsg();
    ~RequestMsg();
    virtual int32_t GetStatusCode();
    virtual std::string& GetMethod();
    virtual void Parse(std::string& msg);
private:
    std::string method_;
};

class ResponseMsg : public Message
{
public:
    ResponseMsg();
    ~ResponseMsg();
    virtual int32_t GetStatusCode();
    virtual std::string& GetMethod();
    virtual void Parse(std::string& msg);
private:
    std::string sdp_;
    int32_t status_;
    std::string statusText_;
};