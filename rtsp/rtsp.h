#pragma once
#include <memory>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include "frame/frame.h"
namespace OHOS::Sharing
{

    typedef enum {
        RTP_Invalid = -1,
        RTP_TCP = 0,
        RTP_UDP = 1,
        RTP_MULTICAST = 2,
    } eRtpType;

#define RTP_PT_MAP(XX) \
    XX(PCMU, TrackType::TRACK_AUDIO, 0, 8000, 1, CodecG711U) \
    XX(GSM, TrackType::TRACK_AUDIO , 3, 8000, 1, CodecInvalid) \
    XX(G723, TrackType::TRACK_AUDIO, 4, 8000, 1, CodecInvalid) \
    XX(DVI4_8000, TrackType::TRACK_AUDIO, 5, 8000, 1, CodecInvalid) \
    XX(DVI4_16000, TrackType::TRACK_AUDIO, 6, 16000, 1, CodecInvalid) \
    XX(LPC, TrackType::TRACK_AUDIO, 7, 8000, 1, CodecInvalid) \
    XX(PCMA, TrackType::TRACK_AUDIO, 8, 8000, 1, CodecG711A) \
    XX(G722, TrackType::TRACK_AUDIO, 9, 8000, 1, CodecInvalid) \
    XX(L16_Stereo, TrackType::TRACK_AUDIO, 10, 44100, 2, CodecInvalid) \
    XX(L16_Mono, TrackType::TRACK_AUDIO, 11, 44100, 1, CodecInvalid) \
    XX(QCELP, TrackType::TRACK_AUDIO, 12, 8000, 1, CodecInvalid) \
    XX(CN, TrackType::TRACK_AUDIO, 13, 8000, 1, CodecInvalid) \
    XX(MPA, TrackType::TRACK_AUDIO, 14, 90000, 1, CodecInvalid) \
    XX(G728, TrackType::TRACK_AUDIO, 15, 8000, 1, CodecInvalid) \
    XX(DVI4_11025, TrackType::TRACK_AUDIO, 16, 11025, 1, CodecInvalid) \
    XX(DVI4_22050, TRACK_AUDIO, 17, 22050, 1, CodecInvalid) \
    XX(G729, TrackType::TRACK_AUDIO, 18, 8000, 1, CodecInvalid) \
    XX(CelB, TrackType::TRACK_VIDEO, 25, 90000, 1, CodecInvalid) \
    XX(JPEG, TrackType::TRACK_VIDEO, 26, 90000, 1, CodecInvalid) \
    XX(nv, TrackType::TRACK_VIDEO, 28, 90000, 1, CodecInvalid) \
    XX(H261, TrackType::TRACK_VIDEO, 31, 90000, 1, CodecInvalid) \
    XX(MPV, TrackType::TRACK_VIDEO, 32, 90000, 1, CodecInvalid) \
    XX(MP2T, TrackType::TRACK_VIDEO, 33, 90000, 1, CodecInvalid) \
    XX(H263, TrackType::TRACK_VIDEO, 34, 90000, 1, CodecInvalid) \

    typedef enum {
#define ENUM_DEF(name, type, value, clock_rate, channel, codec_id) PT_ ## name = value,
        RTP_PT_MAP(ENUM_DEF)
#undef ENUM_DEF
        PT_MAX = 128
    } PayloadType;

class SdpTrack
{
public:
    using Ptr = std::shared_ptr<SdpTrack>;
    std::string t_;
    std::string b_;
    uint16_t port_;

    float duration_ = 0;
    float start_ = 0;
    float end_ = 0;

    std::map<char, std::string> other_;
    std::multimap<std::string, std::string> attr_;

    std::string ToString(uint16_t port = 0) const;
    std::string GetName() const;
    std::string GetControlUrl(const std::string& baseUrl) const;
public:
    int32_t pt_;
    int32_t channel_;
    int samplerate_;
    TrackType type_;
    std::string codec_;
    std::string fmtp_;
    std::string control_;
public:
    bool inited_ = false;
    uint8_t interleaved_ = 0;
    uint16_t seq_ = 0;
    uint32_t ssrc_ = 0;
    uint32_t timeStamp_ = 0;
};

class SdpParser
{
public:
    using Ptr = std::shared_ptr<SdpParser>;

    SdpParser() {}
    SdpParser(const std::string &sdp) { Load(sdp); }
    ~SdpParser() {}

    void Load(const std::string &sdp);
    bool Available() const;
    SdpTrack::Ptr GetTrack(TrackType type) const;
    std::vector<SdpTrack::Ptr> GetAvailableTrack() const;
    std::string toString() const;

private:
    std::vector<SdpTrack::Ptr> trackVec_;
};
class Sdp : public CodecInfo {
public:
    using Ptr = std::shared_ptr<Sdp>;
    Sdp(uint32_t sampleRate, uint8_t payloadType) {
        sampleRate_ = sampleRate;
        payloadType_ = payloadType;
    }
    virtual ~Sdp() {}
    virtual std::string GetSdp() const = 0;
    uint8_t GetPayloadType() const {
        return payloadType_;
    }
    uint32_t GetSampleRate() const {
        return sampleRate_;
    }
private:
    uint8_t payloadType_;
    uint32_t sampleRate_;
};

class TitleSdp : public Sdp {
public:
    TitleSdp(float durSec = 0,
        const std::map<std::string, std::string> &header = std::map<std::string, std::string>(),
        int version = 0) : Sdp(0, 0) {
        std::stringstream stream;
        stream << "v=" << version << "\r\n";
        if (!header.empty()) {
            for (auto &[key, value] : header)
            {
                stream << key << "=" << value << "\r\n";
            }
        }
        else
        {
            stream << "o=- 0 0 IN IP4 0.0.0.0\r\n";
            stream << "s=Streamed by " << "TODO-OpenHarmony" << "\r\n";
            stream << "c=IN IP4 0.0.0.0\r\n";
            stream << "t=0 0\r\n";
        }
        if (durSec <= 0)
        {
            stream << "a=range:npt=now-\r\n";
        }
        else {
            durSec_ = durSec;
            stream << "a=range:npt=0-" << durSec_ << "\r\n";
        }
        stream << "a=control:*\r\n";
        sdp_ = stream.str();
    }
    std::string GetSdp() const override {
        return sdp_;
    }

    CodecId GetCodecId() const override {
        return CodecInvalid;
    }

    float GetDuration() const {
        return durSec_;
    }
private:
    float durSec_ = 0;
    std::string sdp_;
};
class RtpPayload {
public:
    static int GetClockRate(int pt);
    static TrackType GetTrackType(int pt);
    static int GetAudioChannel(int pt);
    static const char *GetName(int pt);
    static CodecId GetCodecId(int pt);

private:
    RtpPayload() = delete;
    ~RtpPayload() = delete;
};
}
