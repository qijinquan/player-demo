#ifndef SHARING_SDP_PARSER_H
#define SHARING_SDP_PARSER_H
#include <map>
#include <memory>
#include <string>
#include <vector>
#include "frame/frame.h"
namespace OHOS::Sharing {
//a = rtpmap:96 mpeg4-generic/12000/2
struct SdpRtpMap
{
    using Ptr = std::shared_ptr<SdpRtpMap>;
    int32_t ParseLine(std::string& rtpMap);
    int32_t channels;
    std::string format;
    int32_t feq;
    int32_t pt;
};

//a = fmtp:96 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=149056e500
struct SdpFmtp
{
    using Ptr = std::shared_ptr<SdpFmtp>;
    int32_t pt;
    std::string value;
    int32_t ParseLine(std::string& line);
};

//m=audio 0 RTP/AVP 96
//a=
//a=
struct SdpMedia
{
    using Ptr = std::shared_ptr<SdpMedia>;
    int32_t ParseLine(std::string &line);
    int32_t ParseAttr(std::string &line);
    std::map<std::string, std::string> attr;
    //std::vector<SdpFmtp::Ptr> fmtps;
    SdpFmtp::Ptr fmtp;
    std::string control;

    std::string mediaType; //audio video and so on
    int32_t mediaPort;      //默认是0
    std::string protocol; //AVP/RTP
    int32_t pt;
    SdpRtpMap::Ptr rtpMap;
};

class SdpParser {
public:
    using Ptr = std::shared_ptr<SdpParser>;
    SdpParser() = default;
    SdpParser(const std::string &sdp);
    ~SdpParser() = default;
    void LoadSdp(const std::string &sdp);
    std::vector<SdpMedia::Ptr>& GetAllMedias();
    void Clear();
private:
    int32_t ParseLine(char type, std::string& line);
private:
    std::string connectAddr_;
    std::string name_;
    std::string origin_;
    uint32_t version_;
    std::vector<SdpMedia::Ptr> medias_;
    std::map<std::string, std::string> attr;
    float duration_;
    float start_;
    float end_;
};
}  // namespace OHOS::Sharing
#endif  // SHARING_SDP_PARSER_H