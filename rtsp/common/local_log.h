#ifndef SHARING_LOCAL_LOG_H
#define SHARING_LOCAL_LOG_H
#include <cstdarg>
namespace OHOS {
namespace Sharing {
void LocalLogWrite(const char* loglevel, const char* fmt, ...);

#define F_SHARING_LOCAL_LOG(loglevel, fmt, ...) \
LocalLogWrite(loglevel, "[Sharing]{%{public}s():%{public}d} " fmt, __FUNCTION__, __LINE__, __VA_ARGS__);

#define SHARING_LOGD(fmt, ...) F_SHARING_LOCAL_LOG("DEBUG", fmt, __VA_ARGS__)
#define SHARING_LOGI(fmt, ...) F_SHARING_LOCAL_LOG("INFO", fmt, __VA_ARGS__)
#define SHARING_LOGW(fmt, ...) F_SHARING_LOCAL_LOG("WARN", fmt, __VA_ARGS__)
#define SHARING_LOGE(fmt, ...) F_SHARING_LOCAL_LOG("ERROR", fmt, __VA_ARGS__)
#define SHARING_LOGF(fmt, ...) F_SHARING_LOCAL_LOG("FATAL", fmt, __VA_ARGS__)

} // namespace Sharing
} // namespace OHOS
#endif