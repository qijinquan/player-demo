#ifndef SHARING_COMMON_H
#define SHARING_COMMON_H

#include <string>

namespace OHOS {
namespace Sharing {
class Object
{
public:
	Object() {}
	virtual ~Object() {}

	void SetClassName(const std::string& name) { className_ = name; }
	std::string GetClassName() const { return className_; }

protected:
	std::string className_;
};
} // namespace Sharing
} // namespace OHOS
#endif