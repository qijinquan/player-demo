#ifndef SHARING_LOG_LOG_H
#define SHARING_LOG_LOG_H

#ifndef SHARING_LOCAL_LOG
#define SHARING_LOCAL_LOG
#endif

#ifdef SHARING_LOCAL_LOG
#include "local_log.h"
#else
#include "sharing_log.h"
#endif

#endif