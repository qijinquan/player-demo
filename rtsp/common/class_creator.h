#ifndef SHARING_CLASS_FACTORY_H
#define SHARING_CLASS_FACTORY_H

#include <unordered_map>
#include <string>
#include <mutex>
#include "singleton.h"

namespace OHOS {
namespace Sharing {
using ClassCreateFun = void *(*)(void);

class ClassFactory : public Singleton<ClassFactory> {
    friend class Singleton<ClassFactory>; 
public:
    void RegisterClass(const std::string& name, ClassCreateFun creator);

    void Unregister(const std::string& name);

    void *NewInstance(const std::string& name);

public:
    std::unordered_map<std::string, ClassCreateFun> register_;
    std::mutex mutex_;
};

class ClassRegister {
public:
    ClassRegister(const std::string &name, ClassCreateFun func)
    {
        ClassFactory::GetInstance().RegisterClass(name, func);
    };

private:
    ClassRegister() = delete;
};

template<class T>
class ClassCreator {
public:
    static T *NewInstance(const std::string &name)
    {
        return (T *)ClassFactory::GetInstance().NewInstance(name);
    };

    static std::shared_ptr<T> NewSharedInstance(const std::string &name)
    {
        return std::shared_ptr<T>((T *)ClassFactory::GetInstance().NewInstance(name));
    };

    static std::shared_ptr<T> NewUniqueInstance(const std::string &name)
    {
        return std::unique_ptr<T>((T *)ClassFactory::GetInstance().NewInstance(name));
    };
};

#define REGISTER_CLASS_CREATOR(className) \
    static void *className##Creator(void) \
    {                                      \
        return (void *)(new className()); \
    }                                      \
    const ClassRegister className##Register(#className, className##Creator)

} // namespace Sharing
} // namespace OHOS
#endif