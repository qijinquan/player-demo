#include "class_creator.h"
#include "log.h"

namespace OHOS {
namespace Sharing {
void ClassFactory::RegisterClass(const std::string &name, ClassCreateFun func)
{
    std::lock_guard<std::mutex> lock(mutex_);
    if (name == "") {
        return;
    }
    SHARING_LOGD("[ClassFactory] RegisterClass name=%{public}s", name.c_str());
    register_.emplace(name, func);
    SHARING_LOGD("[ClassFactory] RegisterClass ok");
}

void *ClassFactory::NewInstance(const std::string &name)
{
    std::lock_guard<std::mutex> lock(mutex_);
    auto it = register_.find(name);
    if (it == register_.end()) {
        return nullptr;
    } else {
        return it->second();
    }
}
} // namespace Sharing
} // namespace OHOS