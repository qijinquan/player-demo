#include "local_log.h"
#include <string>
#include <stdarg.h>

namespace OHOS {
namespace Sharing {
std::string ReplaceStr(const std::string& str, const std::string& src, const std::string& dst)
{
    if (src.empty()) {
        return str;
    }

    std::string::size_type pos = 0;
    std::string strTmp = str;
    while ((pos = strTmp.find(src, pos)) != std::string::npos) {
        strTmp.replace(pos, src.length(), dst);
        pos += dst.length();
    }

    return strTmp;
}

void LocalLogWrite(const char* loglevel, const char* fmt, ...)
{
    std::string strFormat = fmt;
    strFormat = ReplaceStr(strFormat, "{public}", "");

    char buff[1024] = "";
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buff,1024, strFormat.c_str(), ap);
    va_end(ap);

    printf("[%s]%s\r\n", loglevel, buff);
}
} // namespace Sharing
} // namespace OHOS