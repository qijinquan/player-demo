#ifndef __BUFFER_H__
#define __BUFFER_H__

#include <cassert>
#include <memory>
#include <string>
#include <vector>
#include <type_traits>
#include <functional>
#include <cstdlib>
#include <string.h>

template <typename T> struct is_pointer : public std::false_type {};
template <typename T> struct is_pointer<std::shared_ptr<T> > : public std::true_type {};
template <typename T> struct is_pointer<std::shared_ptr<T const> > : public std::true_type {};
template <typename T> struct is_pointer<T*> : public std::true_type {};
template <typename T> struct is_pointer<const T*> : public std::true_type {};

namespace OHOS {
namespace Sharing {

//缓存基类
class Buffer {
public:
    using Ptr = std::shared_ptr<Buffer>;

    Buffer() = default;
    virtual ~Buffer() = default;

    //返回数据长度
    virtual char *Data() const = 0;
    virtual size_t Size() const = 0;

    virtual std::string ToString() const {
        return std::string(Data(), Size());
    }

    virtual size_t GetCapacity() const {
        return Size();
    }
    protected:
};

template <typename C>
class BufferOffset : public  Buffer {
public:
    using Ptr = std::shared_ptr<BufferOffset>;

    BufferOffset(C data, size_t offset = 0, size_t len = 0) : data_(std::move(data)) {
        Setup(offset, len);
    }

    ~BufferOffset() override = default;

    char *Data() const override {
        return const_cast<char *>(GetPointer<C>(data_)->Data()) + offset_;
    }

    size_t Size() const override {
        return size_;
    }

    std::string ToString() const override {
        return std::string(Data(), Size());
    }

private:
    void Setup(size_t offset = 0, size_t size = 0) {
        auto max_size = GetPointer<C>(data_)->Size();
        assert(offset + size <= max_size);
        if (!size) {
            size = max_size - offset;
        }
        size_ = size;
        offset_ = offset;
    }

    template<typename T>
    static typename std::enable_if<::is_pointer<T>::value, const T &>::type
    GetPointer(const T &data) {
        return data;
    }

    template<typename T>
    static typename std::enable_if<!::is_pointer<T>::value, const T *>::type
    GetPointer(const T &data) {
        return &data;
    }

private:
    C data_;
    size_t size_;
    size_t offset_;
};

using BufferString = BufferOffset<std::string>;

//指针式缓存对象，
class BufferRaw : public Buffer {
public:
    using Ptr = std::shared_ptr<BufferRaw>;

    static Ptr create();

    ~BufferRaw() override {
        if (data_) {
            delete[] data_;
        }
    }

    //在写入数据时请确保内存是否越界
    char *Data() const override {
        return data_;
    }

    //有效数据大小
    size_t Size() const override {
        return size_;
    }

    //分配内存大小
    void SetCapacity(size_t capacity) {
        if (data_) {
            do {
                if (capacity > capacity_) {
                    //请求的内存大于当前内存，那么重新分配
                    break;
                }

                if (capacity_ < 2 * 1024) {
                    //2K以下，不重复开辟内存，直接复用
                    return;
                }

                if (2 * capacity > capacity_) {
                    //如果请求的内存大于当前内存的一半，那么也复用
                    return;
                }
            } while (false);

            delete[] data_;
        }
        data_ = new char[capacity];
        capacity_ = capacity;
    }

    //设置有效数据大小
    virtual void SetSize(size_t size) {
        if (size > capacity_) {
        }
        size_ = size;
    }

    //赋值数据
    void Assign(const char *data, size_t size = 0) {
        if (size <= 0) {
            size = strlen(data);
        }
        SetCapacity(size + 1);
        memcpy(data_, data, size);
        data_[size] = '\0';
        SetSize(size);
    }

    size_t GetCapacity() const override {
        return capacity_;
    }

protected:
    BufferRaw(size_t capacity = 0) {
        if (capacity) {
            SetCapacity(capacity);
        }
    }

    BufferRaw(const char *data, size_t size = 0) {
        Assign(data, size);
    }

private:
    size_t size_ = 0;
    size_t capacity_ = 0;
    char *data_ = nullptr;
};

class BufferLikeString : public Buffer {
public:
    ~BufferLikeString() override = default;

    BufferLikeString() {
        eraseHead_ = 0;
        eraseTail_ = 0;
    }

    BufferLikeString(std::string str) {
        str_ = std::move(str);
        eraseHead_ = 0;
        eraseTail_ = 0;
    }

    BufferLikeString &operator=(std::string str) {
        str_ = std::move(str);
        eraseHead_ = 0;
        eraseTail_ = 0;
        return *this;
    }

    BufferLikeString(const char *str) {
        str_ = str;
        eraseHead_ = 0;
        eraseTail_ = 0;
    }

    BufferLikeString &operator=(const char *str) {
        str_ = str;
        eraseHead_ = 0;
        eraseTail_ = 0;
        return *this;
    }

    BufferLikeString(BufferLikeString &&that) {
        str_ = std::move(that.str_);
        eraseHead_ = that.eraseHead_;
        eraseTail_ = that.eraseTail_;
        that.eraseHead_ = 0;
        that.eraseTail_ = 0;
    }

    BufferLikeString &operator=(BufferLikeString &&that) {
        str_ = std::move(that.str_);
        eraseHead_ = that.eraseHead_;
        eraseTail_ = that.eraseTail_;
        that.eraseHead_ = 0;
        that.eraseTail_ = 0;
        return *this;
    }

    BufferLikeString(const BufferLikeString &that) {
        str_ = that.str_;
        eraseHead_ = that.eraseHead_;
        eraseTail_ = that.eraseTail_;
    }

    BufferLikeString &operator=(const BufferLikeString &that) {
        str_ = that.str_;
        eraseHead_ = that.eraseHead_;
        eraseTail_ = that.eraseTail_;
        return *this;
    }

    char *Data() const override {
        return (char *) str_.data() + eraseHead_;
    }

    size_t Size() const override {
        return str_.size() - eraseTail_ - eraseHead_;
    }

    BufferLikeString &Erase(size_t pos = 0, size_t n = std::string::npos) {
        if (pos == 0) {
            //移除前面的数据
            if (n != std::string::npos) {
                //移除部分
                if (n > Size()) {
                    //移除太多数据了
                    //throw std::out_of_range("BufferLikeString::erase out_of_range in head");
                }
                //设置起始便宜量
                eraseHead_ += n;
                Data()[Size()] = '\0';
                return *this;
            }
            //移除全部数据
            eraseHead_ = 0;
            eraseTail_ = str_.size();
            Data()[0] = '\0';
            return *this;
        }

        if (n == std::string::npos || pos + n >= Size()) {
            //移除末尾所有数据
            if (pos >= Size()) {
                //移除太多数据
                //throw std::out_of_range("BufferLikeString::erase out_of_range in tail");
            }
            eraseTail_ += Size() - pos;
            Data()[Size()] = '\0';
            return *this;
        }

        //移除中间的
        if (pos + n > Size()) {
            //超过长度限制
            //throw std::out_of_range("BufferLikeString::erase out_of_range in middle");
        }
        str_.erase(eraseHead_ + pos, n);
        return *this;
    }

    BufferLikeString &Append(const BufferLikeString &str) {
        return Append(str.Data(), str.Size());
    }

    BufferLikeString &Append(const std::string &str) {
        return Append(str.data(), str.size());
    }

    BufferLikeString &Append(const char *data) {
        return Append(data, strlen(data));
    }

    BufferLikeString &Append(const char *data, size_t len) {
        if (len <= 0) {
            return *this;
        }
        if (eraseHead_ > str_.capacity() / 2) {
            MoveData();
        }
        if (eraseTail_ == 0) {
            str_.append(data, len);
            return *this;
        }
        str_.insert(eraseHead_ + Size(), data, len);
        return *this;
    }

    void PushBack(char c) {
        if (eraseTail_ == 0) {
            str_.push_back(c);
            return;
        }
        Data()[Size()] = c;
        --eraseTail_;
        Data()[Size()] = '\0';
    }

    BufferLikeString &Insert(size_t pos, const char *s, size_t n) {
        str_.insert(eraseHead_ + pos, s, n);
        return *this;
    }

    BufferLikeString &Assign(const char *data) {
        return Assign(data, strlen(data));
    }

    BufferLikeString &Assign(const char *data, size_t len) {
        if (len <= 0) {
            return *this;
        }
        if (data >= str_.data() && data < str_.data() + str_.size()) {
            eraseHead_ = data - str_.data();
            if (data + len > str_.data() + str_.size()) {
                //throw std::out_of_range("BufferLikeString::assign out_of_range");
            }
            eraseTail_ = str_.data() + str_.size() - (data + len);
            return *this;
        }
        str_.assign(data, len);
        eraseHead_ = 0;
        eraseTail_ = 0;
        return *this;
    }

    void Clear() {
        eraseHead_ = 0;
        eraseTail_ = 0;
        str_.clear();
    }

    char &operator[](size_t pos) {
        if (pos >= Size()) {
            //throw std::out_of_range("BufferLikeString::operator[] out_of_range");
        }
        return Data()[pos];
    }

    const char &operator[](size_t pos) const {
        return (*const_cast<BufferLikeString *>(this))[pos];
    }

    size_t Capacity() const {
        return str_.capacity();
    }

    void Reserve(size_t size) {
        str_.reserve(size);
    }

    void Resize(size_t size, char c = '\0') {
        str_.resize(size, c);
        eraseHead_ = 0;
        eraseTail_ = 0;
    }

    bool Empty() const {
        return Size() <= 0;
    }

    std::string substr(size_t pos, size_t n = std::string::npos) const {
        if (n == std::string::npos) {
            //获取末尾所有的
            if (pos >= Size()) {
                //throw std::out_of_range("BufferLikeString::substr out_of_range");
            }
            return str_.substr(eraseHead_ + pos, Size() - pos);
        }

        //获取部分
        if (pos + n > Size()) {
            //throw std::out_of_range("BufferLikeString::substr out_of_range");
        }
        return str_.substr(eraseHead_ + pos, n);
    }

private:
    void MoveData() {
        if (eraseHead_) {
            str_.erase(0, eraseHead_);
            eraseHead_ = 0;
        }
    }

private:
    size_t eraseHead_;
    size_t eraseTail_;
    std::string str_;
};
}
}

#endif // __BUFFER_H__