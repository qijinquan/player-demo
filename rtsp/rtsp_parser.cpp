#include "rtsp_parser.h"
#include <sstream>
#include <vector>
#include "utils/utils.h"
namespace OHOS::Sharing {
int32_t RtspMessage::Parse(std::string &packet)
{
    msg_ = packet;
    //分离header 和 sdp
    std::vector<std::string> vec = Split(packet, "\r\n\r\n");
    if (vec.size() > 1) {
        sdp_ = vec[1];
    }
    auto line = vec[0];
    vec.clear();

    vec = Split(line, "\r\n");
    ParseCommandLine(vec[0]);
    for (size_t i = 1; i < vec.size(); i++) {
        ParseHeader(vec[i]);
    }
    return 0;
}

StatusCode RtspMessage::GetStatusCode() const
{
    return statusCode_;
}

const std::string &RtspMessage::GetMethod() const
{
    return method_;
}
const std::string &RtspMessage::Url() const
{
    return url_;
}
const std::string &RtspMessage::FullUrl() const
{
    return url_;
    // TODO:
}
void RtspMessage::Clear()
{
    method_ = "";
    url_ = "";
    params_ = "";
    statusCode_ = StatusCode::Status_Code_INVALID;
    statusText_ = "";
    headers_.clear();
    urlArgs_.clear();
}
const std::string &RtspMessage::operator[](std::string name) const
{
    auto it = headers_.find(name);
    if (it == headers_.end()) {
        return empty_;
    }
    return it->second;
}
std::map<std::string, std::string> &RtspMessage::GetHeaders()
{
    return headers_;
    // TODO:
}
std::map<std::string, std::string> &RtspMessage::GetUrlArgs()
{
    return urlArgs_;
}
std::string &RtspMessage::ToString()
{
    //第二次调用会返回一样的msg， 不能修改
    if (msg_.empty()) {
        std::stringstream stream;
        if (isRequest_) {
            stream << method_ << " " << url_ << " " << protocol_ << "\r\n";
        } else {
            stream << protocol_ << " " << std::to_string((int)statusCode_) << " " << statusText_ << "\r\n";
        }
        for (auto &[key, value] : headers_) {
            stream << key << ": " << value << "\r\n";
        }
        stream << "\r\n";
        if (sdp_.empty()) {
            stream << sdp_;
        }
        msg_ = stream.str();
    }
    return msg_;
}
const std::string &RtspMessage::GetSdp() const
{
    return sdp_;
}
bool RtspMessage::HandleAuthenticationFailure()
{
    // res need auth, if is request, return
    if (isRequest_) {
        return false;
    }
    // Have authed, but failed
    if (!realm_.empty()) {
        return false;
    }
    std::string authInfo = headers_["WWW-Authenticate"];
    if (authInfo.empty()) {
        return false;
    }
    return true;
}

bool RtspMessage::IsRequest()
{
    return isRequest_;
}

int32_t RtspMessage::ParseUrl()
{
    return 0;
}
int32_t RtspMessage::ParseHeader(std::string &line)
{
    std::vector<std::string> vec = SplitOnce(line, ":");
    if (vec.size() == 2) {  // 2 for header line param num
        headers_.emplace(Trim(vec[0]), Trim(vec[1]));
    }
    return 0;
}
int32_t RtspMessage::ParseCommandLine(std::string &line)
{
    std::vector<std::string> vec = Split(line, " ");
    if (vec.size() != 3) {  // 3 for command line param num
        return -1;
    }
    if (vec[0].find("RTSP") == std::string::npos) {
        isRequest_ = true;
        method_ = vec[0];
        url_ = vec[1];  // TODO: fullUrl
        protocol_ = vec[2];
    } else {
        isRequest_ = false;
        protocol_ = vec[0];
        statusCode_ = (StatusCode)std::stoi(vec[1]);
        statusText_ = vec[2];
    }
    return 0;
}
}  // namespace OHOS::Sharing