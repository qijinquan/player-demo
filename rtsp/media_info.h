#ifndef MEDIA_INFO_H
#define MEDIA_INFO_H
#include <memory>
#include <string>
namespace OHOS::Sharing {
class MediaFormat {
public:
    using Ptr = std::shared_ptr<MediaFormat>;
    MediaFormat()
    {
        bitRate_ = 0;
        channelCount_ = 0;
        codecType = 0;
        frameRate_ = 0;
        payloadType_ = 0;

        sampleRate_ = 0;
        height_ = 0;
        width_ = 0;
    }

    virtual ~MediaFormat()
    {}

public:
    virtual void Clear()
    {
        bitRate_ = 0;
        channelCount_ = 0;
        codecType = 0;
        frameRate_ = 0;
        payloadType_ = 0;

        sampleRate_ = 0;
        height_ = 0;
        width_ = 0;

        codecName_.clear();
        fmtp_.clear();
        mediaType_.clear();
    }

public:
    std::string codecName_;  ///< Track format name / 当前媒体格式名称
    std::string fmtp_;       ///< RTP SDP FMTP 属性
    std::string mediaType_;  ///< Media type, audio/video

    uint32_t bitRate_;       ///<
    uint32_t channelCount_;  ///< Audio Channel Count / 音频通道数
    uint32_t codecType;      ///< Codec Type, 编码类型
    uint32_t frameRate_;     ///<
    uint32_t payloadType_;   ///< RTP 负载格式
    uint32_t sampleRate_;    ///< RTP 频率
    uint32_t height_;        ///< Video height
    uint32_t width_;         ///< Video width
};
}  // namespace OHOS::Sharing

#endif