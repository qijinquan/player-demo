#ifndef SHARING_RTSP_PARSER_H
#define SHARING_RTSP_PARSER_H
#include <inttypes.h>
#include <map>
#include <string>
namespace OHOS::Sharing {
enum class StatusCode : uint32_t {
    Status_Code_INVALID = 0,
    Status_Code_Continue = 100,  ///< Continue

    Status_Code_OK = 200,              ///< SuccessOK
    Status_Code_Created = 201,         ///< SuccessCreated
    Status_Code_Accepted = 202,        ///< SuccessAccepted
    Status_Code_NoContent = 204,       ///< SuccessNoContent
    Status_Code_PartialContent = 206,  ///< SuccessPartialContent
    Status_Code_LowOnStorage = 250,    ///< SuccessLowOnStorage
    Status_Code_NoResponse = 255,      ///< NoResponse

    Status_Code_MultipleChoices = 300,  ///< MultipleChoices
    Status_Code_PermMoved = 301,        ///< RedirectPermMoved
    Status_Code_TempMoved = 302,        ///< RedirectTempMoved
    Status_Code_SeeOther = 303,         ///< RedirectSeeOther
    Status_Code_NotModified = 304,      ///< RedirectNotModified
    Status_Code_UseProxy = 305,         ///< UseProxy

    Status_Code_BadRequest = 400,       ///< ClientBadRequest
    Status_Code_UnAuthorized = 401,     ///< ClientUnAuthorized
    Status_Code_PaymentRequired = 402,  ///< PaymentRequired
    Status_Code_Forbidden = 403,        ///< ClientForbidden
    Status_Code_NotFound = 404,         ///< ClientNotFound

    /**
     * 请求的方法不允许用于请求 URI 指定的资源. 应答消息中必须包含一个 Allow
     * 字段指定这个资源允许的所有方法.
     */
    Status_Code_MethodNotAllowed = 405,             ///< ClientMethodNotAllowed
    Status_Code_NotAcceptable = 406,                ///< NotAcceptable
    Status_Code_ProxyAuthenticationRequired = 407,  ///< ProxyAuthenticationRequired
    Status_Code_RequestTimeout = 408,               ///< RequestTimeout
    Status_Code_Conflict = 409,                     ///< ClientConflict
    Status_Code_Gone = 410,                         ///< Gone
    Status_Code_LengthRequired = 411,               ///< LengthRequired
    Status_Code_PreconditionFailed = 412,           ///< PreconditionFailed
    Status_Code_RequestEntityTooLarge = 413,        ///< RequestEntityTooLarge
    Status_Code_RequestURITooLarge = 414,           ///< RequestURITooLarge
    Status_Code_UnsupportedMediaType = 415,         ///< UnsupportedMediaType

    /** 请求的接受方不支持请求中的一个或多个参数. */
    Status_Code_ParameterNotUnderstood = 451,  ///< ClientParameterNotUnderstood

    /** 媒体服务器不知道 Conference 字段指定的 Conference. */
    Status_Code_ConferenceNotFound = 452,  ///< ClientConferenceNotFound

    /** 这个请求因为没有足够的带宽而被拒绝. */
    Status_Code_NotEnoughBandwidth = 453,  ///< ClientNotEnoughBandwidth

    /** Session 字段指定的 RTSP 会话已丢失, 无效的或者已经过期了. */
    Status_Code_SessionNotFound = 454,  ///< ClientSessionNotFound

    /** 客户端或服务端不存在当前状态下处理这个请求. */
    Status_Code_MethodNotValidInState = 455,  ///< ClientMethodNotValidInState

    /** 服务端无法处理指定的头字段. */
    Status_Code_HeaderFieldNotValid = 456,  ///< ClientHeaderFieldNotValid

    /** 给定的 Range 值超出范围. */
    Status_Code_InvalidRange = 457,  ///< ClientInvalidRange

    /** 在 SET_PARAMETER 请求中要设置的参数只能读不能修改. */
    Status_Code_Status_Code_ReadOnlyParameter = 458,          ///< ClientReadOnlyParameter
    Status_Code_Status_Code_AggregateOptionNotAllowed = 459,  ///< ClientAggregateOptionNotAllowed
    Status_Code_Status_Code_AggregateOptionAllowed = 460,     ///< ClientAggregateOptionAllowed

    /** Transport 头字段中不包含支持的传输方式. */
    Status_Code_UnsupportedTransport = 461,  ///< ClientUnsupportedTransport

    /** 客户端地址无法到达, 所以无法建立数据传输通道. */
    Status_Code_DestinationUnreachable = 462,  ///< ClientDestinationUnreachable

    Status_Code_InternalError = 500,            ///< ServerInternal
    Status_Code_NotImplemented = 501,           ///< ServerNotImplemented
    Status_Code_BadGateway = 502,               ///< ServerBadGateway
    Status_Code_ServerUnavailable = 503,        ///< ServerUnavailable
    Status_Code_GatewayTimeout = 504,           ///< ServerGatewayTimeout
    Status_Code_RTSPVersionNotSupported = 505,  ///< RTSPVersionNotSupported

    /** 不支持 Require 或 Proxy-Require 头字段中给定的选项. */
    Status_Code_OptionNotSupported = 551  ///< OptionNotSupported
};
class RtspMessage {
public:
    RtspMessage(bool request = false) : isRequest_(request)
    {}
    ~RtspMessage() = default;
    int32_t Parse(std::string &packet);
    StatusCode GetStatusCode() const;
    const std::string &GetMethod() const;
    const std::string &Url() const;
    const std::string &FullUrl() const;
    const std::string &operator[](std::string name) const;
    std::map<std::string, std::string> &GetHeaders();
    std::map<std::string, std::string> &GetUrlArgs();
    std::string& ToString();
    const std::string& GetSdp() const;
    bool HandleAuthenticationFailure();
    void Clear();
    bool IsRequest();
private:
    int32_t ParseUrl();
    int32_t ParseHeader(std::string& line);
    int32_t ParseCommandLine(std::string& line);

private:
    bool isRequest_;
    StatusCode statusCode_ = StatusCode::Status_Code_INVALID;
    std::string statusText_;
    std::string method_;
    std::string url_;
    std::string protocol_;
    std::string params_;
    std::string sdp_;
    std::string realm_;
    std::map<std::string, std::string> headers_;
    std::map<std::string, std::string> urlArgs_;
    std::string msg_;
    std::string empty_ = "";
};
}  // namespace OHOS::Sharing
#endif  // SHARING_RTSP_PARSER_H