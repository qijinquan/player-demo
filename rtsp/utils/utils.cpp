#include "utils.h"
#include <cstdlib>
#include <string.h>

namespace OHOS {
namespace Sharing {
std::vector<std::string> Split(const std::string &s, const char *delim) 
{
    std::vector<std::string> ret;
    size_t last = 0;
    auto index = s.find(delim, last);
    while (index != std::string::npos) {
        if (index - last > 0) {
            ret.push_back(s.substr(last, index - last));
        }
        last = index + strlen(delim);
        index = s.find(delim, last);
    }
    if (!s.size() || s.size() - last > 0) {
        ret.push_back(s.substr(last));
    }
    return ret;
}
std::vector<std::string> SplitOnce(const std::string &s, const char *delim)
{
    std::vector<std::string> ret;
    size_t last = 0;
    auto index = s.find(delim, last);
    if (index != std::string::npos)
    {
        ret.push_back(s.substr(0, index));
        ret.push_back(s.substr(index+1));
    }
    else
    {
        ret.push_back(s);
    }
    return ret;
}

#define TRIM(s, chars) \
do{ \
    std::string map(0xFF, '\0'); \
    for (auto &ch : chars) { \
        map[(unsigned char &)ch] = '\1'; \
    } \
    while(s.size() && map.at((unsigned char &)s.back())) s.pop_back(); \
    while(s.size() && map.at((unsigned char &)s.front())) s.erase(0,1); \
}while(0);

//去除前后的空格、回车符、制表符
std::string &Trim(std::string &s, const std::string &chars) 
{
    TRIM(s, chars);
    return s;
}

std::string Trim(std::string &&s, const std::string &chars) 
{
    TRIM(s, chars);
    return std::move(s);
}

}  // namespace Sharing
}  // namespace OHOS