#ifndef __UTILS_H__
#define __UTILS_H__
#include <string>
#include <vector>
namespace OHOS {
namespace Sharing {

std::vector<std::string> Split(const std::string &s, const char *delim);
std::vector<std::string> SplitOnce(const std::string &s, const char *delim);
//去除前后的空格、回车符、制表符...
std::string &Trim(std::string &s, const std::string &chars = " \r\n\t");
std::string Trim(std::string &&s, const std::string &chars = " \r\n\t");

}  // namespace Sharing
}  // namespace OHOS

#endif // __UTILS_H__