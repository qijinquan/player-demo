#include "uri.h"
#include "utils/utils.h"
namespace OHOS::Sharing {
Uri::Uri()
{}
Uri::Uri(std::string &url)
{
    Parse(url);
}
std::string &Uri::GetHost()
{
    return host_;
}
std::string &Uri::GetHref()
{
    return href_;
}

std::string &Uri::GetPath()
{
    return path_;
}

std::string &Uri::GetProtocol()
{
    return protocol_;
}
std::string &Uri::GetHostName()
{
    return host_;
}
uint32_t Uri::GetPort()
{
    return port_;
}

void Uri::SetHost(std::string &host)
{
    host_ = host;
}
void Uri::SetHref(std::string &href)
{
    href_ = href;
}
void Uri::SetProtocol(std::string &protocol)
{
    protocol_ = protocol;
}
void Uri::SetPath(std::string &pathName)
{
    path_ = pathName;
}
void Uri::SetUserName(std::string &userName)
{
    userName_ = userName;
}
void Uri::SetPassword(std::string &password)
{
    passWord_ = password;
}
void Uri::SetPort(uint32_t port)
{
    port_ = port;
}
bool Uri::Parse(const std::string &url)
{
    if (url.empty()) {
        return false;
    }

    href_ = url;
    auto vec = Split(url, "://");
    if (vec.size() != 2) {
        return false;
    }

    protocol_ = vec[0];
    auto tmpUrl = vec[1];
    auto pos = tmpUrl.find('/');
    auto host = tmpUrl.substr(0, pos);
    path_ = tmpUrl.substr(pos + 1);

    vec = Split(host, "@");

    host = vec[vec.size() - 1];
    if (vec.size() == 2) {
        vec = Split(vec[0], ":");
        if (vec.size() != 2) {
            return false;
        }

        userName_ = vec[0];
        passWord_ = vec[1];
    }
    vec = Split(host, ":");
    if (vec.size() != 2)
    {
        return false;
    }
    host_ = vec[0];
    port_ = std::atoi(vec[1].c_str());
    return true;
}
}  // namespace OHOS::Sharing