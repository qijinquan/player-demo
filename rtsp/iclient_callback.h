#pragma once
#include <string>
class IClientCallback :public std::enable_shared_from_this<IClientCallback>
{
public:
    using Ptr = std::shared_ptr<IClientCallback>;
    IClientCallback() = default;
    virtual ~IClientCallback() = default;
public:
    virtual void OnClientConnectResult(bool isSuccess) = 0;
    virtual void OnClientReadData(int32_t fd, const std::string &buf) = 0;
    virtual void OnClientWriteable(int32_t fd) = 0;
    virtual void OnClientClose(int32_t fd) = 0;
    virtual void OnClientException(int32_t fd) = 0;
};