#include "message.h"
#include <vector>
#include <iostream>
#include <regex>
using namespace std;

bool static Split(string& src, string sep, vector<string>& vec)
{
    std::regex pattern(sep);
    const std::sregex_token_iterator end;
    sregex_token_iterator it(src.begin(), src.end(), pattern, -1);
    while (it != end)
    {
        vec.push_back(*it++);
    }
    return true;
}
//首位去空格
static string& Trim(string& src)
{
    src.erase(0, src.find_first_not_of(" "));
    src.erase(src.find_last_not_of(" ")+1);
    return src;
}

static void TrimAll(string& src)
{
    string::size_type index = 0;
    while ((index = src.find(" ", index))!= string::npos)
    {
        src.erase(index, 1);
    }
}
Message::Message()
{

}

Message::~Message()
{

}


std::string & Message::GetUrl()
{
    return url_;
}

std::string & Message::GetFullUrl()
{
    // TODO: 在此处插入 return 语句
    return url_;
}

bool Message::IsRequest()
{
    return isRequest_;
}


RequestMsg::RequestMsg()
{
}

RequestMsg::~RequestMsg()
{
}

int32_t RequestMsg::GetStatusCode()
{
    return int32_t();
}

std::string & RequestMsg::GetMethod()
{
    return method_;
}

void RequestMsg::Parse(std::string & msg)
{
    vector<string> vec;
    //先根据\r\n\r\n 区分出请求和body
    Split(msg, "\r\n\r\n", vec);
    if (vec.size() > 1)
    {
        sdp_ = vec[1];
    }

    auto req = vec[0];
    vec.clear();
    Split(req, "\r\n", vec);

    //parse REQ
    auto line = vec[0];
    vector<string> vecReq;
    Split(line, " ", vecReq);
    if (vecReq.size() != 3)
    {
        return;
    }
    method_ = vecReq[0];
    url_ = vecReq[1];
    protocol_ = vecReq[2];

    vector<string> head;
    for (size_t i = 1; i < vec.size(); i++)
    {
        //parse Header
        auto& line = vec[i];
        
        Split(line, ":", head);
        headers_.emplace(head[0], std::move(Trim(head[1])));
        head.clear();
    }

}

ResponseMsg::ResponseMsg()
{

}

ResponseMsg::~ResponseMsg()
{

}

int32_t ResponseMsg::GetStatusCode()
{
    return int32_t();
}

std::string & ResponseMsg::GetMethod()
{
    // TODO: 在此处插入 return 语句
    std::string temp = "";
    return temp;
}


void ResponseMsg::Parse(std::string & msg)
{
}
