#ifndef I_RTSP_CLIENT_H
#define I_RTSP_CLIENT_H
#include <string>
#include <vector>
#include "media_info.h"
namespace OHOS {
namespace   Sharing {
        enum class TransType { RTP_INVALID = -1, RTP_OVER_TCP, RTP_OVER_UDP, RTP_MULTICAST };
        /** RTSP 会话状态. */
        enum class RtspStatus {
            RTSP_STATUS_UNKNOWN = 0,
            RTSP_STATUS_CONNECTING = 10,
            RTSP_STATUS_INIT = 20,
            RTSP_STATUS_READY = 30,
            RTSP_STATUS_PLAYING = 40,
            RTSP_STATUS_RECORDING = 50
        };
        class IRtspClient {
        public:
            IRtspClient() = default;
            virtual ~IRtspClient() = default;
            TransType GetTransType()
            {
                return type_;
            }
            void SetTransType(TransType type)
            {
                type_ = type;
            }
            RtspStatus GetStatus()
            {
                return status_;
            }
            void SetStatus(RtspStatus status)
            {
                status_ = status;
            }
            
        public:
            virtual bool SetUrl(const std::string &url) = 0;
            virtual void Close() = 0;
            virtual bool Open() = 0;

        protected:
            TransType type_ = TransType::RTP_OVER_UDP;
            RtspStatus status_ = RtspStatus::RTSP_STATUS_UNKNOWN;
            // std::string url_;
        };
    }  // namespace OHOS::Sharing
}

#endif