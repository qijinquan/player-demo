#include "TcpClient.h"
#include <iostream>
#include <WinSock2.h>
#include <thread>
#include "utils/utils.h"
#include <sstream>
using namespace OHOS::Sharing;
using namespace std;
TcpClient::Ptr TcpClient::CreateTcpClient(const std::string& peer, int32_t port, IClientCallback::Ptr callback)
{
    auto client = std::make_shared<TcpClient>(peer, port, callback);
    client->Connect();
    return client;
}
TcpClient::TcpClient(const std::string& peer, int32_t port, IClientCallback::Ptr callback):peer_(peer), port_(port), callback_(callback)
{

}
void TcpClient::Send(const std::string& buf)
{
    std::cout << "Sned buf:" << std::endl;
    std::cout << buf << std::endl;
    auto vec = Split(buf, "\r\n");
    auto line  = vec[0];
    vec = Split(line, " ");
    auto method = vec[0];
    stringstream ss;
    if (method == "OPTIONS")
    {
        ss << "RTSP/1.0 200 OK" << "\r\n";
        ss << "CSeq: 1" << "\r\n";
        ss << "Server: Wowza Streaming Engine 4.8.19+4 build20220628155901" << "\r\n";
        ss << "Public: DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE, OPTIONS, ANNOUNCE, RECORD, GET_PARAMETER" << "\r\n";
        ss << "Supported: play.basic, con.persistent" << "\r\n";
    }
    else if (method == "DESCRIBE")
    {
            ss << "RTSP/1.0 200 OK" << "\r\n";
            ss << "CSeq : 2" << "\r\n";
            ss << "Server : Wowza Streaming Engine 4.8.19+4 build20220628155901" << "\r\n";
            ss << "Cache-Control : no-cache" << "\r\n";
            ss << "Expires : Thu, 28 Jul 2022 03:28:36 UTC" << "\r\n";
            ss << "Content-Length : 606" << "\r\n";
            ss << "Content-Base : rtsp://wowzaec2demo.streamlock.net:554/vod/mp4:BigBuckBunny_115k.mp4/" << "\r\n";
            ss << "Date : Thu, 28 Jul 2022 03:28:36 UTC" << "\r\n";
            ss << "Content-Type : application/sdp" << "\r\n";
            ss << "Session : 332111097; timeout=60" << "\r\n";
            ss << "\r\n";
            ss << "v=0" << "\r\n";
            ss << "o=-332111097 332111097 IN IP4 34.227.104.115" << "\r\n";
            ss << "s=BigBuckBunny_115k.mp4" << "\r\n";
            ss << "c=IN IP4 34.227.104.115" << "\r\n";
            ss << "t=0 0" << "\r\n";
            ss << "a=sdplang:en" << "\r\n";
            ss << "a=range:npt = 0- 634.625" << "\r\n";
            ss << "a=control:*" << "\r\n";
            ss << "m=audio 0 RTP/AVP 96" << "\r\n";
            ss << "a=rtpmap:96 mpeg4-generic/12000/2" << "\r\n";
            ss << "a=fmtp:96 profile-level-id=1; mode=AAC-hbr; sizelength=13; indexlength=3; indexdeltalength=3; config=149056e500" << "\r\n";
            ss << "a=control:trackID=1" << "\r\n";
            ss << "m=video 0 RTP/AVP 97" << "\r\n";
            ss << "a=rtpmap:97 H264/90000" << "\r\n";
            ss << "a=fmtp:97 packetization-mode=1 profile-level-id=64000C; sprop-parameter-sets=Z2QADKzZQ8Vv/ACAAGxAAAADAEAAAAwDxQplgA==,aOvssiw=" << "\r\n";
            ss << "a=cliprect:0, 0, 160, 240" << "\r\n";
            ss << "a=framesize:97 240-160" << "\r\n";
            ss << "a=framerate:24.0" << "\r\n";
            ss << "a=control:trackID=2" << "\r\n";
    }
    else if (method == "SETUP")
    {   
        static uint32_t index = 1;
        ss << "RTSP/1.0 200 OK" << "\r\n";
        ss << "CSeq: 4" << "\r\n";
            ss << "Server : Wowza Streaming Engine 4.8.19 + 4 build20220628155901" << "\r\n";
            ss << "Cache-Control : no-cache" << "\r\n";
            ss << "Expires : Thu, 28 Jul 2022 03 : 28 : 37 UTC" << "\r\n";
            if (index++%2 == 0)
            {
                ss << "Transport:RTP/AVP/TCP;unicast;interleaved=2-3" << "\r\n";
            }
            else {
                ss << "Transport:RTP/AVP/TCP;unicast;interleaved=1-2" << "\r\n";
            }
            ss << "Date: Thu, 28 Jul 2022 03:28:37 UTC" << "\r\n";
            ss << "Session : 332111097;timeout=60" << "\r\n";
    }
    else if (method == "PLAY")
    {
        ss << "RTSP/1.0 200 OK" << "\r\n";
        ss << "CSeq:4" << "\r\n";
        ss << "Server: Wowza Streaming Engine 4.8.19 + 4 build20220628155901" << "\r\n";
        ss << "Cache-Control: no-cache" << "\r\n";
        ss << "Expires:Thu, 28 Jul 2022 03 : 28 : 37 UTC" << "\r\n";
    }
    const std::string result = ss.str();
    std::thread t1([result, this] {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        if (callback_)
        {
            callback_->OnClientReadData(0, result);
        }
    });
    t1.detach();
    

}
bool TcpClient::Connect()
{   
    /*
    SOCKET socket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socket_ == INVALID_SOCKET)
    {
        std::cout << "Socket error" << std::endl;
        return false;
    }
    sockaddr_in sock_in;
    sock_in.sin_family = AF_INET;
    sock_in.sin_port = htons(port_);
    sock_in.sin_addr.S_un.S_addr = inet_addr(peer_.c_str());
    //inet_pton();
    if (connect(socket_, (sockaddr*)&sock_in, sizeof(sock_in)) == SOCKET_ERROR) {
        std::cout << "Connect error" << std::endl;
        return false;
    }
    //auto ptr = shared_from_this();
    //启动线程读取数据
    std::thread t1([this] {
        fd_set fread;
        do
        {
            
            FD_ZERO(&fread);
            FD_SET(0, &fread);
            timeval timeout{0, 200};
            auto fdCount = select(0, &fread, nullptr, nullptr, &timeout);
        } while (true);
    });
    t1.detach();
    */
    std::thread t1([this] {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        if (callback_)
        {
            callback_->OnClientConnectResult(true);
        }
        
    });
    t1.detach();
    return true;
}