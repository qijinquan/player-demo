#ifndef RTSP_CLIENT_H
#define RTSP_CLIENT_H
#include <functional>
#include <map>
#include "i_rtsp_client.h"
#include "media_info.h"
//#include "network/interfaces/iclient_callback.h"
#include "iclient_callback.h"
//#include "network/network_factory.h"
#include "TcpClient.h"
#include "rtsp_parser.h"
//#include "sdp_parser.h"
#include "uri.h"
#include "rtsp.h"
namespace OHOS::Sharing {
enum class RtspRspResult { RSP_OK, RSP_UNAUTHED, RSP_ERR };
enum class RtspExceptType{
    CONNECT_ERR,
    PLAY_ERR,
    PUSH_ERR,
};
enum class PauseType { TYPE_PAUSE, TYPE_PLAY, TYPE_SPEED };
class RtspClient
    : public IRtspClient
    , public IClientCallback {
public:
    using Ptr = std::shared_ptr<RtspClient>;
    using OnRecvRtspPacket = std::function<void(const std::string& reqMethod, const RtspMessage &rsp)>;
    using OnRtspException = std::function<void(const RtspExceptType except)>;
    using OnRtspReady = std::function<void(void)>;
    RtspClient() : IRtspClient()
    {}
    virtual ~RtspClient() = default;

    void SetOnRecvRtspPacket(OnRecvRtspPacket cb);
    void SetExceptionCb(OnRtspException cb);
    void SetRtspReadyCb(OnRtspReady cb);
    void SendOption();
    void SendDescribe();
    void SendAnnounce();
    void SendSetup(uint32_t trackIndex);
    void SendPlay();
    void SendPause(PauseType type, uint32_t seekMs);
    void SendRecord();
    void SetTransPort(uint16_t port = 0, TrackType type = TrackType::TRACK_AUDIO);
    uint16_t GetTransPort(TrackType type = TrackType::TRACK_AUDIO);
    RtspRspResult HandleOptionRsp(const RtspMessage &rsp);
    RtspRspResult HandleDescribeRsp(const RtspMessage &rsp);
    RtspRspResult HandleSetupRsp(const RtspMessage &rsp);
    RtspRspResult HandlePlayRsp(const RtspMessage &rsp);
    RtspRspResult HandleAnnounceRsp(const RtspMessage &rsp);
    RtspRspResult HandleRecordRsp(const RtspMessage &rsp);

public:
    //继承至RtspClient
    virtual bool SetUrl(const std::string &url) override;
    virtual void Close() override;
    virtual bool Open() override;

public:
    // 继承至Tcp
    
    virtual void OnClientConnectResult(bool isSuccess) ;
    //virtual void OnClientReadData(int32_t fd, const DataBuffer::Ptr &buf) ;
    virtual void OnClientReadData(int32_t fd, const std::string& buf);
    virtual void OnClientWriteable(int32_t fd) ;
    virtual void OnClientClose(int32_t fd) ;
    virtual void OnClientException(int32_t fd);
    
public:
    MediaFormat::Ptr GetTrackFormat(uint32_t mediaType);
    //std::vector<SdpMedia::Ptr>& GetAllMedia();
    std::vector<SdpTrack::Ptr>& GetAllMedia();
protected:
    bool Connect(uint32_t timeout = 0);
    int32_t SendRtspRequest(const std::string &method, const std::string &url,
                            std::map<std::string, std::string> &headers, const std::string &sdp = "");
    RtspRspResult HandleResponse(const RtspMessage &rsp);
    bool HandleAuthFailure(const std::string &line);
    void OnException(RtspExceptType type);
private:
    //NetworkFactory::ClientPtr tcpClient_;
    TcpClient::Ptr tcpClient_;
    Uri::Ptr uri_;
    std::string sessionId_;
    //std::vector<SdpMedia::Ptr> medias_;
    std::vector<SdpTrack::Ptr> medias_;
    uint32_t cseq_ = 1;
    uint32_t auth_;
    std::string protocol_;
    std::string realm_;
    std::string nonce_;
    OnRecvRtspPacket recvRtsp_;
    OnRtspException exceptCb_;
    OnRtspReady ready_;
    RtspMessage recvMsg_;
    std::vector<std::string> supportedCmd_;
    std::string lastMethod_;
    std::string contentBase_;  // trackContentBase
    uint16_t port_[TrackType::TRACK_MAX];
    bool push_;
};
}  // namespace OHOS::Sharing
#endif  // RTSP_CLIENT_H