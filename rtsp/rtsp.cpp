#include "rtsp.h"
#include "utils/utils.h"
//#include "frame/frame.h"
namespace OHOS::Sharing {
    static void GetAttrSdp(const std::multimap<std::string, std::string> &attr, std::stringstream& ss) {
        const std::map<std::string, std::string>::value_type *ptr = nullptr;
        for (auto &pr : attr) {
            if (pr.first == "control") {
                ptr = &pr;
                continue;
            }
            if (pr.second.empty()) {
                ss << "a=" << pr.first << "\r\n";
            }
            else {
                ss << "a=" << pr.first << ":" << pr.second << "\r\n";
            }
        }
        if (ptr) {
            ss << "a=" << ptr->first << ":" << ptr->second << "\r\n";
        }
    }
    static TrackType ToTrackType(const std::string &str) {
        if (str == "") {
            return TrackType::TRACK_TITLE;
        }

        if (str == "video") {
            return TrackType::TRACK_VIDEO;
        }

        if (str == "audio") {
            return TrackType::TRACK_AUDIO;
        }

        return TrackType::TRACK_INVALID;
    }
    std::string SdpTrack::ToString(uint16_t port) const
    {
        std::stringstream ss;
        switch (type_) {
        case TrackType::TRACK_TITLE: {
            TitleSdp title(duration_);
            ss << title.GetSdp();
            break;
        }
        case TrackType::TRACK_AUDIO:
        case TrackType::TRACK_VIDEO: {
            if (type_ == TrackType::TRACK_AUDIO) {
                ss << "m=audio " << port << " RTP/AVP " << pt_ << "\r\n";
            }
            else {
                ss << "m=video " << port << " RTP/AVP " << pt_ << "\r\n";
            }
            if (!b_.empty()) {
                ss << "b=" << b_ << "\r\n";
            }
            GetAttrSdp(attr_, ss);
            break;
        }
        default: break;
        }
        return std::move(ss.str());
    }

    std::string SdpTrack::GetName() const
    {
        switch (pt_) {
#define SWITCH_CASE(name, type, value, clock_rate, channel, codec_id) case value :  return #name;
            RTP_PT_MAP(SWITCH_CASE)
#undef SWITCH_CASE
        default: return codec_;
        }
    }
    std::string SdpTrack::GetControlUrl(const std::string& baseUrl) const
    {
        if (control_.find("://") != std::string::npos) {
            return control_;
        }
        return baseUrl + "/" + control_;
    }
    void SdpParser::Load(const std::string &sdp)
    {
        trackVec_.clear();
        SdpTrack::Ptr track = std::make_shared<SdpTrack>();
        track->type_ = TrackType::TRACK_TITLE;
        trackVec_.emplace_back(track);
        auto lines = Split(sdp, "\n");
        for (auto &line : lines)
        {
            Trim(line);
            if (line.size() < 2 || line[1] != '=')
            {
                continue;
            }
            char opt = line[0];
            std::string val = line.substr(2);
            switch (opt)
            {
            case 't':
                track->t_ = val;
                break;
            case 'b':
                track->b_ = val;
                break;
            case 'm':
            {
                track = std::make_shared<SdpTrack>();
                int pt, port;
                char rtp[16] = { 0, }, type[16] = { 0 };
                if (4 == sscanf_s(val.data(), "%15[^ ] %d %15[^ ] %d", type, sizeof(type), &port,  rtp, sizeof(rtp), &pt))
                {
                    track->pt_ = pt;
                    track->samplerate_ = RtpPayload::GetClockRate(pt);
                    track->channel_ = RtpPayload::GetAudioChannel(pt);
                    track->type_ = ToTrackType(type);
                    track->port_ = port;
                    trackVec_.emplace_back(track);
                }
                break;
            }
            case 'a': {
                auto vec = SplitOnce(val, ":");
                if (vec.size() > 1)
                {
                    track->attr_.emplace(vec[0], vec[1]);
                }
                else {
                    track->attr_.emplace(vec[0], "");
                }
                break;
            }
            default:
                track->other_[opt] = val;
                break;
            }
        }
        for (auto& track : trackVec_ )
        {
            auto it = track->attr_.find("range");
            if (it != track->attr_.end())
            {
                char name[16] = { 0 }, start[16] = { 0 }, end[16] = { 0 };
                int ret = sscanf_s(it->second.data(), "%15[^=]=%15[^-]-%15s", name, sizeof(name), start, sizeof(start), end, sizeof(end));
                if (ret == 3 || ret == 2)
                {
                    if (strcmp(start, "now") == 0) {
                        strcpy_s(start, "0");
                    }
                    track->start_ = (float)atof(start);
                    track->end_ = (float)atof(end);
                    track->duration_ = track->end_ - track->start_;
                }
            }
            for (it = track->attr_.find("rtpmap"); it != track->attr_.end() && it->first == "rtpmap";) {
                auto &rtpmap = it->second;
                int pt, samplerate, channel;
                char codec[16] = { 0 };

                sscanf_s(rtpmap.data(), "%d", &pt);
                if (track->pt_ != pt) {
                    //pt不匹配
                    it = track->attr_.erase(it);
                    continue;
                }
                if (4 == sscanf_s(rtpmap.data(), "%d %15[^/]/%d/%d", &pt, codec, sizeof(codec), &samplerate, &channel)) {
                    track->codec_ = codec;
                    track->samplerate_ = samplerate;
                    track->channel_ = channel;
                }
                else if (3 == sscanf_s(rtpmap.data(), "%d %15[^/]/%d", &pt, codec, sizeof(codec), &samplerate)) {
                    track->pt_ = pt;
                    track->codec_ = codec;
                    track->samplerate_ = samplerate;
                }
                if (!track->samplerate_ && track->type_ == TRACK_VIDEO) {
                    //未设置视频采样率时，赋值为90000
                    track->samplerate_ = 90000;
                }
                ++it;
            }
            for (it = track->attr_.find("fmtp"); it != track->attr_.end() && it->first == "fmtp"; ) {
                auto &fmtp = it->second;
                int pt;
                sscanf_s(fmtp.data(), "%d", &pt);
                if (track->pt_ != pt) {
                    //pt不匹配
                    it = track->attr_.erase(it);
                    continue;
                }
                auto vec = SplitOnce(fmtp, " ");
                if (vec.size() > 1)
                {
                    track->fmtp_ = vec[1];
                }
                ++it;
            }
            it = track->attr_.find("control");
            if (it != track->attr_.end()) {
                track->control_ = it->second;
            }
        }

    }
    bool SdpParser::Available() const
    {
        return GetTrack(TrackType::TRACK_AUDIO) || GetTrack(TrackType::TRACK_VIDEO);
    }
    SdpTrack::Ptr SdpParser::GetTrack(TrackType type) const
    {
        for (auto &track : trackVec_) {
            if (track->type_ == type) {
                return track;
            }
        }
        return nullptr;
    }
    std::vector<SdpTrack::Ptr> SdpParser::GetAvailableTrack() const
    {
        std::vector<SdpTrack::Ptr> ret;
        bool audioAdded = false;
        bool videoAdded = false;
        for (auto &track : trackVec_) {
            if (track->type_ == TrackType::TRACK_AUDIO) {
                if (!audioAdded) {
                    ret.emplace_back(track);
                    audioAdded = true;
                }
                continue;
            }

            if (track->type_ == TrackType::TRACK_VIDEO) {
                if (!videoAdded) {
                    ret.emplace_back(track);
                    videoAdded = true;
                }
                continue;
            }
        }
        return ret;
    }
    std::string SdpParser::toString() const
    {
        std::string title, audio, video;
        for (auto &track : trackVec_) {
            switch (track->type_) {
            case TrackType::TRACK_TITLE: {
                title = track->ToString();
                break;
            }
            case TrackType::TRACK_VIDEO: {
                video = track->ToString();
                break;
            }
            case TrackType::TRACK_AUDIO: {
                audio = track->ToString();
                break;
            }
            default: break;
            }
        }
        return title + video + audio;
    }

    int RtpPayload::GetClockRate(int pt) {
        switch (pt) {
#define SWITCH_CASE(name, type, value, clock_rate, channel, codec_id) case value :  return clock_rate;
            RTP_PT_MAP(SWITCH_CASE)
#undef SWITCH_CASE
        default: return 90000;
        }
    }

    TrackType RtpPayload::GetTrackType(int pt) {
        switch (pt){
#define SWITCH_CASE(name, type, value, clock_rate, channel, codec_id) case value : return type;
            RTP_PT_MAP(SWITCH_CASE)
#undef SWITCH_CASE

        default: return TRACK_INVALID;
        }
    }

    int RtpPayload::GetAudioChannel(int pt) {
        switch (pt) {
#define SWITCH_CASE(name, type, value, clock_rate, channel, codec_id) case value :  return channel;
            RTP_PT_MAP(SWITCH_CASE)
#undef SWITCH_CASE
        default: return 1;
        }
    }

    const char *RtpPayload::GetName(int pt) {
        switch (pt) {
#define SWITCH_CASE(name, type, value, clock_rate, channel, codec_id) case value :  return #name;
            RTP_PT_MAP(SWITCH_CASE)
#undef SWITCH_CASE
        default: return "unknown payload type";
        }
    }

    CodecId RtpPayload::GetCodecId(int pt) {
        switch (pt) {
#define SWITCH_CASE(name, type, value, clock_rate, channel, codec_id) case value :  return codec_id;
            RTP_PT_MAP(SWITCH_CASE)
#undef SWITCH_CASE
        default: return CodecInvalid;
        }
    }
}
