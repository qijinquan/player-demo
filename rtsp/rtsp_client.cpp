#include "rtsp_client.h"
#include <iomanip>
#include <sstream>
//#include "sdp_parser.h"
#include "rtsp.h"
#include "utils/utils.h"
#include "common/log.h"
#include "i_rtsp_client.h"
namespace OHOS {
    namespace Sharing {

        bool RtspClient::SetUrl(const std::string &url)
        {
            auto uri = std::make_shared<Uri>();
            if (!uri->Parse(url)) {
                return false;
            }
            uri_.swap(uri);
            return true;
        }
        void RtspClient::Close()
        {
            return;
        }

        bool RtspClient::Open()
        {
            if (GetStatus() != RtspStatus::RTSP_STATUS_UNKNOWN) {
                return false;
            }
            return Connect();
        }

        void RtspClient::OnClientConnectResult(bool isSuccess)
        {
            if (!isSuccess) {
                OnException(RtspExceptType::CONNECT_ERR);
                return;
            }
            
            if (GetStatus() == RtspStatus::RTSP_STATUS_CONNECTING) {
                SetStatus(RtspStatus::RTSP_STATUS_INIT);
            }
            
            if (ready_) {
                ready_();
            }
        }

        void RtspClient::OnClientReadData(int32_t fd, const std::string &buf)
        {
            (void)fd;
            std::string packet = buf;// buf->Peek();
            recvMsg_.Clear();
            recvMsg_.Parse(packet);
            if (recvRtsp_) {
                recvRtsp_(lastMethod_, recvMsg_);
            }
        }

        void RtspClient::OnClientWriteable(int32_t fd)
        {
            (void)fd;
        }

        void RtspClient::OnClientClose(int32_t fd)
        {
            (void)fd;
            // TODO:
        }

        void RtspClient::OnClientException(int32_t fd)
        {
            (void)fd;
            OnException(RtspExceptType::CONNECT_ERR);
        }

        void RtspClient::SendOption()
        {
            std::map<std::string, std::string> headers;
            SendRtspRequest("OPTIONS", uri_->GetHref(), headers);
        }

        RtspRspResult RtspClient::HandleOptionRsp(const RtspMessage &rsp)
        {
            auto rspCode = HandleResponse(rsp);
            if (rspCode != RtspRspResult::RSP_OK) {
                return rspCode;
            }
            // Get command lists
            supportedCmd_.clear();
            supportedCmd_ = Split(rsp["Public"], ",");
            return rspCode;
        }

        void RtspClient::SendDescribe()
        {
            std::map<std::string, std::string> headers;
            SendRtspRequest("DESCRIBE", uri_->GetHref(), headers);
        }

        RtspRspResult RtspClient::HandleDescribeRsp(const RtspMessage &rsp)
        {
            auto rspCode = HandleResponse(rsp);
            if (rspCode != RtspRspResult::RSP_OK) {
                return rspCode;
            }
            contentBase_ = rsp["Content-Base"];
            if (contentBase_.empty()) {
                contentBase_ = uri_->GetHref();
            }
            if (contentBase_.back() == '/') {
                contentBase_.pop_back();
            }
            SdpParser parser(rsp.GetSdp());
            medias_ = parser.GetAvailableTrack();
            
            if (medias_.size() == 0) {
                return RtspRspResult::RSP_ERR;
            }
            auto sess = Split(rsp["Session"], ";");  // Session: 332111097;timeout=60
            sessionId_ = sess[0];
            push_ = false;
            return rspCode;
        }
        RtspRspResult RtspClient::HandleSetupRsp(const RtspMessage &rsp)
        {
            if (rsp.GetStatusCode() != StatusCode::Status_Code_OK) {
                return RtspRspResult::RSP_ERR;
            }
            return RtspRspResult::RSP_OK;
        }

        RtspRspResult RtspClient::HandlePlayRsp(const RtspMessage &rsp)
        {
            if (rsp.GetStatusCode() != StatusCode::Status_Code_OK) {
                return RtspRspResult::RSP_ERR;
            }
            return RtspRspResult::RSP_OK;
        }
        void RtspClient::SendAnnounce()
        {
            // TODO:
            const std::string sdp = "";
            std::map<std::string, std::string> headers;
            SendRtspRequest("ANNOUNCE", uri_->GetHref(), headers);
            // rsp 中解析session
        }

        RtspRspResult RtspClient::HandleAnnounceRsp(const RtspMessage &rsp)
        {
            std::string auth = rsp["WWW-Authenticate"];
            if (rsp.GetStatusCode() == StatusCode::Status_Code_UnAuthorized && HandleAuthFailure(auth)) {
                return RtspRspResult::RSP_UNAUTHED;
            }
            // if (parser.GetStatusCode() == 302) {
            //     auto newUrl = parser["Location"];
            //     if (newUrl.empty()) {
            //         throw std::runtime_error("未找到Location字段(跳转url)");
            //     }
            //     publish(newUrl);
            //     return;
            // }
            if (rsp.GetStatusCode() != StatusCode::Status_Code_OK) {
                return RtspRspResult::RSP_ERR;
            }

            contentBase_ = rsp["Content-Base"];

            if (contentBase_.empty()) {
                contentBase_ = uri_->GetHref();
            }
            if (contentBase_.back() == '/') {
                contentBase_.pop_back();
            }
            sessionId_ = rsp["Session"];  // check, 只带session
            push_ = true;
            return RtspRspResult::RSP_OK;
        }

        void RtspClient::SendSetup(uint32_t trackIndex)
        {
            if (trackIndex >= medias_.size()) {
                return;
            }
            auto track = medias_[trackIndex];
            auto url = contentBase_ + "/" + track->control_;
            // transport
            std::stringstream stream;
            switch (type_) {
            case TransType::RTP_OVER_UDP:
                stream << "RTP/AVP;unicast;client_port=" << port_[track->type_] << "-" << port_[track->type_]+1;
                break;
            case TransType::RTP_OVER_TCP:
                stream << "RTP/AVP/TCP;unicast;interleaved=" << 2*track->type_ << "-" << 2 * track->type_+1;
                break;
            default:
                break;
            }
            if (push_) {
                stream << ";mode=record";
            }
            std::map<std::string, std::string> headers;
            headers.emplace("Transport", stream.str());
            SendRtspRequest("SETUP", url, headers);
        }

        void RtspClient::SendPlay()
        {
            if (push_) {
                // TODO:
                return;
            }
            auto url = contentBase_ + "/";
            std::map<std::string, std::string> headers;
            headers.emplace("Range", "0.000-");
            SendRtspRequest("PLAY", url, headers);
        }
        void RtspClient::SendPause(PauseType type, uint32_t seekMs)
        {
            auto url = contentBase_ + "/";
            std::map<std::string, std::string> headers;
            switch (type) {
            case PauseType::TYPE_PAUSE:
                SendRtspRequest("PAUSE", url, headers);
                break;
            case PauseType::TYPE_PLAY:
                SendRtspRequest("PLAY", url, headers);
                break;
            case PauseType::TYPE_SPEED: {
                std::stringstream ss;
                ss << "npt=" << std::setiosflags(std::ios::fixed) << std::setprecision(2) << seekMs / 1000.0 << "-";
                headers.emplace("Range", ss.str());
                break;
            }
            default:
                break;
            }
        }
        void RtspClient::SendRecord()
        {
            std::map<std::string, std::string> headers;
            headers.emplace("Range", "npt=0.000-");
            SendRtspRequest("RECORD", contentBase_, headers);
        }
        RtspRspResult RtspClient::HandleRecordRsp(const RtspMessage &rsp)
        {
            if (rsp.GetStatusCode() != StatusCode::Status_Code_OK) {
                return RtspRspResult::RSP_ERR;
            }
            return RtspRspResult::RSP_OK;
        }

        bool RtspClient::Connect(uint32_t timeout)
        {
            (void)timeout;
            SetStatus(RtspStatus::RTSP_STATUS_CONNECTING);
            // TCP 连接
            //tcpClient_ = NetworkFactory::CreateTcpClient(uri_->GetHost(), uri_->GetPort(), shared_from_this());
            tcpClient_ = TcpClient::CreateTcpClient(uri_->GetHost(), uri_->GetPort(), shared_from_this());
            return true;
        }
        MediaFormat::Ptr RtspClient::GetTrackFormat(uint32_t mediaType)
        {
            // TODO:
            (void)mediaType;
            return nullptr;
        }
        std::vector<SdpTrack::Ptr>& RtspClient::GetAllMedia()
        {
            return medias_;
        }
        int32_t RtspClient::SendRtspRequest(const std::string &method, const std::string &url,
            std::map<std::string, std::string> &headers, const std::string &sdp)
        {
            RtspStatus state = GetStatus();
            
            if (state == RtspStatus::RTSP_STATUS_UNKNOWN || state == RtspStatus::RTSP_STATUS_CONNECTING) {
                // TODO:
                return -1;  // 无效的状态
            }
            
            std::stringstream sstream;
            headers.emplace("CSeq", std::to_string(cseq_++));
            headers.emplace("User-Agent", "OpenHarmony");
            if (!sessionId_.empty()) {
                headers.emplace("Session", sessionId_);
            }
            //  MD5加密
            if (auth_ == 2) {
                // md5sum_response(client, "DESCRIBE", client->uri, response_auth);
                // Authorization: Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", response=\"%s\"\r\n"
                // Add to headers
            }
            else if (auth_ == 1) {
                // base64_response(client, response_auth);
                //"Authorization: Basic %s\r\n", response_auth);
                // Add to headers
            }

            if (!sdp.empty()) {
                headers.emplace("Content-Length", std::to_string(sdp.size()));
                headers.emplace("Content-Type", "application/sdp");
            }

            sstream << method << " " << url << " " << protocol_ << "\r\n";
            for (auto &[key, value] : headers) {
                sstream << key << ":" << value << "\r\n";
            }
            sstream << "\r\n";

            if (!sdp.empty()) {
                sstream << sdp;
            }
            std::string result = sstream.str();
            lastMethod_ = method;
            if (tcpClient_) {
                //DataBuffer::Ptr bufSend = std::make_shared<DataBuffer>();
                //bufSend->PushData(result.c_str(), result.size());
                //tcpClient_->Send(bufSend, bufSend->Size());
                tcpClient_->Send(result);
            }
            
            
            return 0;
        }
        void RtspClient::SetTransPort(uint16_t port, TrackType type)
        {
            port_[type] = port;
        }
        uint16_t RtspClient::GetTransPort(TrackType type)
        {
            return port_[type];
        }
        RtspRspResult RtspClient::HandleResponse(const RtspMessage &rsp)
        {
            auto auth = rsp["WWW-Authenticate"];
            if (rsp.GetStatusCode() == StatusCode::Status_Code_UnAuthorized && HandleAuthFailure(auth)) {
                SHARING_LOGD("HandleAuthFailure");
                return RtspRspResult::RSP_UNAUTHED;
            }
            // TODO:
            // if(rsp.GetStatusCode() == StatusCode::Status_Code_PermMoved || rsp.GetStatusCode() ==
            // StatusCode::Status_Code_TempMoved){
            //     auto newUrl = parser["Location"];
            //     if(newUrl.empty()){
            //         throw std::runtime_error("未找到Location字段(跳转url)");
            //     }
            //     play(newUrl);
            //     return false;
            // }
            if (rsp.GetStatusCode() != StatusCode::Status_Code_OK) {
                SHARING_LOGD("status code is %{public}d", rsp.GetStatusCode());
                return RtspRspResult::RSP_ERR;
            }
            return RtspRspResult::RSP_OK;
        }
        bool RtspClient::HandleAuthFailure(const std::string &line)
        {
            if (!realm_.empty()) {
                return false;
            }
            auto realm = new char[line.size()];
            auto nonce = new char[line.size()];
            auto stale = new char[line.size()];
            unsigned int size = line.size();
            // std::onceToken token(nullptr, [&](){
            //     delete realm[];
            //     delete nonce[];
            //     delete stale[];
            // });
            // TODO delete[]
            if (sscanf_s(line.data(), "Digest realm =\"%[^\"]\", nonce=\"%[^\"]\", stale=%[a-zA-Z]", realm, size, nonce, size, stale, size) == 3) {
                realm_ = realm;
                nonce_ = nonce;
                return true;
            }
            if (sscanf_s(line.data(), "Digest realm=\"%[^\"]\", nonce=\"%[^\"]\"", realm, size, nonce, size) == 2) {
                realm_ = realm;
                nonce_ = nonce;
                return true;
            }
            if (sscanf_s(line.data(), "Basic realm=\"%[^\"]\"", realm, size) == 1) {
                realm_ = realm;
                return true;
            }
            return true;
        }
        void RtspClient::OnException(RtspExceptType type)
        {
            if (exceptCb_) {
                exceptCb_(type);
            }
        }
        void RtspClient::SetOnRecvRtspPacket(OnRecvRtspPacket cb)
        {
            recvRtsp_ = cb;
        }
        void RtspClient::SetExceptionCb(OnRtspException cb)
        {
            exceptCb_ = cb;
        }

        void RtspClient::SetRtspReadyCb(OnRtspReady cb)
        {
            ready_ = cb;
        }

    }  // namespace OHOS::Sharing
}