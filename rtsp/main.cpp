﻿// rtsp.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
#include "uri.h"
#include <iostream>
#include <sstream>
#include <WinSock2.h>
#include <mutex>
#include <condition_variable>
#include "message.h"
#include "rtsp_client.h"
using namespace OHOS::Sharing;
int main()
{
    WORD sockVersion = MAKEWORD(2, 2);
    WSADATA wsdata;
    if (WSAStartup(sockVersion, &wsdata) != 0)
    {
        return 1;
    }
    std::string url = "rtsp://192.168.159.128:554/live/test";
    auto uri = std::make_shared<Uri>(url);
    auto client = std::make_shared<RtspClient>();
    std::mutex mutex;
    std::condition_variable signal;
    client->SetExceptionCb([](const RtspExceptType except){
        std::cout << "exit" << std::endl;
        exit(0);
    });
    client->SetRtspReadyCb([client]() {
        client->SendOption();
    });
    uint16_t index = 0;
    client->SetOnRecvRtspPacket([client,&signal,&index](const std::string& reqMethod, const RtspMessage &rsp) {
        if (reqMethod == "OPTIONS")
        {
            client->HandleOptionRsp(rsp);
            client->SendDescribe();
        }else if (reqMethod == "DESCRIBE")
        {
            client->HandleDescribeRsp(rsp);
            client->SetTransPort(43356, TrackType::TRACK_AUDIO);
            client->SetTransPort(43358, TrackType::TRACK_VIDEO);
            client->SendSetup(index++);
        }
        else if (reqMethod == "SETUP")
        {
            client->HandleSetupRsp(rsp);
            if (index<2)
            {
                client->SendSetup(index++);
            }
            else
            {
                client->SendPlay();
            }
        }
        else if (reqMethod == "PLAY")
        {
            client->HandlePlayRsp(rsp);
        } else{
            signal.notify_one();
        }
    });
    client->SetUrl(url);
    client->Open();
    std::unique_lock<std::mutex> lk(mutex);
    signal.wait(lk);
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
