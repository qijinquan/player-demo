#include "h264_frame.h"

namespace OHOS {
namespace Sharing {
    
size_t PrefixSize(const char *ptr, size_t len) {
    if (len < 4) {
        return 0;
    }
    if (ptr[0] != 0x00 || ptr[1] != 0x00) {
        //不是0x00 00开头
        return 0;
    }
    if (ptr[2] == 0x00 && ptr[3] == 0x01) {
        //是0x00 00 00 01
        return 4;
    }
    if (ptr[2] == 0x01) {
        //是0x00 00 01
        return 3;
    }
    return 0;
}

static const char *MemFind(const char *buf, ssize_t len, const char *subBuf, ssize_t subLen) 
{
    for (auto i = 0; i < len - subLen; ++i) {
        if (memcmp(buf + i, subBuf, subLen) == 0) {
            return buf + i;
        }
    }
    return NULL;
}

void SplitH264(const char *ptr, size_t len, size_t prefix, const std::function<void(const char *, size_t, size_t)> &cb) {
    auto start = ptr + prefix;
    auto end = ptr + len;
    size_t nextPrefix;
    while (true) {
        auto nextStart = MemFind(start, end - start, "\x00\x00\x01", 3);
        if (nextStart) {
            //找到下一帧
            if (*(nextStart - 1) == 0x00) {
                //这个是00 00 00 01开头
                nextStart -= 1;
                nextPrefix = 4;
            } else {
                //这个是00 00 01开头
                nextPrefix = 3;
            }
            //记得加上本帧prefix长度
            cb(start - prefix, nextStart - start + prefix, prefix);
            //搜索下一帧末尾的起始位置
            start = nextStart + nextPrefix;
            //记录下一帧的prefix长度
            prefix = nextPrefix;
            continue;
        }
        //未找到下一帧,这是最后一帧
        cb(start - prefix, end - start + prefix, prefix);
        break;
    }
}

}
}