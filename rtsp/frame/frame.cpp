#include "frame.h"
#include "frame_impl.h"

namespace OHOS {
namespace Sharing {

TrackType GetTrackType(CodecId codecId) {
    switch (codecId) {
#define XX(name, type, value, str, mpeg_id) case name : return type;
        CODEC_MAP(XX)
#undef XX
        default : return TRACK_INVALID;
    }
}

TrackType CodecInfo::GetTrackType() const
{
    return Sharing::GetTrackType(GetCodecId());
}

Frame::Ptr Frame::GetCacheAbleFrame(const Ptr &frame)
{
    if(frame->CacheAble()){
        return frame;
    }
    return std::make_shared<FrameCacheAble>(frame);
}

}
}