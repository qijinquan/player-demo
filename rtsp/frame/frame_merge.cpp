#include "frame_merge.h"
#include <arpa/inet.h>

namespace OHOS {
namespace Sharing {

static size_t constexpr MAX_FRAME_CACHE_SIZE = 100;

FrameMerger::FrameMerger(int type) : type_(type)
{
}

void FrameMerger::Clear()
{
    frameCache_.clear();
    haveDecodeAbleFrame_ = false;
}

bool FrameMerger::InputFrame(const Frame::Ptr &frame, const onOutput &cb, BufferLikeString *buffer)
{
    if (WillFlush(frame)) {
        Frame::Ptr back = frameCache_.back();
        Buffer::Ptr mergedFrame = back;
        bool haveKeyFrame = back->KeyFrame();

        if (frameCache_.size() != 1 || type_ == MP4_NAL_SIZE || buffer) {
            //在MP4模式下，一帧数据也需要在前添加nalu_size
            BufferLikeString tmp;
            BufferLikeString &merged = buffer ? *buffer : tmp;

            if (!buffer) {
                tmp.Reserve(back->Size() + 1024);
            }

            for (auto &&frame : frameCache_) {
                DoMerge(merged, frame);
                if (frame->KeyFrame()) {
                    haveKeyFrame = true;
                }
            }
            mergedFrame = std::make_shared<BufferOffset<BufferLikeString> >(buffer ? merged : std::move(merged));
        }
        cb(back->Dts(), back->Pts(), mergedFrame, haveKeyFrame);
        frameCache_.clear();
        haveDecodeAbleFrame_ = false;
    }

    if (!frame) {
        return false;
    }

    if (frame->DecodeAble()) {
        haveDecodeAbleFrame_ = true;
    }
    frameCache_.emplace_back(Frame::GetCacheAbleFrame(frame));
    return true;
}

bool FrameMerger::WillFlush(const Frame::Ptr &frame) const
{
    if (frameCache_.empty()) {
        //缓存为空
        return false;
    }
    if (!frame) {
        return true;
    }
    switch (type_) {
        case NONE: {
            // frame不是完整的帧，我们合并为一帧
            bool newFrame = false;
            switch (frame->GetCodecId()) {
                case CodecH264:
                case CodecH265: {
                    //如果是新的一帧，前面的缓存需要输出
                    newFrame = frame->PrefixSize();
                    break;
                }
                default:
                    break;
            }
            //遇到新帧、或时间戳变化或缓存太多，防止内存溢出，则flush输出
            return newFrame || frameCache_.back()->Dts() != frame->Pts() || frameCache_.size() > MAX_FRAME_CACHE_SIZE;
        }

        case MP4_NAL_SIZE:
        case H264_PREFIX: {
            if (!haveDecodeAbleFrame_) {
                //缓存中没有有效的能解码的帧，所以这次不flush
                return frameCache_.size() > MAX_FRAME_CACHE_SIZE;
            }
            if (frameCache_.back()->Dts() != frame->Dts() || frame->DecodeAble() || frame->ConfigFrame()) {
                //时间戳变化了,或新的一帧，或遇到config帧，立即flush
                return true;
            }
            return frameCache_.size() > MAX_FRAME_CACHE_SIZE;
        }
        default: /*不可达*/
            assert(0);
            return true;
    }
}

void FrameMerger::DoMerge(BufferLikeString &merged, const Frame::Ptr &frame) const
{
    switch (type_) {
        case NONE: {
            //此处是合并ps解析输出的流，解析出的流可能是半帧或多帧，不能简单的根据nal type过滤
            //此流程只用于合并ps解析输出为H264/H265，后面流程有split和忽略无效帧操作
            merged.Append(frame->Data(), frame->Size());
            break;
        }
        case H264_PREFIX: {
            if (frame->PrefixSize()) {
                merged.Append(frame->Data(), frame->Size());
            } else {
                merged.Append("\x00\x00\x00\x01", 4);
                merged.Append(frame->Data(), frame->Size());
            }
            break;
        }
        case MP4_NAL_SIZE: {
            uint32_t naluSize = (uint32_t)(frame->Size() - frame->PrefixSize());
            naluSize = htonl(naluSize);
            merged.Append((char *)&naluSize, 4);
            merged.Append(frame->Data() + frame->PrefixSize(), frame->Size() - frame->PrefixSize());
            break;
        }
        default:
            break;
    }
}

}  // namespace Sharing
}  // namespace OHOS