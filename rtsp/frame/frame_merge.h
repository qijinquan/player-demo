#ifndef __FRAME_MERGE_H__
#define __FRAME_MERGE_H__

#include <functional>
#include <list>
#include <memory>

#include "common/buffer.h"
#include "frame.h"

namespace OHOS {
namespace Sharing {

/**
 * Merge some frames with the same timestamp
 */
class FrameMerger {
public:
    using onOutput = std::function<void(uint32_t dts, uint32_t pts, const Buffer::Ptr &buffer, bool have_key_frame)>;
    using Ptr = std::shared_ptr<FrameMerger>;
    enum {
        NONE = 0,
        H264_PREFIX,
        MP4_NAL_SIZE,
    };

    FrameMerger(int type);
    ~FrameMerger() = default;

    void Clear();
    bool InputFrame(const Frame::Ptr &frame, const onOutput &cb, BufferLikeString *buffer = nullptr);

private:
    bool WillFlush(const Frame::Ptr &frame) const;
    void DoMerge(BufferLikeString &buffer, const Frame::Ptr &frame) const;

private:
    int type_;
    bool haveDecodeAbleFrame_ = false;
    std::list<Frame::Ptr> frameCache_;
};

}  // namespace Sharing
}  // namespace OHOS

#endif // __FRAME_MERGE_H__