#ifndef __FRAME_IMPL_H__
#define __FRAME_IMPL_H__

#include <string>
#include <mutex>
#include <list>
#include <map>
#include "frame.h"

namespace OHOS {
namespace Sharing {

class FrameImp : public Frame {
public:
    using Ptr = std::shared_ptr<FrameImp>;

    template<typename C=FrameImp>
    static std::shared_ptr<C> Create() {
        return std::shared_ptr<C>(new C());
    }

    char *Data() const override{
        return (char *)buffer_.Data();
    }

    size_t Size() const override {
        return buffer_.Size();
    }

    uint32_t Dts() const override {
        return dts_;
    }

    uint32_t Pts() const override{
        return pts_ ? pts_ : dts_;
    }

    size_t PrefixSize() const override{
        return prefixSize_;
    }

    CodecId GetCodecId() const override{
        return codecId_;
    }

    bool KeyFrame() const override {
        return false;
    }

    bool ConfigFrame() const override{
        return false;
    }

public:
    CodecId codecId_ = CodecInvalid;
    uint32_t dts_ = 0;
    uint32_t pts_ = 0;
    size_t prefixSize_ = 0;
    BufferLikeString buffer_;

protected:
    FrameImp() = default;
};

/**
 * 一个Frame类中可以有多个帧，他们通过 0x 00 00 01 分隔
 * 先把这种复合帧split成单个帧然后再处理
 * 一个复合帧可以通过无内存拷贝的方式切割成多个子Frame
 * 提供该类的目的是切割复合帧时防止内存拷贝，提高性能
 */
template<typename Parent>
class FrameInternal : public Parent{
public:
    typedef std::shared_ptr<FrameInternal> Ptr;
    FrameInternal(const Frame::Ptr &parentFrame, char *ptr, size_t size, size_t prefixSize)
            : Parent(ptr, size, parentFrame->Dts(), parentFrame->Pts(), prefixSize) {
        parentFrame_ = parentFrame;
    }
    bool CacheAble() const override {
        return parentFrame_->CacheAble();
    }
private:
    Frame::Ptr parentFrame_;
};

/**
 * 通过Frame接口包装指针，方便使用者把自己的数据快速接入
 */
class FrameFromPtr : public Frame{
public:
    typedef std::shared_ptr<FrameFromPtr> Ptr;

    FrameFromPtr(CodecId codec_id, char *ptr, size_t size, uint32_t dts, uint32_t pts = 0, size_t prefix_size = 0)
            : FrameFromPtr(ptr, size, dts, pts, prefix_size) {
        codecId_ = codec_id;
    }

    FrameFromPtr(char *ptr, size_t size, uint32_t dts, uint32_t pts = 0, size_t prefix_size = 0){
        ptr_ = ptr;
        size_ = size;
        dts_ = dts;
        pts_ = pts;
        prefixSize_ = prefix_size;
    }

    char *Data() const override{
        return ptr_;
    }

    size_t Size() const override {
        return size_;
    }

    uint32_t Dts() const override {
        return dts_;
    }

    uint32_t Pts() const override{
        return pts_ ? pts_ : Dts();
    }

    size_t PrefixSize() const override{
        return prefixSize_;
    }

    bool CacheAble() const override {
        return false;
    }

    CodecId GetCodecId() const override {
        if (codecId_ == CodecInvalid) {
            //throw std::invalid_argument("FrameFromPtr对象未设置codec类型");
        }
        return codecId_;
    }

    void SetCodecId(CodecId codec_id) {
        codecId_ = codec_id;
    }

    bool KeyFrame() const override {
        return false;
    }

    bool ConfigFrame() const override{
        return false;
    }

protected:
    FrameFromPtr() {}

protected:
    char *ptr_;
    uint32_t dts_;
    uint32_t pts_ = 0;
    size_t size_;
    size_t prefixSize_;
    CodecId codecId_ = CodecInvalid;
};

/**
 * 该对象的功能是把一个不可缓存的帧转换成可缓存的帧
 */
class FrameCacheAble : public FrameFromPtr {
public:
    typedef std::shared_ptr<FrameCacheAble> Ptr;

    FrameCacheAble(const Frame::Ptr &frame, bool forceKeyFrame = false) {
        if (frame->CacheAble()) {
            frame_ = frame;
            ptr_ = frame->Data();
        } else {
            buffer_ = FrameImp::Create();
            buffer_->buffer_.Assign(frame->Data(), frame->Size());
            ptr_ = buffer_->Data();
        }
        size_ = frame->Size();
        dts_ = frame->Dts();
        pts_ = frame->Pts();
        prefixSize_ = frame->PrefixSize();
        codecId_ = frame->GetCodecId();
        key_ = forceKeyFrame ? true : frame->KeyFrame();
        config_ = frame->ConfigFrame();
        dropAble_ = frame->DropAble();
        decodeAble_ = frame->DecodeAble();
    }

    ~FrameCacheAble() override = default;

    /**
     * 可以被缓存
     */
    bool CacheAble() const override {
        return true;
    }

    bool KeyFrame() const override{
        return key_;
    }

    bool ConfigFrame() const override{
        return config_;
    }

    bool DropAble() const override {
        return dropAble_;
    }

    bool DecodeAble() const override {
        return decodeAble_;
    }

private:
    bool key_;
    bool config_;
    bool dropAble_;
    bool decodeAble_;
    Frame::Ptr frame_;
    FrameImp::Ptr buffer_;
};

/**
 * 该对象可以把Buffer对象转换成可缓存的Frame对象
 */
template <typename Parent>
class FrameWrapper : public Parent{
public:
    ~FrameWrapper() = default;

    /**
     * 构造frame
     * @param buf 数据缓存
     * @param dts 解码时间戳
     * @param pts 显示时间戳
     * @param prefix 帧前缀长度
     * @param offset buffer有效数据偏移量
     */
    FrameWrapper(const Buffer::Ptr &buf, uint32_t dts, uint32_t pts, size_t prefix, size_t offset) : Parent(buf->Data() + offset, buf->Size() - offset, dts, pts, prefix){
        buf_ = buf;
    }

    /**
     * 构造frame
     * @param buf 数据缓存
     * @param dts 解码时间戳
     * @param pts 显示时间戳
     * @param prefix 帧前缀长度
     * @param offset buffer有效数据偏移量
     * @param codec 帧类型
     */
    FrameWrapper(const Buffer::Ptr &buf, uint32_t dts, uint32_t pts, size_t prefix, size_t offset, CodecId codec) : Parent(codec, buf->Data() + offset, buf->Size() - offset, dts, pts, prefix){
        buf_ = buf;
    }

    /**
     * 该帧可缓存
     */
    bool CacheAble() const override {
        return true;
    }

private:
    Buffer::Ptr buf_;
};

}
}

#endif // __FRAME_IMPL_H__