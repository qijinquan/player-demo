#ifndef __FRAME_H__
#define __FRAME_H__

#include "common/buffer.h"

namespace OHOS {
namespace Sharing {
/*
enum TrackType{
    TRACK_INVALID = -1,
    TRACK_VIDEO=0,
    TRACK_AUDIO,
    TRACK_TITLE,
    TRACK_APPLICATION,
    TRACK_MAX
};*/
typedef enum
{
    TRACK_INVALID = -1,
    TRACK_VIDEO = 0,
    TRACK_AUDIO,
    TRACK_TITLE,
    TRACK_APPLICATION,
    TRACK_MAX
}TrackType;

#define CODEC_MAP(XX) \
    XX(CodecH264,  TRACK_VIDEO, 0, "H264", PSI_STREAM_H264)          \
    XX(CodecH265,  TRACK_VIDEO, 1, "H265", PSI_STREAM_H265)          \
    XX(CodecAAC,   TRACK_AUDIO, 2, "mpeg4-generic", PSI_STREAM_AAC)  \
    XX(CodecG711A, TRACK_AUDIO, 3, "PCMA", PSI_STREAM_AUDIO_G711A)   \
    XX(CodecG711U, TRACK_AUDIO, 4, "PCMU", PSI_STREAM_AUDIO_G711U)   \
    XX(CodecOpus,  TRACK_AUDIO, 5, "opus", PSI_STREAM_AUDIO_OPUS)    \
    XX(CodecL16,   TRACK_AUDIO, 6, "L16", PSI_STREAM_RESERVED)       \
    XX(CodecVP8,   TRACK_VIDEO, 7, "VP8", PSI_STREAM_VP8)            \
    XX(CodecVP9,   TRACK_VIDEO, 8, "VP9", PSI_STREAM_VP9)            \
    XX(CodecAV1,   TRACK_VIDEO, 9, "AV1X", PSI_STREAM_AV1)

typedef enum {
    CodecInvalid = -1,
#define XX(name, type, value, str, mpeg_id) name = value,
    CODEC_MAP(XX)
#undef XX
    CodecMax
} CodecId;

/**
 * 字符串转媒体类型转
 */
TrackType GetTrackType(const std::string &str);

/**
 * 媒体类型转字符串
 */
const char* GetTrackString(TrackType type);

/**
 * 根据SDP中描述获取codec_id
 * @param str
 * @return
 */
CodecId GetCodecId(const std::string &str);

/**
 * 获取编码器名称
 */
const char *GetCodecName(CodecId codecId);

/**
 * 获取音视频类型
 */
TrackType GetTrackType(CodecId codecId);

/**
 * 编码信息的抽象接口
 */
class CodecInfo {
public:
    typedef std::shared_ptr<CodecInfo> Ptr;

    CodecInfo(){}
    virtual ~CodecInfo(){}

    /**
     * 获取编解码器类型
     */
    virtual CodecId GetCodecId() const = 0;

    /**
     * 获取编码器名称
     */
    const char *GetCodecName() const;

    /**
     * 获取音视频类型
     */
    TrackType GetTrackType() const;
};

/**
 * 帧类型的抽象接口
 */
class Frame : public Buffer, public CodecInfo {
public:
    using Ptr = std::shared_ptr<Frame>;
    virtual ~Frame(){}

    /**
     * 返回解码时间戳，单位毫秒
     */
    virtual uint32_t Dts() const = 0;

    /**
     * 返回显示时间戳，单位毫秒
     */
    virtual uint32_t Pts() const {
        return Dts();
    }

    /**
     * 前缀长度，譬如264前缀为0x00 00 00 01,那么前缀长度就是4
     * aac前缀则为7个字节
     */
    virtual size_t PrefixSize() const = 0;

    /**
     * 返回是否为关键帧
     */
    virtual bool KeyFrame() const = 0;

    /**
     * 是否为配置帧，譬如sps pps vps
     */
    virtual bool ConfigFrame() const = 0;

    /**
     * 是否可以缓存
     */
    virtual bool CacheAble() const { return true; }

    /**
     * 该帧是否可以丢弃
     * SEI/AUD帧可以丢弃
     * 默认都不能丢帧
     */
    virtual bool DropAble() const { return false; }

    /**
     * 是否为可解码帧
     * sps pps等帧不能解码
     */
    virtual bool DecodeAble() const {
        if (GetTrackType() != TRACK_VIDEO) {
            //非视频帧都可以解码
            return true;
        }
        //默认非sps pps帧都可以解码
        return !ConfigFrame();
    }

    /**
     * 返回可缓存的frame
     */
    static Ptr GetCacheAbleFrame(const Ptr &frame);
};

}
}

#endif // __FRAME_H__