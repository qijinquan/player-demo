#ifndef __H264_FRAME_H__
#define __H264_FRAME_H__

#include <cstdlib>
#include <memory>
#include <string>
#include "frame_impl.h"

namespace OHOS {
namespace Sharing {

#define H264_TYPE(v) ((uint8_t)(v) & 0x1F)

size_t PrefixSize(const char *ptr, size_t len);
void SplitH264(const char *ptr, size_t len, size_t prefix, const std::function<void(const char *, size_t, size_t)> &cb);

template<typename Parent>
class H264FrameHelper : public Parent{
public:
    using Ptr = std::shared_ptr<H264FrameHelper>;

    enum {
        NAL_IDR = 5,
        NAL_SEI = 6,
        NAL_SPS = 7,
        NAL_PPS = 8,
        NAL_AUD = 9,
        NAL_B_P = 1,
    };

    template<typename ...ARGS>
    H264FrameHelper(ARGS &&...args): Parent(std::forward<ARGS>(args)...) {
        this->codecId_ = CodecH264;
    }

    ~H264FrameHelper() = default;

    bool KeyFrame() const override
    {
        auto nalPtr = (uint8_t *)this->Data() + this->PrefixSize();
        return H264_TYPE(*nalPtr) == NAL_IDR && DecodeAble();
    }

    bool ConfigFrame() const override
    {
        auto nalPtr = (uint8_t *)this->Data() + this->PrefixSize();
        switch (H264_TYPE(*nalPtr)) {
            case NAL_SPS:
            case NAL_PPS:
                return true;
            default:
                return false;
        }
    }

    bool DropAble() const override
    {
        auto nalPtr = (uint8_t *)this->Data() + this->PrefixSize();
        switch (H264_TYPE(*nalPtr)) {
            case NAL_SEI:
            case NAL_AUD:
                return true;
            default:
                return false;
        }
    }

    bool DecodeAble() const override
    {
        auto nalPtr = (uint8_t *)this->Data() + this->PrefixSize();
        auto type = H264_TYPE(*nalPtr);
        //多slice情况下, first_mb_in_slice 表示其为一帧的开始
        return type >= NAL_B_P && type <= NAL_IDR && (nalPtr[1] & 0x80);
    }

};

using H264Frame = H264FrameHelper<FrameImp>;

using H264FrameNoCacheAble = H264FrameHelper<FrameFromPtr>;

}
}

#endif // __H264_FRAME_H__