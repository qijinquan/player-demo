#include "sdp_parser.h"
#include <sstream>
#include "utils/utils.h"
namespace OHOS::Sharing {
int32_t SdpFmtp::ParseLine(std::string &line)
{
    auto pos = line.find(" ");
    pt = std::atoi(line.substr(0, pos).c_str());
    value = line.substr(pos + 1);
    return 0;
}
int32_t SdpMedia::ParseLine(std::string &line)
{
    auto vec = Split(line, " ");
    int32_t index = 0;
    for (auto &item : vec) {
        switch (index) {
            case 0:
                mediaType = item;
                break;
            case 1:
                mediaPort = atoi(item.c_str());
                break;
            case 2:
                protocol = item;
                break;
            case 3:
                pt = atoi(item.c_str());
                break;
        }
        index++;
    }
    return 0;
}

int32_t SdpMedia::ParseAttr(std::string &line)
{
    auto vec = Split(line, ":");
    if (vec.size() == 2) {
        if (vec[0] == "rtpmap") {
            rtpMap = std::make_shared<SdpRtpMap>();
            rtpMap->ParseLine(vec[1]);
        } else if (vec[0] == "control") {
            control = vec[1];
        } else if (vec[0] == "fmtp") {
            fmtp = std::make_shared<SdpFmtp>();
            fmtp->ParseLine(vec[1]);
        }
    }
    return 0;
}
int32_t SdpRtpMap::ParseLine(std::string &rtpMap)
{
    // a=rtpmap:96 H264/90000
    // a=rtpmap:97 MP4A-LATM/48000/2  rtpmap:8 PCMA/8000/1
    std::vector<std::string> vec = Split(rtpMap, " ");
    if (vec.size() != 2) {
        return 0;
    }
    pt = std::atoi(vec[0].c_str());

    auto line = vec[1];
    vec.clear();
    vec = Split(line, "/");
    int32_t index = 0;
    for (auto &item : vec) {
        switch (index) {
            case 0:
                format = item;
                break;
            case 1:
                feq = std::atoi(item.c_str());
                break;
            case 2:
                channels = std::atoi(item.c_str());
                break;
            default:
                break;
        }
        index++;
    }
    return 0;
}

SdpParser::SdpParser(const std::string &sdp)
{
    LoadSdp(sdp);
}

void SdpParser::LoadSdp(const std::string &sdp)
{
    std::vector<std::string> lines = Split(sdp, "\r\n");  // TODO: check
    SdpMedia::Ptr media = nullptr;
    for (auto &line : lines) {
        // RtspUtil::TrimAll(line);
        if (line.length() < 2 || line[1] != '=') {
            continue;
        }
        char opt = line[0];
        std::string val = line.substr(2);
        switch (opt) {
            case 'm': {
                media = std::make_shared<SdpMedia>();
                media->ParseLine(val);
                medias_.emplace_back(media);
                break;
            }
            case 'a': {
                if (media) {
                    media->ParseAttr(val);
                } else {
                    ParseLine(opt, val);
                }

                break;
            }
            default:
                ParseLine(opt, val);
                break;
        }
    }
}

int32_t SdpParser::ParseLine(char type, std::string &line)
{
    switch (type) {
        case 'v':
            version_ = atoi(line.c_str());
            break;  // v=<version>
        case 'o':
            origin_ = line;
            break;  // o=<Origin Info>
        case 's':
            name_ = line;
            break;   // s=<Session Name>
        case 'c': {  // c=<Connection Info>
            std::stringstream ss;
            ss << line;
            std::string tmp;
            ss >> tmp;
            ss >> tmp;
            ss >> connectAddr_;
            break;
        }
        case 't':   // TODO, start time, end time ,duration
            break;  // t=<Time>
        case 'a': {
            std::vector<std::string> vec = Split(line, ":");
            if (vec.size() == 2) {
                attr.emplace(vec[0], vec[1]);
            }
            break;
        }
    }
    return 0;
}

std::vector<SdpMedia::Ptr>& SdpParser::GetAllMedias()
{
    return medias_;
}

void SdpParser::Clear()
{
    connectAddr_ = "";
    name_ = "";
    origin_ = "";
    version_ = 0;
    medias_.clear();
    attr.clear();
    duration_ = 0;
    start_ = 0;
    end_ = 0;
}
}  // namespace OHOS::Sharing