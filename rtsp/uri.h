#ifndef URI_H
#define URI_H
#include <string>
#include <iostream>
#include <memory>
namespace OHOS {
namespace Sharing {
class Uri {
public:
    using Ptr = std::shared_ptr<Uri>;
    Uri();
    Uri(std::string &url);
    std::string &GetHost();
    std::string &GetHref();
    std::string &GetPath();
    std::string &GetProtocol();
    std::string &GetHostName();
    uint32_t GetPort();

    void SetHost(std::string &host);
    void SetHref(std::string &href);
    void SetProtocol(std::string &protocol);
    void SetPath(std::string &path);
    void SetUserName(std::string &userName);
    void SetPassword(std::string &password);
    void SetPort(uint32_t port);
    bool Parse(const std::string &url);

private:
    std::string href_;
    std::string protocol_;
    std::string host_;
    std::string path_;
    std::string userName_;
    std::string passWord_;
    uint32_t port_;
};
}  // namespace OHOS
}// namespace Sharing
#endif  // URI_H