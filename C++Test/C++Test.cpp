﻿// C++Test.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
#include <iostream>
//#include "memwatch.h"
#include <functional>
#include <memory>
#include <map>
#include <sstream>
#include <set>
#include <stack>
#include <fstream>
int gargc;
char** gargv = nullptr;
using namespace std;
/*
void* operator new(size_t size) {
    return malloc(size);
}

void* operator new[](size_t size) {
    //return mwMalloc(size, file, line);
    return  malloc(size);
}
void operator delete(void *p) {
    free(p);
}
void operator delete[](void *p) {
    free(p);
}
*/
class B;
class A : public enable_shared_from_this<A>
{
public:
    using Ptr = std::shared_ptr<A>;
    using WPtr = std::weak_ptr<A>;
    A(){cout<<"A()==="<<endl;}
    A(const A&a){ cout << "A(&a)===" << endl; }
    A(const A&&a) { cout << "A(&&a)===" << endl; }
    ~A(){ cout << "~A()===" << endl; }
    A& operator=(const A&a){ cout << "Operator=A(&a)===" << endl;return *this; }
    A& operator=(const A&&a) { cout << "Operator=A(&&a)===" << endl;return *this; }
    void Print(){ cout << "Print===" << endl; }
    void RegisterB(shared_ptr<B> b) {
        b_ = b;
        //b_->RegisterA(shared_from_this());
    }
 private:
    shared_ptr<B> b_;
};

class B
{
public:
    using Ptr = std::shared_ptr<B>;
    
    B() { cout << "B()===" << endl; }
    ~B() { cout << "~B()===" << endl; }
    B(const B&o)
    {
        a_ = o.a_;
        //stream_ = std::move(o.stream_);
    }
    void RegisterA(A::Ptr a) {
        a_ = a;
    }
    private:
    stringstream stream_;
    A::WPtr a_;
};
struct DisableCompare {
    bool operator()(std::string a, std::string b) const {
        if (a.compare(b))
        {
            return false;
        }
        return true;
    }
};
int main(int argc, char** argv)
{
    gargc = 0;
    gargv = nullptr;
    char* abf = nullptr;
    abf--;
    printf("0x%x\n", abf);
    vector<uint8_t> vecs;
    vecs.resize(10);
    memcpy(vecs.data(), "1234567", strlen("1234567"));
    //mwInit();
    //auto a = std::make_shared<A>();
    //std::cout<<"1111"<<std::endl;
    //int* inta = new int(10);
   // char* p = (char*)malloc(10);
    //int* test = new int(10);
    // a = nullptr;
    
    //mwTerm();
    std::ifstream is("123.log");
    is.read(buf,1);
    std::ofstream is2("1232.log", ios::trunc);
    std::stack<size_t> myStack;
    cout<<"sizeof = "<<sizeof(size_t)<<endl;
    
    for (size_t i = 0; i < 1000; i++)
    {
        myStack.push(i);
    }
    for (size_t i = 0; i < 10; i++)
    {
        myStack.push(i);
    }
    cout<< myStack.size()<<endl;
   
    std::map<string, int, DisableCompare> testMap;
    testMap.insert({ "2",2 });
    testMap.insert({ "1",1 });
    testMap.insert({ "3",3 });
    cout<<"map size :" <<testMap.size()<<endl;
    for (auto&  pair : testMap)
    {
        cout<<"key:"<<pair.first <<", value:"<<pair.second<<endl;
    }
    return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
