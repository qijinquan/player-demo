﻿// YuvScale.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <malloc.h>
using namespace std;
#define WIDTH 640
#define HEIGHT 480
#define DSTWIDTH (WIDTH/2)
#define DSTHEIGHT (HEIGHT/2)
void ProcessYUV()
{

    stringstream ss;
    ss << WIDTH << "x" << HEIGHT << ".yuv";
    ifstream fsIn(ss.str().c_str(), std::ofstream::binary);
    ofstream fsOut("out.yuv", std::ofstream::binary | std::ofstream::trunc);
    char buf[WIDTH*HEIGHT * 3 / 2] = { 0 };
    char dstBuf[DSTWIDTH*DSTHEIGHT * 3 / 2] = { 0 }; //存储一半宽、高的yuv420sp的数据
    char* tmpBuf = new char[WIDTH*HEIGHT * 3];//先存所有Y，再存所有U，再存所有V， YUV444P
    int count = 0;
    while (!fsIn.eof())
    {
        count++;
        fsIn.read(buf, sizeof(buf));
        // SRC:YUV420SP格式--NV12
        /*
        *   Y00, Y01,Y02,Y03
        *   Y10, Y11,Y12,Y13
        *   U00, V00,U01,V01
        */
        
        memcpy(tmpBuf, buf, WIDTH*HEIGHT);
        char* tmpU = tmpBuf + WIDTH * HEIGHT;
        char* tmpV = tmpU + WIDTH * HEIGHT;
        //保存所有的u和v
        char* srvUv = buf + WIDTH * HEIGHT;
        for (size_t i = 0; i < HEIGHT; i++)
        {
            for (size_t j = 0; j < WIDTH/2; j++)
            {   
                char u = srvUv[i/2 * WIDTH + 2 * j];
                char v = srvUv[i/2 * WIDTH + 2 * j+1];
                tmpU[i*WIDTH + 2*j] = u;
                tmpU[i*WIDTH + 2*j+1] = u;
                tmpV[i*WIDTH + 2 * j] = v;
                tmpV[i*WIDTH + 2 * j + 1] = v;
            }
        }
        
        for (size_t i = 0; i < DSTHEIGHT; i++)
        {
            for (size_t j = 0; j < DSTWIDTH; j++)
            {
                //y
                dstBuf[i* DSTWIDTH + j] = tmpBuf[2*i*WIDTH + j*2];//取（0,0），（0,2），（2,0），（2,2）的数据
            }
        }
        /*
        // yuv420p
        //抽一半数据
        char* uStart = dstBuf + DSTWIDTH * DSTHEIGHT;
        char* vStart = dstBuf + DSTWIDTH * DSTHEIGHT*5/4;
        for (size_t i = 0; i < DSTHEIGHT/2; i++)
        {
            for (size_t j = 0; j < DSTWIDTH/2; j++)
            {
                //u, i<=w/4, j<=h/4
                uStart[i*DSTWIDTH /2 + j] = tmpU[4 * i*WIDTH + j * 4];//取（0,0），（0,2），（2,0），（2,2）的数据
                //v
                vStart[i*DSTWIDTH /2 + j] = tmpV[4 * i*WIDTH + j * 4];//取（0,0），（0,2），（2,0），（2,2）的数据
            }
        }
        */
        // yuv420sp
        char* uvStart = dstBuf + DSTWIDTH * DSTHEIGHT;
        for (size_t i = 0; i < DSTHEIGHT / 2; i++)
        {
            for (size_t j = 0; j < DSTWIDTH / 2; j++)
            {
                //u, i<=w/4, j<=h/4
                *uvStart++ = tmpU[4 * i*WIDTH + j * 4];//取（0,0），（0,2），（2,0），（2,2）的数据
                //v
                *uvStart++ = tmpV[4 * i*WIDTH + j * 4];//取（0,0），（0,2），（2,0），（2,2）的数据
            }
        }
        fsOut.write(dstBuf, DSTWIDTH * DSTHEIGHT*3/2);
        fsOut.flush();
        
        /*
        //抽一半数据
        char* yvStart = buf + WIDTH * HEIGHT;
        for (size_t i = 0; i < DSTHEIGHT / 2; i++)
        {
            for (size_t j = 0; j < DSTWIDTH / 2; j++)
            {
                // 第一个数据是u,第二个数据是v
                short U = (int8_t)(*(yvStart + i * WIDTH + j)) + (int8_t)(*(yvStart + i * WIDTH + j + 2)) + (int8_t)(*(yvStart + (i + 1) * WIDTH + j)) + (int8_t)(*(yvStart + (i + 1) * WIDTH + j + 2));
                short V = (int8_t)(*(yvStart + i * WIDTH + j + 1)) + (int8_t)(*(yvStart + i * WIDTH + j + 3)) + (int8_t)(*(yvStart + (i + 1) * WIDTH + j + 1)) + (int8_t)(*(yvStart + (i + 1) * WIDTH + j + 3));
                dstBuf[DSTHEIGHT * DSTWIDTH + i * DSTWIDTH + 2 * j] = (char)(U / 4);
                dstBuf[DSTHEIGHT * DSTWIDTH + i * DSTWIDTH + 2 * j + 1] = (char)(V / 4);
            }
        }
        */
        //UV 要展开到y
        /*
        for (size_t i = 0; i < DSTHEIGHT / 2; i++)
        {
            for (size_t j = 0; j < DSTWIDTH / 2; j++)
            {
                // 第一个数据是u,第二个数据是v
                dstBuf[DSTHEIGHT * DSTWIDTH + i * DSTWIDTH + 2 * j ] = buf[WIDTH*HEIGHT + i * WIDTH + j * 4 ];
                dstBuf[DSTHEIGHT * DSTWIDTH + i * DSTWIDTH + 2 * j + 1] = buf[WIDTH*HEIGHT + i * WIDTH + j * 4 + 1];
            }
        }
        */
        //铺满整个数据，如果是缩小一半，则是取周边4个数据的平均数据,最后UV部分，还是会有问题
        /*
        char* yvStart = buf + WIDTH*HEIGHT;
        for (size_t i = 0; i < DSTHEIGHT / 2; i++)
        {
            for (size_t j = 0; j < DSTWIDTH / 2; j++)
            {
                // 第一个数据是u,第二个数据是v
                short U = (int8_t)(*(yvStart + i * WIDTH + j)) + (int8_t)(*(yvStart + i * WIDTH + j + 2)) + (int8_t)(*(yvStart + (i+1) * WIDTH + j)) + (int8_t)(*(yvStart + (i + 1) * WIDTH + j + 2));
                short V = (int8_t)(*(yvStart + i * WIDTH + j + 1)) + (int8_t)(*(yvStart + i * WIDTH + j + 3)) + (int8_t)(*(yvStart + (i + 1) * WIDTH + j + 1)) + (int8_t)(*(yvStart + (i + 1) * WIDTH + j + 3));
                dstBuf[DSTHEIGHT * DSTWIDTH + i * DSTWIDTH + 2 * j] =  (char)(U/4);
                dstBuf[DSTHEIGHT * DSTWIDTH + i * DSTWIDTH + 2 * j + 1] = (char)(V / 4);
            }
        }
        fsOut.write(dstBuf, sizeof(dstBuf));
        fsOut.flush();
        */
        
    }
    cout << "Run:" << count << endl;
}

void ProcessRGBA()
{
#ifdef WIDTH
#undef WIDTH
#endif
#define WIDTH 720

#ifdef HEIGHT
#undef HEIGHT
#endif

#define HEIGHT 480
#ifdef DSTWIDTH
#undef DSTWIDTH
#endif
#define DSTWIDTH (WIDTH/2)

#ifdef DSTHEIGHT
#undef DSTHEIGHT
#endif
#define DSTHEIGHT (HEIGHT/2)

    stringstream ss;
    ss << "rgba32_"<<WIDTH << "x" << HEIGHT << ".RAW";
    ifstream fsIn(ss.str().c_str(), std::ofstream::binary);
    ofstream fsOut("out.RAW", std::ofstream::binary | std::ofstream::trunc);
    //char buf[WIDTH*HEIGHT * 4] = { 0 };
    char* buf = new char[WIDTH*HEIGHT * 4];
    char* dstBuf = new char[DSTWIDTH*DSTHEIGHT * 4];
    int count = 0;
    while (!fsIn.eof())
    {
        count++;
        fsIn.read(buf, WIDTH*HEIGHT * 4);
        // YUV420SP格式

        for (size_t i = 0; i < DSTHEIGHT; i++)
        {
            for (size_t j = 0; j < DSTWIDTH; j++)
            {
                dstBuf[4*i*DSTWIDTH + 4*j] = buf[4 * 2 * i*WIDTH + 2*j * 4];
                dstBuf[4 * i*DSTWIDTH + 4*j + 1] = buf[4 * 2 * i*WIDTH + 2 * j * 4 + 1];
                dstBuf[4 * i*DSTWIDTH + 4*j + 2] = buf[4 * 2 * i*WIDTH + 2 * j * 4 + 2];
                dstBuf[4 * i*DSTWIDTH + 4*j + 3] = buf[4 * 2 * i*WIDTH + 2 * j * 4 + 3];
            }
        }

        fsOut.write(dstBuf, DSTWIDTH*DSTHEIGHT * 4);
        fsOut.flush();
    }
    delete[] dstBuf;
    delete[] buf;
    cout << "Run:" << count << endl;
    
}

int main()
{
    ProcessYUV();
    //ProcessRGBA();
    return 0;
}
