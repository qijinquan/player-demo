﻿// CasTest.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <memory>
#include <atomic>
#include <thread>
using namespace std;
template<class T>
struct Node
{
    using Ptr = shared_ptr<Node>;
    T val;
    Ptr next = nullptr;
};

template<typename T>
class ConcurrentQueue
{
public:
    bool Push(T& v)
    {
        auto node = make_shared<Node<T>>();
        node->val = v;
        node->next = nullptr;
        std::shared_ptr<Node<T>> lastNode = nullptr;
        // lastNode 指向自己
        lastNode = atomic_exchange(&lastNode, node);
        // last的next指向自己
        atomic_store(&(lastNode->next), node);
        ++size_;
        return true;
    }

    bool Pop(T &v)
    {
        std::shared_ptr<Node<T>> fristNode = nullptr;
        std::shared_ptr<Node<T>> fristNodeNext = nullptr;
        do
        {
            fristNode = std::atomic_load(&first_);
            if (fristNode == nullptr) {
                return false;
            }
            fristNodeNext = std::atomic_load(&(first_->next));
            if (fristNodeNext == nullptr)
            {
                return false;
            }
        } while (!atomic_compare_exchange_strong(&first_, &fristNode, fristNodeNext));
        --size_;
        v = std::move(fristNodeNext->val);
        return true;
    }
private:
    std::shared_ptr<Node<T>> first_;
    //atomic_bool bPop_;
    std::shared_ptr<Node<T>> last_;
    atomic<uint32_t> size_ = 0;
};
using Data = struct
{
    int id;
    int data;
};
std::shared_ptr<ConcurrentQueue<Data>> queue = nullptr;
int main()
{
    queue = std::make_shared<ConcurrentQueue<Data>>();
    thread t1([]{
        std::cout<<"Run"<<endl;
        for (int i = 0; i < 50; i++)
        {
            Data data;
            data.id = 1;
            data.data = i+1;
            queue->Push(data);
        }
        });
        /*
    thread t2([] {
        std::cout << "T2 Run" << endl;
        for (int i = 0; i < 50; i++)
        {
            Data data;
            data.id = 1;
            data.data = i + 1;
            queue->Push(data);
        }
    });*/
    thread t3([] {
        std::cout << "T3 Run" << endl;
        Data data;
        do
        {   
            if (queue->Pop(data))
            {
                cout<<"id:"<<data.id<<" ,data="<<data.data<<endl;
            }
        } while (true);
    });

    t1.detach();
    //t2.detach();
    t3.join();
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
