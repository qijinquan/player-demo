#pragma once
#include <cinttypes>
#include <memory>
/*
UDP头
source port 2Byte
Dst Port : 2Byte
Length: 2字节
CheckSum: 2字节
*/
/*
    0               1               2               3
 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|v=2|P|X|   CC  |M|      PT     |        sequence number        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                           timestamp                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|           synchronization source(SSRC) identifier             |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| contributing source(CSRC) identifiers |
|                             ….|
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  */
/*
V RTP版本，固定为2
P 填充，最后一个byte 表示可以负载中填充的数据
X 扩展， 如果为1，固定头后，跟着扩展头（CSRC后）
CC CSRC数量，
M 标志，具体协议具体定义，如流协议中的帧边界
PT 负责类型
seq 序列号，每发送一个RTP包，序列号加1
timestamp： 时间戳，RTP数据中第一个字节的采样时间，对多包传输时，其时间戳是一样的
SSRC：同步源，
CSRC 贡献源
*/
#pragma pack(1)
typedef struct {
  //小端序时将数据倒着来
  uint32_t cc : 4;
  uint32_t x : 1;
  uint32_t p : 1;
  uint32_t v : 2; // protocol version

  uint32_t pt : 7;
  uint32_t m : 1;
  
  uint32_t seq : 16;
  uint32_t timestamp;
  uint32_t ssrc;
}RtpHead;

#pragma pack()

class RtpPacket {
public:
  using Ptr = std::shared_ptr<RtpPacket>;
  RtpPacket() = default;
  ~RtpPacket();
  RtpHead head;
  uint32_t csrc[16] = {0};
  const void *extension = nullptr;
  uint16_t extlen = 0;
  uint16_t reserved = 0;
  int payloadLen = 0;
  uint8_t *payload = nullptr;
  const uint8_t* origin = nullptr;
  int Deserialize(const uint8_t* data, int bytes);
};
