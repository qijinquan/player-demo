#include "RtpPacket.h"
#include <memory>
#include <WinSock2.h>
RtpPacket::~RtpPacket() {
  if (origin) {
    delete[] origin;
    origin = nullptr;
  }
}
int RtpPacket::Deserialize(const uint8_t *data, int bytes) {
    origin = data;
    int hdrLen = sizeof(head);
    memcpy(&head, data, hdrLen);
    head.seq = ntohs(head.seq);
    head.timestamp = ntohl(head.timestamp);
    uint8_t *ptr = (uint8_t *)data;
    for (size_t i = 0; i < head.cc; i++) {
      memcpy((void *)&csrc[i], ptr + hdrLen, 4);
      hdrLen += 4;
    }
    /*
    payloadLen = bytes - hdrLen;
    payload = ptr + hdrLen;
    */
    if (1 == head.x) {
      ;// TODO:
    }
    payload = ptr;
    payloadLen = bytes - hdrLen;

    if (1 == head.p) {
      uint8_t padding = ((uint8_t*)data)[bytes - 1];
      payloadLen -= padding;
    }
    return 0; 
}
