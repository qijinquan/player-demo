﻿// Rtp.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "RtpPacket.h"
#include <WinSock2.h>
#include <cinttypes>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>
#include <thread>
#include <variant>
#include <map>

using namespace std;
class A {
public:
  A() { std::cout << "A construct" << std::endl; }
  ~A() { std::cout << "A DesConstruct" << std::endl; }
  A(const A &a) {
    std::cout << "A construct(A&a)" << std::endl;
    this->i = a.i;
  }
  A(const A &&a) {
    std::cout << "A construct(A&a)" << std::endl;
    this->i = a.i;
  }
  A &operator=(A &other) {
      std::cout << "A operator=(A&a)" << std::endl;
    this->i = other.i;
    return *this;
  }
  A &operator=(A &&other) {
      std::cout << "A operator=(A&&a)" << std::endl;
    this->i = other.i;
    return *this;
  }
  void Print() {
    this_thread::sleep_for(std::chrono::milliseconds(10));
    cout << "Print" << i << endl;
  }

private:
  int i;
};
//static A a;
//A &GetInstance() { return a; }

class VariantT {
public:
  VariantT(int32_t data) {
    var_ = data;
  }
  VariantT(std::string data) {
      var_ = data;
  }
  bool IsInt() {   
    return var_.index() == 0;
  }
  bool IsString() {
      return var_.index() == 1;
  }
  int32_t GetInt() { 
    return std::get<int32_t>(var_);
  }
  std::string GetString() { 
    return std::get<std::string>(var_); 
  }
 private:
  variant<int32_t, std::string> var_;
};
int main() {
  /*
  fstream file("export.txt");
  char buf[6000] = {0};
  file.getline(buf, 6000);
  size_t len = strlen(buf);
  len /= 3;
  uint8_t c = 0;
  int count = 0x2a;
  uint8_t *data = new uint8_t[len - 0x2a];
  int i = 0;
  do {
    sscanf_s(buf + count * 3, "%hhx", &c);
    count++;
    data[i++] = c;
  } while (count < len);
  auto rtp = std::make_shared<RtpPacket>();
  rtp->Deserialize(data, len - 0x2a);
  */
  /*
  GetInstance();
  std::thread t1([&](){ 
  GetInstance().Print();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    GetInstance().Print();
  });
  t1.detach();
  std::cout << "Hello World!\n";

  VariantT var("123");
  VariantT var2(123);

  std::cout << "IsInt:"<< var2.IsString()<<std::endl;
  std::cout << "IsString:" << var.IsString() << std::endl;
  */
    {
        A a;
        map<uint32_t, A> maps;
        maps.insert({ 1,a });
    }
    std::cout<<"******"<<std::endl;
    {
        A a;
        map<uint32_t, A> maps;
        maps[1] = std::move(a);
    }
    std::cout << "******" << std::endl;
    {
        A a;
        map<uint32_t, A> maps;
        maps.insert({ 1,std::move(a) });
    }
  return 0;
}
