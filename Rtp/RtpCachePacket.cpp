#include "RtpCachePacket.h"

RtpCachePacket::RtpCachePacket() {
    caps = 30;
    caches.resize(caps);
}

void RtpCachePacket::PushOnePacket(RtpPacket::Ptr pkt) {
  uint32_t pos = pkt->head.seq % caps;
  caches[pos] = pkt;
}

RtpPacket::Ptr RtpCachePacket::GetNextPacket() {
    //获取下一包
  return nullptr;
}
