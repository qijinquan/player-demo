#pragma once
#include "RtpPacket.h"
#include <vector>
struct Frame {
    char data[1920*1080*2];
    int dataLen;
    int frameNum;
};
class RtpCachePacket {
public:
  RtpCachePacket();
  ~RtpCachePacket() = default;
  void PushOnePacket(RtpPacket::Ptr pkt);
  RtpPacket::Ptr GetNextPacket();
  private:
  int caps = 30;
  std::vector<RtpPacket::Ptr> caches;
  uint32_t readPos = 0;
  uint32_t writePos = 0;
};
