﻿// config.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <map>
#include <any>
#include <list>
#include <tuple>
#include <shared_mutex>
using KeyValue = std::map < std::string, std::any>;
using KeyValuePtr = std::map < std::string, std::shared_ptr<std::any>>;
using ObjectValue = std::map<std::string, std::any>;
using ObjectValuePtr = std::shared_ptr<ObjectValue>;
void PrintKeyValue(KeyValue& config)
{
    for (auto iter = config.begin(); iter != config.end(); iter++)
    {
        std::cout << "key:" << iter->first.c_str() << std::endl;
        std::cout << "value:" << std::endl;
        std::any& any = iter->second;
        if (typeid(KeyValue) == any.type())
        {
            KeyValue& cfg = std::any_cast<KeyValue&>(any);
            PrintKeyValue(cfg);
        }
        else if (typeid(int) == any.type())
        {
            std::cout << std::any_cast<int>(any) << std::endl;
        }
        else if (typeid(std::string) == any.type())
        {
            std::string& temp = std::any_cast<std::string&>(any);
            std::cout << temp.c_str() << std::endl;
        }
    }
}

void PrintKeyValuePtr(ObjectValuePtr& config)
{
    for (auto iter = config->begin(); iter != config->end(); iter++)
    {
        std::cout << "key:" << iter->first.c_str() << std::endl;
        std::cout << "value:" << std::endl;
        std::any any = iter->second;
        if (typeid(ObjectValuePtr) == any.type())
        {
            ObjectValuePtr cfg = std::any_cast<ObjectValuePtr>(any);
            PrintKeyValuePtr(cfg);
        }
        else if (typeid(int) == any.type())
        {
            std::cout << std::any_cast<int>(any) << std::endl;
        }
        else if (typeid(std::string) == any.type())
        {
            std::string& temp = std::any_cast<std::string&>(any);
            std::cout << temp.c_str() << std::endl;
        }
    }
}


int main()
{
    KeyValue modelConfigs;
    KeyValue common;
    KeyValue local;
    KeyValue obj;
    common.emplace("name", "name");
    common.emplace("obj", obj);
    obj.emplace("age", 1);
    obj.emplace("name", "Jim");
    local.emplace("port", 1);
    modelConfigs.emplace("common", common);
    modelConfigs.emplace("local", local);
    
    //PrintKeyValue(modelConfigs);
    


    auto cfg = std::make_shared<ObjectValue>();
    auto tag1 = std::make_shared<ObjectValue>();
    auto tag2 = std::make_shared<ObjectValue>();
    cfg->emplace("tag1", tag1);
    cfg->emplace("tag2", tag2);

    tag1->emplace("key1", 1);
    tag1->emplace("key2", "2");

    tag2->emplace("key1", "3");
    tag2->emplace("key2", "4");
    PrintKeyValuePtr(cfg);
    std::cout << "Hello World!\n";

    std::tuple tup = std::make_tuple<std::string, std::any>("key1", 1);

    std::shared_mutex rwMutex;
    //读锁
    std::shared_lock<std::shared_mutex> rLock;
    std::unique_lock<std::shared_mutex> wLock;

}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
