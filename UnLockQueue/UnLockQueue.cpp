﻿// UnLockQueue.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <atomic>
#include <cassert>
#include "LockFreeQueue.h"
//#include <hash_map>
#include <unordered_map>
#include <map>
#include <thread>
using namespace std;
using TaskPtr = std::shared_ptr<thread>;

// 升序排列
template<class T>
struct Compare
{
    constexpr bool operator()(const T& _Left, const T& _Right) const
    {	// apply operator< to operands
        return (_Left > _Right);
    }
};
std::atomic<int> cnt = { 0 };

void f()
{
    for (int n = 0; n < 1000; ++n) {
        cnt.fetch_add(1, std::memory_order_relaxed);
    }
}
std::atomic<std::string*> ptr;
static int d1;
static int d2;
void producer()
{
    std::string* p = new std::string("Hello");
    d1 = 42;
    ptr.store(p, std::memory_order_relaxed);//对参数的读写不会重排到这之后，所有的写入可见于获得同一个原子变量的其它线程，d1 可被其它线程读取
    //std::this_thread::sleep_for(std::chrono::milliseconds(1));
    d2 = 43;
}

void consumer()
{
    std::string* p2;
    while (!(p2 = ptr.load(std::memory_order_relaxed)))// 对参数的读写不会重排到前面，release的原子写入，能被正确即时读到
        ;
    assert(*p2 == "Hello"); // 绝无问题
    //assert(d1 == 42); // 绝无问题
    //assert(d2 == 43); //d2 可能为0
    cout<<"d1 = "<<d1<<endl;
    cout<<"d2 = "<<d2<<endl;

}
int main()
{
    atomic<int> a = 1;
    int b = 1;
    auto ret = a.compare_exchange_weak(b, 3);// a == b 则将a 的值修改为3，返回true， 否则返回false，其它不变
    
    std::shared_ptr<LockFreeQueue<int>> queue = std::make_shared<LockFreeQueue<int>>();
    vector<TaskPtr> tasks;
    for (size_t i = 0; i < 5; i++)
    {
        tasks.push_back(std::move(make_shared<thread>([&queue]{
            for (size_t j = 0; j < 1000; j++)
            {
                queue->Enqueue(j);
            }
        })));
    }
    for (size_t i = 0; i < 5; i++)
    {
        tasks[i]->join();
    }
    /*
    int sum = 0;
    while (true)
    {
        int a;
        if (queue->Dequeue(a)) {
            sum += a;
        }
    }
    */
    std::map<int, int, Compare<int>> maps;
    maps.insert(std::make_pair(1,1));
    maps.insert(std::make_pair(2, 2));

    std::map<int, int> maps2;
    maps2.insert(std::make_pair(1, 1));
    maps2.insert(std::make_pair(2, 2));

    std::cout << "ret="<<ret <<" a = "<<a.load()<<endl;

    std::vector<std::thread> v;
    for (int n = 0; n < 10; ++n) {
        v.emplace_back(f);
    }
    for (auto& t : v) {
        t.join();
    }
    std::cout << "Final counter value is " << cnt << '\n';
    std::thread t2(consumer);
    std::thread t1(producer);
    t2.join(); t1.join();

    return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
