#pragma once
#include <atomic>
#include <iostream>

template <typename T>
class LockFreeQueue {
public:
    LockFreeQueue() {
        Node* node = new Node();
        head_.store(node);
        tail_.store(node);
    }

    ~LockFreeQueue() {
        while (Node* node = head_.load()) {
            head_.store(node->next);
            delete node;
        }
    }

    void Enqueue(const T& value) {
        Node* node = new Node(value);
        Node* tail = tail_.load();
        Node* next = nullptr;
        while (true) {
            next = tail->next.load();
            if (!next) {
                // this == next 时，将tail->next 设置为node， 返回true， 否则返回false
                if (tail->next.compare_exchange_weak(next, node)) {
                    break;
                }
            }
            else {
                tail_.compare_exchange_weak(tail, next);
            }
        }
        tail_.compare_exchange_weak(tail, node);
    }

    bool Dequeue(T& value) {
        Node* head = head_.load();
        Node* tail = tail_.load();
        Node* next = nullptr;
        while (true) {
            next = head->next.load();
            if (head == head_.load()) {
                if (head == tail) {
                    if (!next) {
                        return false;
                    }
                    tail_.compare_exchange_weak(tail, next);
                }
                else {
                    if (head_.compare_exchange_weak(head, next)) {
                        value = next->value;
                        delete head;
                        break;
                    }
                }
            }
        }
        return true;
    }

private:
    struct Node {
        Node() : next(nullptr) {}
        Node(const T& value) : value(value), next(nullptr) {}

        T value;
        std::atomic<Node*> next;
    };

    std::atomic<Node*> head_;
    std::atomic<Node*> tail_;
};