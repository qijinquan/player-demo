#include "AudioInput.h"
#include <iostream>
extern "C" {
#include <libswresample/swresample.h>  
}
using namespace std;
AudioInput::AudioInput(std::shared_ptr<AudioBufferQ> bufferQ):bufferQ_(std::move(bufferQ)), streamIndex_(-1), running_(false)
{
    
}
AudioInput::~AudioInput()
{
    if (avcodecCtx_)
    {
        //avcodec_close(avcodecCtx_);
        avcodecCtx_ = nullptr;
    }
    if (avfCtx_)
    {
        //avformat_free_context(avfCtx_);
        avfCtx_ = nullptr;
    }
}
bool AudioInput::Init(std::string & microephone)
{
    avdevice_register_all();
    AVInputFormat* inputFormat = av_find_input_format("dshow");

    AVFormatContext* temp = nullptr;
    auto err = avformat_open_input(&temp, microephone.c_str(), inputFormat, nullptr);
    if (err != 0) {
        char buf[256] = { 0 };
        av_make_error_string(buf, sizeof(buf), err);
        cout << "avformat_open_input err: " << buf << endl;
        return false;
    }
    avfCtx_.reset(temp, [](AVFormatContext *s) {
        avformat_free_context(s);
    });

    err = avformat_find_stream_info(avfCtx_.get(), NULL);
    if (err != 0) {
        char buf[256] = { 0 };
        av_make_error_string(buf, sizeof(buf), err);
        cout << "avformat_find_stream_info err: " << buf << endl;
        return false;
    }
    for (size_t i = 0; i < avfCtx_->nb_streams; i++)
    {
        if (avfCtx_->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            streamIndex_ = i;
            break;
        }
    }
    if (streamIndex_ == -1)
    {
        return false;
    }
    return true;
}

bool AudioInput::OpenCapture()
{
    if (avfCtx_ == nullptr || streamIndex_ == -1)
    {
        return false;
    }
    // 打开解码器
    AVCodec* codec = avcodec_find_decoder(avfCtx_->streams[streamIndex_]->codecpar->codec_id);
    avcodecCtx_.reset(avcodec_alloc_context3(codec), [](AVCodecContext* avctx){
        avcodec_free_context(&avctx);
    });

    //将解码器参数保存到Context中
    avcodec_parameters_to_context(avcodecCtx_.get(), avfCtx_->streams[streamIndex_]->codecpar);

    // TODO，这个版本，avcodec_parameters_to_context调用后，这些参数没有都保存到context中，后面直接用codecpar即可
    //codecCtx->sample_fmt = (AVSampleFormat)formatCtx->streams[audioIndex]->codecpar->format; //sample_fmt 是一65535， format 是S16，不能直接赋值，setby decoder
    //avcodecCtx_->channels = avfCtx_->streams[streamIndex_]->codecpar->channels;
    //avcodecCtx_->channel_layout = avfCtx_->streams[streamIndex_]->codecpar->channel_layout;
    //avcodecCtx_->sample_rate = avfCtx_->streams[streamIndex_]->codecpar->sample_rate;

    auto err = avcodec_open2(avcodecCtx_.get(), codec, nullptr);
    if (err < 0) {
        char buf[256];
        av_make_error_string(buf, 256, err);
        std::cerr << "Can not open codec" << buf << std::endl;
        return false;
    }
    return true;
}

bool AudioInput::StartCapture()
{
    if (running_.load())
    {
        return true;
    }
    capThread_ = std::make_shared<thread>(std::bind(&AudioInput::CaptureRun, this));
    return true;
}

void AudioInput::StopCapture()
{
    running_.store(false);
    if (capThread_ && capThread_->joinable())
    {
        capThread_->join();
    }
}

void AudioInput::CaptureRun()
{
    //线程运行
    running_.store(true);

    shared_ptr<AVPacket> pkt(av_packet_alloc(), [](AVPacket* pkt) {
        av_packet_free(&pkt);
    });
    av_init_packet(pkt.get());
    shared_ptr<AVFrame> frame(av_frame_alloc(), [](AVFrame* frame) {
        av_frame_free(&frame);
    });

    shared_ptr<AVFrame> coverFrame(av_frame_alloc(), [](AVFrame* frame) {
        av_frame_free(&frame);
    });

    shared_ptr<SwrContext> swr = nullptr;
    uint8_t* audioData = nullptr;
    int audioDataSize = 0;
    int outSamples = 0;
    while (running_.load())
    {
        if (av_read_frame(avfCtx_.get(), pkt.get()) < 0)
        {
            continue;
        }
        if (avcodec_send_packet(avcodecCtx_.get(), pkt.get()) < 0)
        {
            av_packet_unref(pkt.get());
            continue;
        }
        av_packet_unref(pkt.get());
        // 这里可以是一个循环，解码器中可能存在未解码的数据，需要解码完全
        if (avcodec_receive_frame(avcodecCtx_.get(), frame.get()) == 0)
        {
            if (swr == nullptr)
            {
                swr.reset(swr_alloc(), [](SwrContext*s) {
                    swr_free(&s);
                });
                auto param = avfCtx_->streams[streamIndex_]->codecpar;
                av_opt_set_channel_layout(swr.get(), "icl", av_get_default_channel_layout(param->channels), 0);//in_channel_layout
                av_opt_set_channel_layout(swr.get(), "ocl", av_get_default_channel_layout(bufferQ_->GetChannel()), 0);//out_channel_layout
                av_opt_set_int(swr.get(), "isr", param->sample_rate, 0); //输入采样率,in_sample_rate
                av_opt_set_int(swr.get(), "osr", bufferQ_->GetSampleRate(), 0); //输出采样率,out_sample_rate
                av_opt_set_sample_fmt(swr.get(), "isf", (AVSampleFormat)frame->format, 0); //输入数据格式,in_sample_fmt
                av_opt_set_sample_fmt(swr.get(), "osf", bufferQ_->GetSampleFormat(), 0); // 输出为s16,out_sample_fmt
                swr_init(swr.get());
                //计算出输出+samples
                outSamples = av_rescale_rnd(frame->nb_samples, bufferQ_->GetSampleRate(), param->sample_rate, AV_ROUND_UP);
                audioDataSize = av_samples_get_buffer_size(NULL, bufferQ_->GetChannel(), outSamples, bufferQ_->GetSampleFormat(), 1);
                audioData = (uint8_t*)av_malloc(audioDataSize);
                coverFrame->nb_samples = outSamples;
                auto err = avcodec_fill_audio_frame(coverFrame.get(), bufferQ_->GetChannel(), bufferQ_->GetSampleFormat(), audioData, audioDataSize, 1);
            }

            auto samples = swr_convert(swr.get(), coverFrame->data, coverFrame->nb_samples, (const uint8_t**)frame->data, frame->nb_samples);
            if (samples > 0)
            {
                bufferQ_->Push(frame);
                av_frame_unref(frame.get());
            }

        }
    }
    if (audioData)
    {
        av_free(audioData);
    }
}
