#pragma once
#include <memory>
#include <shared_mutex>
extern "C" {
#include <libavformat/avformat.h>  
#include <libavcodec/avcodec.h>
#include <libavutil/error.h>
#include <libavutil/audio_fifo.h>
#include <libswresample/swresample.h>
#include <libavdevice/avdevice.h>
}
class AudioBufferQ
{
public:
    AudioBufferQ(AVSampleFormat format, int channels, int sampleRate);
    ~AudioBufferQ();
    
    void Push(std::shared_ptr<AVFrame> frame);

    bool Get(std::shared_ptr<AVFrame> frame, int nbSamples);

    AVSampleFormat GetSampleFormat();
    int GetChannel();
    int GetSampleRate();
private:
    std::shared_ptr<AVAudioFifo> audioFifo_;
    std::shared_mutex mutex_;
    std::once_flag once_;
    uint8_t* inputBuffer_;
    AVSampleFormat format_;
    int channels_;
    int sampleRate_;
    int inputBufferSize_;
};

