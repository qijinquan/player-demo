//

#include <iostream>
#include <iostream> 
#include <string>
#include <thread>
#include <memory>
#include <Windows.h>
#include "AudioBufferQ.h"
#include "AudioInput.h"
#include "AudioSink.h"
#include "AudioPlayerSink.h"
extern "C" {
#include <libavformat/avformat.h>  
#include <libavcodec/avcodec.h>
#include <libavutil/error.h>
#include <libavutil/audio_fifo.h>
#include <libswresample/swresample.h>
#include <libavdevice/avdevice.h>
}

using namespace std;
string UnicodeToUtf8(wstring& unicode)
{
    int size = WideCharToMultiByte(CP_UTF8, 0, unicode.c_str(), unicode.length(), nullptr, 0, nullptr, nullptr);
    char* buf = new char[size + 1];
    WideCharToMultiByte(CP_UTF8, 0, unicode.c_str(), unicode.length(), buf, size, nullptr, nullptr);
    buf[size] = 0;
    string tmp(buf);
    delete[] buf;
    return tmp;
}
int GetSampleFmt(AVCodec* avcodec)
{
    return 0;
}
#ifdef main
#undef main
#endif //main
int main(int argc, char** argv) {
    
    shared_ptr<AudioBufferQ> bufferQ = std::make_shared<AudioBufferQ>(AV_SAMPLE_FMT_S16, 2, 44100);
    
    wstring inFile = L"audio=麦克风 (Realtek(R) Audio)";
    string name = UnicodeToUtf8(inFile);
    
    shared_ptr<AudioInput> audioCapture = make_shared<AudioInput>(bufferQ);
    if (!audioCapture->Init(name))
    {
        cout<<" audio capture init error"<<endl;
        return -1;
    }
    if (!audioCapture->OpenCapture())
    {
        cout << " audio open capture init error" << endl;
        return -1;
    }
    audioCapture->StartCapture();
    
    //shared_ptr<AudioSink> audioSink = make_shared<AudioSink>(bufferQ);
    shared_ptr<AudioPlayerSink> audioSink = make_shared<AudioPlayerSink>(bufferQ);
    std::string outName = "output.wav";
    if (!audioSink->Init(outName))
    {
        cout << " audio sink init error" << endl;
        return -1;
    }
    if (!audioSink->OpenSink())
    {
        cout << " audio sink open error" << endl;
        return -1;
    }
    if (!audioSink->StartPlayer())
    {
        cout << " audio sink player error" << endl;
        audioCapture->StopCapture();
        return -1;
    }
    
    
    this_thread::sleep_for(chrono::seconds(5));
    audioCapture->StopCapture();
    audioCapture = nullptr;
    audioSink->StopPlayer();
    audioSink = nullptr;
    //this_thread::sleep_for(chrono::seconds(5));
    return 0;
}


/*
int main(int argc, char* argv[]) {
    // 打开输入文件  
    AVFormatContext* inputContext = avformat_alloc_context();
    if (avformat_open_input(&inputContext, "input.raw", NULL, NULL) != 0) {
        fprintf(stderr, "无法打开输入文件\n");
        return 1;
    }

    // 获取输入流信息  
    AVCodecParameters* inputCodecParameters = inputContext->streams[0]->codecpar;
    enum AVCodecID inputCodecID = inputContext->streams[0]->codec->codec_id;

    // 创建输出上下文  
    AVFormatContext* outputContext = avformat_alloc_context();
    av_opt_set(outputContext, "avio_flags", "direct", 0);
    if (avio_open(&outputContext->pb, "output.wav", AVIO_FLAG_WRITE) != 0) {
        fprintf(stderr, "无法打开输出文件\n");
        return 1;
    }

    // 设置输出封装格式  
    outputContext->oformat = av_guess_format("wav", NULL, NULL);
    if (!outputContext->oformat) {
        fprintf(stderr, "无法确定输出封装格式\n");
        return 1;
    }

    // 创建输出流信息  
    AVCodecParameters* outputCodecParameters = avcodec_parameters_alloc();
    outputCodecParameters->format = AV_SAMPLE_FMT_S16;
    outputCodecParameters->channels = inputCodecParameters->channels;
    outputCodecParameters->sample_rate = inputCodecParameters->sample_rate;
    outputCodecParameters->codec_id = AV_CODEC_ID_PCM_S16LE;
    outputContext->streams[0] = avformat_new_stream(outputContext, NULL);
    outputContext->streams[0]->codecpar = outputCodecParameters;

    // 创建音频转换器上下文  
    SwrContext* swrContext = swr_alloc();
    swr_set_output_format(swrContext, outputCodecParameters->format, outputCodecParameters->channels, outputCodecParameters->sample_rate);
    swr_init(swrContext);

    // 读取输入数据并转换到输出格式  
    AVPacket inputPacket;
    av_init_packet(&inputPacket);
    uint8_t* inputData[inputCodecParameters->channels];
    int16_t* outputData[inputCodecParameters->channels];
    int outputFrameSize = av_samples_get_buffer_size(NULL, outputCodecParameters->channels, 1, outputCodecParameters->sample_fmt, 0);
    uint8_t* outputBuffer = (uint8_t*)av_malloc(outputFrameSize * sizeof(uint8_t));
    while (av_read_frame(inputContext, &inputPacket) >= 0) {
        int inputFrameSize = av_samples_get_buffer_size(NULL, inputCodecParameters->channels, inputPacket.size / inputCodecParameters->channels, inputCodecParameters->sample_fmt, 1);
        swr_convert(swrContext, &outputData, outputFrameSize, (const uint8_t**)inputData, inputFrameSize);
        av_interleaved_write_frame(outputContext, &outputPacket);
        av_packet_unref(&inputPacket);
    }
    av_packet_unref(&outputPacket);

    // 释放资源并关闭文件  
    swr_free(&swrContext);
    avcodec_parameters_free(&inputCodecParameters);
    avformat_close_input(&inputContext);
    avio_close(&outputContext->pb);
    avformat_free_context(outputContext);
    avfree(outputBuffer);
}
*/

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
