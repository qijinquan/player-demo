#pragma once
#include <memory>
#include <string>
#include <atomic>
#include "AudioBufferQ.h"
extern "C" {
#include <libavformat/avformat.h>  
#include <libavcodec/avcodec.h>
#include <libavutil/error.h>
#include <libswresample/swresample.h>
#include <libavdevice/avdevice.h>
}
class AudioSink : public std::enable_shared_from_this<AudioSink>
{
public:
    AudioSink(std::shared_ptr<AudioBufferQ> bufferQ);
    ~AudioSink();

    bool Init(std::string& output);
    bool OpenSink();
    bool StartPlayer();
    void StopPlayer();
private:
    void SinkTaskRun();
private:
    std::shared_ptr<AVFormatContext> avfCtx_;
    std::shared_ptr<AudioBufferQ> bufferQ_;
    std::shared_ptr<AVCodecContext> avcodecCtx_;
    std::shared_ptr<std::thread> wThread_;
    std::atomic_bool running_;
    std::string output_;
};

