#pragma once
#include <memory>
#include <string>
#include <atomic>
#include "AudioBufferQ.h"
extern "C" {
#include <libavformat/avformat.h>  
#include <libavcodec/avcodec.h>
#include <libavutil/error.h>
#include <libswresample/swresample.h>
#include <libavdevice/avdevice.h>
}
//从麦克风采集语音
class AudioInput : public std::enable_shared_from_this<AudioInput>
{
public:
    AudioInput(std::shared_ptr<AudioBufferQ> bufferQ);
    ~AudioInput();
    
    bool Init(std::string& microephone);
    bool OpenCapture();
    bool StartCapture();
    void StopCapture();
private:
    void CaptureRun();
private:
    std::shared_ptr<AVFormatContext> avfCtx_;
    std::shared_ptr<AudioBufferQ> bufferQ_;
    std::shared_ptr<AVCodecContext> avcodecCtx_;
    std::shared_ptr<std::thread> capThread_;
    int streamIndex_;
    std::atomic_bool running_;
};

