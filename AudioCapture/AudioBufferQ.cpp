#include "AudioBufferQ.h"
using namespace std;
AudioBufferQ::AudioBufferQ(AVSampleFormat format, int channels, int sampleRate):format_(format), channels_(channels), sampleRate_(sampleRate), inputBufferSize_(0)
{
    audioFifo_.reset(av_audio_fifo_alloc(format, channels, sampleRate * 2), [](AVAudioFifo *af) {
        av_audio_fifo_free(af);
    });
}
AudioBufferQ::~AudioBufferQ()
{
    if (inputBuffer_)
    {
        av_free(inputBuffer_);
        inputBuffer_ = nullptr;
    }
    audioFifo_ = nullptr;
}
void AudioBufferQ::Push(std::shared_ptr<AVFrame> frame)
{
    if (frame == nullptr)
    {
        return;
    }
    call_once(once_, [this, frame]() {
        inputBufferSize_ = av_samples_get_buffer_size(NULL, channels_, frame->nb_samples, format_, 1);
        inputBuffer_ = (uint8_t*)av_malloc(inputBufferSize_);
        /*
        inputBuffer_.reset(av_buffer_alloc(inputBufferSize_), [](AVBufferRef* ref){
            av_buffer_unref(&ref);
        });
        */
    });
    lock_guard<shared_mutex> lk(mutex_);
    if (av_audio_fifo_space(audioFifo_.get()) < frame->nb_samples)
    {
        av_audio_fifo_read(audioFifo_.get(), (void**)&inputBuffer_, frame->nb_samples);
    }
    av_audio_fifo_write(audioFifo_.get(), (void**)frame->data, frame->nb_samples);
}

bool AudioBufferQ::Get(std::shared_ptr<AVFrame> frame, int nbSamples)
{
    lock_guard<shared_mutex> lk(mutex_);
    if (av_audio_fifo_size(audioFifo_.get()) >= nbSamples)
    {
        auto samples = av_audio_fifo_read(audioFifo_.get(), (void**)frame->data, nbSamples);
        frame->channels = channels_;
        frame->channel_layout = av_get_default_channel_layout(channels_);
        frame->format = format_;
        return true;
    }
    return false;
}
AVSampleFormat AudioBufferQ::GetSampleFormat()
{
    return format_;
}
int AudioBufferQ::GetChannel()
{
    return channels_;
}
int AudioBufferQ::GetSampleRate()
{
    return sampleRate_;
}