#include "AudioSink.h"
#include <iostream>
using namespace std;
AudioSink::AudioSink(std::shared_ptr<AudioBufferQ> bufferQ) : bufferQ_(std::move(bufferQ)),running_(false), output_("")
{

}
AudioSink::~AudioSink()
{
    if (avcodecCtx_)
    {
        avcodecCtx_ = nullptr;
    }
    avfCtx_ = nullptr;
}
bool AudioSink::Init(std::string& output)
{
    output_ = output;
    AVFormatContext* outfCtx = NULL;
    auto err = avformat_alloc_output_context2(&outfCtx, NULL, NULL, output.c_str());
    if (err < 0)
    {
        cout<<"avformat_alloc_output_context2 err"<<endl;
        return false;
    }
    avfCtx_.reset(outfCtx, [](AVFormatContext* ctx){
        avformat_free_context(ctx);
    });
    avformat_new_stream(avfCtx_.get(), NULL);
    //编码器参数设置
    AVCodecParameters* outputParameters = avfCtx_->streams[0]->codecpar;
    outputParameters->codec_id = AV_CODEC_ID_PCM_S16LE; // 65536,TODO:
    outputParameters->codec_type = AVMEDIA_TYPE_AUDIO;
    outputParameters->sample_rate = bufferQ_->GetSampleRate();
    outputParameters->channels = bufferQ_->GetChannel();
    outputParameters->channel_layout = av_get_default_channel_layout(outputParameters->channels);
    outputParameters->bits_per_coded_sample = 16;
    outputParameters->format = bufferQ_->GetSampleFormat();
    return true;
}

bool AudioSink::OpenSink()
{
    //找到编码器
    AVCodec* outputCodec = avcodec_find_encoder(avfCtx_->streams[0]->codecpar->codec_id);
    avcodecCtx_.reset(avcodec_alloc_context3(outputCodec), [](AVCodecContext* ctx) {
        avcodec_free_context(&ctx);
    });
    avcodec_parameters_to_context(avcodecCtx_.get(), avfCtx_->streams[0]->codecpar);
    //打开编码器
    auto err = avcodec_open2(avcodecCtx_.get(), outputCodec, NULL);
    if (err != 0)
    {
        cout << "avcodec_open2 err:" << endl;
        return false;
    }
    AVIOContext* avioCtx = NULL;
    err = avio_open2(&avioCtx, output_.c_str(), AVIO_FLAG_WRITE, NULL, NULL);
    avfCtx_->pb = avioCtx;
    return true;
}

bool AudioSink::StartPlayer()
{
    if (running_.load())
    {
        return true;
    }

    wThread_ = std::make_shared<thread>(std::bind(&AudioSink::SinkTaskRun, this));
    return true;
}

void AudioSink::StopPlayer()
{
    running_.store(false);
    if (wThread_&& wThread_->joinable())
    {
        wThread_->join();
    }
    cout<<"audio sink end."<<endl;
}

void AudioSink::SinkTaskRun()
{
    running_.store(true);
    // 写入文件头
    if (avformat_write_header(avfCtx_.get(), NULL) != 0) {
        std::cerr << "Failed to write file header." << std::endl;
        return;
    }
    //获取采样数,20ms的采样
    int nbSamples = bufferQ_->GetSampleRate() / 50;
    int size = av_samples_get_buffer_size(NULL, bufferQ_->GetChannel(), nbSamples, bufferQ_->GetSampleFormat(), 1);
    uint8_t* outBuf = (uint8_t*)av_malloc(size);
    shared_ptr<AVFrame> frame(av_frame_alloc(), [](AVFrame* frame) {
        av_frame_free(&frame);
    });
    // must set this 
    frame->nb_samples = nbSamples;
    auto err = avcodec_fill_audio_frame(frame.get(), bufferQ_->GetChannel(), bufferQ_->GetSampleFormat(), outBuf, size, 1);
    while (running_.load())
    {
        if (bufferQ_->Get(frame, nbSamples))
        {
            avio_write(avfCtx_->pb, outBuf, size);
        }
    }
    av_free(outBuf);
    av_write_trailer(avfCtx_.get());
}
