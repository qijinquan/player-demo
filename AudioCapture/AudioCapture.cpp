﻿//

#include <iostream>
#include <iostream> 
#include <string>
#include <thread>
#include <Windows.h>
#include <mutex>
extern "C" {
#include <libavformat/avformat.h>  
#include <libavcodec/avcodec.h>
#include <libavutil/error.h>
#include <libavutil/audio_fifo.h>
#include <libswresample/swresample.h>
#include <libavdevice/avdevice.h>
}

using namespace std;
string UnicodeToUtf8(wstring& unicode)
{
    int size = WideCharToMultiByte(CP_UTF8, 0, unicode.c_str(), unicode.length(), nullptr, 0, nullptr, nullptr);
    char* buf = new char[size+1];
    WideCharToMultiByte(CP_UTF8, 0, unicode.c_str(), unicode.length(), buf, size, nullptr, nullptr);
    buf[size] = 0;
    string tmp(buf);
    delete[] buf;
    return tmp;
}
int GetSampleFmt(AVCodec* avcodec)
{
    return 0;
}
AVAudioFifo* g_audioFifo = NULL;
mutex g_mutex;
int main2(int argc, char** argv) {
    // 打开音频设备
    avdevice_register_all();
    AVInputFormat* inputFormat = av_find_input_format("dshow");

    AVFormatContext* formatCtx = avformat_alloc_context();
    AVDictionary* options = nullptr;
    
    wstring inFile = L"audio=麦克风 (Realtek(R) Audio)";
    string name = UnicodeToUtf8(inFile);
    auto err = avformat_open_input(&formatCtx, name.c_str(), inputFormat, &options);
    if (err != 0) {
        char buf[256];
        av_make_error_string(buf, 256, err);
        std::cerr << "Failed to open input:"<< buf << std::endl;
        return -1;
    }
    int audioIndex = -1;
    err = avformat_find_stream_info(formatCtx, NULL);
    for (int i = 0; i < formatCtx->nb_streams; i++)
    {
        if (formatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            audioIndex = i;
            break;
        }
    }
    if (audioIndex == -1)
    {
        std::cerr << "Can not find audio stream" << std::endl;
        return -1;
    }
    // 打开解码器
    AVCodec* codec = avcodec_find_decoder(formatCtx->streams[audioIndex]->codecpar->codec_id);
    AVCodecContext* codecCtx = avcodec_alloc_context3(codec);
    //将解码器参数保存到Context中
    avcodec_parameters_to_context(codecCtx, formatCtx->streams[audioIndex]->codecpar);


    // TODO，这个版本，有些参数没有设置好,但能打开组件，奇怪？？
    //codecCtx->sample_fmt = (AVSampleFormat)formatCtx->streams[audioIndex]->codecpar->format;
    codecCtx->channels = formatCtx->streams[audioIndex]->codecpar->channels;
    codecCtx->sample_rate = formatCtx->streams[audioIndex]->codecpar->sample_rate;

    err = avcodec_open2(codecCtx, codec, nullptr);
    if (err < 0){
        char buf[256];
        av_make_error_string(buf, 256, err);
        std::cerr << "Can not open codec" << buf << std::endl;
        return -1;
    }
    // 这里设置了2s的语音保存
    g_audioFifo = av_audio_fifo_alloc((AVSampleFormat)formatCtx->streams[audioIndex]->codecpar->format, codecCtx->channels, codecCtx->sample_rate * 2);


    // 采集音频数据
    std::thread audioCapture([codecCtx, formatCtx, audioIndex]{
        AVPacket pkt;
        av_init_packet(&pkt);
        AVFrame* frame = av_frame_alloc();
        
        int gotFrame;
        while (true)
        {
            if (av_read_frame(formatCtx, &pkt) < 0)
            {
                continue;
            }
            if (avcodec_send_packet(codecCtx, &pkt) < 0)
            {
                av_packet_unref(&pkt);
                continue;
            }
            if (avcodec_receive_frame(codecCtx, frame) < 0)
            {
                av_packet_unref(&pkt);
                continue;
            }
            av_packet_unref(&pkt);
            //TODO:将采集到的数据保存在fifo中
            lock_guard<mutex> lk(g_mutex);
            while (av_audio_fifo_space(g_audioFifo) <  frame->nb_samples)
            {
                // 把这些sample读出来，会不会内存泄漏？
                int size = av_samples_get_buffer_size(NULL, codecCtx->channels, frame->nb_samples, (AVSampleFormat)frame->format, 1);
                static AVBufferRef* buf = av_buffer_alloc(size);
                av_audio_fifo_read(g_audioFifo, (void**)&buf->data, frame->nb_samples);
            }
            av_audio_fifo_write(g_audioFifo, (void**)frame->data, frame->nb_samples);
            av_frame_unref(frame);
        }
        av_frame_free(&frame);
        return;
    });

    {
        AVFormatContext* output_format_context2 = NULL;
        err = avformat_alloc_output_context2(&output_format_context2, NULL, NULL, "output.wav");
        avformat_new_stream(output_format_context2, NULL);
        AVCodecParameters* outputParameters = output_format_context2->streams[0]->codecpar;
        outputParameters->codec_id = AV_CODEC_ID_PCM_S16LE;
        outputParameters->codec_type = AVMEDIA_TYPE_AUDIO;
        outputParameters->sample_rate = 44100;
        outputParameters->channels = 2;
        outputParameters->channel_layout = av_get_default_channel_layout(outputParameters->channels);
        outputParameters->bits_per_coded_sample = 16;
        outputParameters->format = AV_SAMPLE_FMT_S16;
        AVCodec* outputCodec = avcodec_find_encoder(outputParameters->codec_id);
        AVCodecContext* avCodecCtx = avcodec_alloc_context3(outputCodec);
        avcodec_parameters_to_context(avCodecCtx, output_format_context2->streams[0]->codecpar);
        err = avcodec_open2(avCodecCtx, outputCodec, NULL);
        AVIOContext* avioCtx = NULL;
        err = avio_open2(&avioCtx, "output.wav", AVIO_FLAG_WRITE, NULL, NULL);
        output_format_context2->pb = avioCtx;
        // 写入文件头  
        if (avformat_write_header(output_format_context2, NULL) != 0) {
            std::cerr << "Failed to write file header." << std::endl;
            return 1;
        }
        
        
        thread oThread([avCodecCtx, output_format_context2, outputParameters]{
            //获取采样数,20ms的采样
            int nbSamples = outputParameters->sample_rate / 50;
            int size = av_samples_get_buffer_size(NULL, outputParameters->channels, nbSamples, (AVSampleFormat)outputParameters->format, 1);
            AVBufferRef* outBuf = av_buffer_alloc(size);
            AVFrame* frame = av_frame_alloc();
            frame->format = outputParameters->format;
            frame->channels = 2;
            avcodec_fill_audio_frame(frame, outputParameters->channels, (AVSampleFormat)outputParameters->format, outBuf->data, outBuf->size, 1);
            int m = 500;
            while (true)
            {
                if (av_audio_fifo_space(g_audioFifo) >= nbSamples)
                {
                    lock_guard<mutex> lk(g_mutex);
                    int samples = av_audio_fifo_read(g_audioFifo, (void**)&outBuf->data, nbSamples);
                    cout<<"read sample = "<< samples<<endl;
                    //准备写入文件
                    if (true)
                    {
                        int size = av_samples_get_buffer_size(NULL, outputParameters->channels, nbSamples, (AVSampleFormat)outputParameters->format, 1);
                        avio_write(output_format_context2->pb, outBuf->data, size);
                    }
                    m--;
                    if (m == 0)
                    {
                        break;
                    }
                }
            }
            cout << "read trailer " << endl;
            av_write_trailer(output_format_context2);
        });
        oThread.join();
    }
    audioCapture.join();

    /*
    // 创建音频帧  
    AVFrame* frame = av_frame_alloc();
    frame->format = codecCtx->sample_fmt;
    frame->channel_layout = av_get_default_channel_layout(codecCtx->channels);
    frame->channels = codecCtx->channels;
    frame->sample_rate = codecCtx->sample_rate;
    frame->nb_samples = codecCtx->frame_size;
    frame->channel_layout = codecCtx->channel_layout;
    if (avcodec_fill_audio_frame(frame, -1, codecCtx->sample_fmt, (uint8_t*)frame->data, (uint8_t*)frame->linesize, frame->nb_samples) < 0) {
        std::cerr << "Failed to fill audio frame" << std::endl;
        return -1;
    }

    // 创建输出格式上下文  
    AVFormatContext* outputFormatCtx = avformat_alloc_context();
    AVCodecID;
    AVCodec* outputCodec = avcodec_find_encoder(AV_CODEC_ID_PCM_S16LE);
    avformat_alloc_output_context2(outputFormatCtx, outputCodec, nullptr, "output.wav");
    AVIOContext* ioctx = avio_alloc_context((uint8_t*)frame->data, frame->linesize * frame->nb_samples, 1, "output.wav", nullptr);
    outputFormatCtx->pb = ioctx;

    // 写入帧数据到输出文件  
    AVPacket pkt;
    avcodec_get_frame(codecCtx, frame); // 创建一个新的帧来存储音频数据  
    pkt.data = (uint8_t*)frame->data; // 设置数据指针为录音缓冲区的数据指针  
    pkt.size = frame->nb_samples * frame->channels * frame->format / 8; // 设置数据大小为录音缓冲区的字节数  
    av_interleaved_write_frame(outputFormatCtx, &pkt); // 将数据写入输出文件并释放包数据  
    avcodec_free_frame(&frame); // 释放音频帧内存。  
    avcodec_free_context(&codecCtx); // 释放编解码器上下文。  
    avformat_close_input(&formatCtx); // 关闭输入格式上下文。  
    avformat_free_context(outputFormatCtx); // 释放输出格式上下文。  
    avio_freep(outputFormatCtx->pb); // 释放IO上下文。  
    */
    return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
