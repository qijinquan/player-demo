﻿// mpeg4Ana.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <fstream>
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")
#define VISOBJSEQ_START_CODE 0xB0
#define VISOBJ_START_CODE 0xB5

using namespace std;
//mpeg2 码流查找，找到00 00 01 B3(SEQ HEADER) 或者00 00 01 00(Frame HEADER),到下一个开头，如果前面有多余的，则是其它需要的信息

//H263 码流查找 & 0x00FFFF80(H263_STARTCODE_MASK) = 0x80 且 &0x0000007C(H263_GOB_ZERO_MASK) = 0x0; 到下一个开始
bool ReadOnePacket(ifstream& is, char* buf, uint32_t& filledLen) {
    char* temp = buf;
    is.read(temp, 3);
    temp+=3;
    if (is.eof())
    {
        filledLen = 0;
        return true;
    }
    bool findVop = false;
    int count = 3;
    while (!is.eof())
    {
        is.read(temp, 1);
        count++;
        if ((temp[0] == (char)0xB6) && (temp[-1] == 0x1) && (temp[-2]==0x0) && (temp[-3] == 0x0))// find the VOP start
        {
            findVop = true;
        }
        
        if (findVop && *temp == 0x1 && temp[-1] == 0x0 && temp[-2] == 0x0) // find the VOP end
        {
            temp -= 3-1; // 计算长度是要加1，所以这里直接少减1
            is.seekg(-3, ios::cur);//seek 到能正常读取0x000001
            break;
        }
        temp++;
    }
    filledLen = (temp - buf);
    return false;
}
int main()
{
    ifstream is("720x480.mpeg4", ios::binary);
    char c = 0;
    char buf[720 * 480 * 2] = { 0 };
    uint32_t Len = 0;
    int count = 0;
    while (!ReadOnePacket(is, buf, Len))
    {
        memset(buf, 0, sizeof(buf));
        cout<<"Read Len = "<<Len<<endl;
        count++;
    }
    /*
    uint32_t header = 0;
    is.read((char*)&header, 4);
    header = ntohl(header);
    if ((header>>8) == 0x1)
    {
        std::cout << "Not find startCode"<<endl;
        return 0;
    }
    switch (header&0xFF)
    {
    case 0x20://MPEG4_VIDOBJLAY_START_CODE
        break;
    case 0x2F:// MPEG4_VIDOBJLAYER_STOP_CODE
        break;
    case VISOBJSEQ_START_CODE://B0 后面1位数据，定义Profile Level 与Level
        break;
    case VISOBJ_START_CODE://B5 视频对象开始，1位数据，版本、类型、信号类型
        break;
    case 0xB3:
        break;
    case 0xB5:
        break;
    case 0xB6:
        break;
    }
    */
    std::cout << "Hello World! count= "<<count;
    return 0;
}

