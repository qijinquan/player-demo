#pragma once
#include <cinttypes>
#include <vector>
#include <iostream>
#include "StreamReader.h"
#define FOURCC(id) (((uint32_t)(id[0])<<24) | (id[1]<<16) | (id[2]<<8) | (id[3]))
class BoxHeader
{
public:
    BoxHeader() = default;
    //BoxHeader() = default;
    virtual ~BoxHeader() = default;

    uint64_t GetBoxSize() const;
    uint32_t GetHeaderSize() const;
    uint32_t GetShortType() const;
    std::vector<uint8_t> GetType() const;
    std::string GetTypeString() const;
    void SetShortType(uint32_t type);
    bool Parse(BitsStreamRange& range);
    void ParseFullBoxHeader(BitsStreamRange& range);
    uint8_t GetVersion() const
    {
        return version_;
    }

    void SetVersion(uint8_t version)
    {
        version_ = version;
    }
 private:
    uint64_t size_ = 0;
    uint32_t headerSize_ = 0;
    uint32_t type_ = 0;
    std::vector<uint8_t> uuidType;
    uint32_t flags = 0;
    uint8_t version_ = 0;
    bool fullBox = false;
};
class Box : public BoxHeader
{
public:
    Box(){}
    Box(const BoxHeader& hdr):BoxHeader(hdr){}
    static bool Read(BitsStreamRange& range, std::shared_ptr<Box>& box);
    //virtual bool Write(StreamWriter& writer);
    virtual void derive_box_version()
    {
        SetVersion(0);
    }
protected:
    virtual bool Parse(BitsStreamRange& range);
    bool ReadChildRen(BitsStreamRange& range, int number = -1);

private:
    std::vector<std::shared_ptr<Box>> children_;
};

class BoxFtyp : public Box
{
public:
    BoxFtyp() {
        SetShortType(FOURCC("ftyp"));
        //full box false
    }
    BoxFtyp(const BoxHeader& hdr) : Box(hdr)
    {
            
    }
    bool HasBrand(uint32_t brand) const;
protected:
    bool Parse(BitsStreamRange& range);
private:
    uint32_t major_ = 0;
    uint32_t minor_ = 0;
    std::vector<uint32_t> brands_;

};
class BoxMeta : public Box
{
public:
    BoxMeta() {
        SetShortType(FOURCC("meta"));
        //full box
    }
    BoxMeta(const BoxHeader& hdr) : Box(hdr)
    {

    }
protected:
    bool Parse(BitsStreamRange& range);
};
class BoxHdlr : public Box
{
public:
    BoxHdlr() {
        SetShortType(FOURCC("hdlr"));
        //full box
    }
    BoxHdlr(const BoxHeader& hdr) : Box(hdr)
    {

    }
    uint32_t GetHandlerType() const;
protected:
    bool Parse(BitsStreamRange& range);
private:
    uint32_t preDefined_ = 0;
    uint32_t handleType_ = FOURCC("pict");//表示的是图片,类似与ios mpeg4视频的track
    uint32_t reserved_[3] = {0,};
    std::string name_;
};

class BoxPitm : public Box
{
public:
    BoxPitm()
    {
        SetShortType(FOURCC("pitm"));
        // full box
    }

    BoxPitm(const BoxHeader& hdr) : Box(hdr)
    {}

    uint32_t GetItemId() const
    {
        return itemId_;
    }

protected:
    bool Parse(BitsStreamRange& range);

private:
    uint32_t itemId_ = 0;
};

class BoxIloc : public Box
{
public:
    BoxIloc()
    {
        SetShortType(FOURCC("iloc"));
        // full box
    }

    BoxIloc(const BoxHeader& hdr) : Box(hdr)
    {}

    struct Extent
    {
        uint64_t index = 0;
        uint64_t offset = 0;
        uint64_t length = 0;
        std::vector<uint8_t> data;
    };

    struct Item
    {
        uint32_t itemId = 0;
        uint8_t method = 0;
        uint8_t dataRefIndex = 0;
        uint8_t baseOffset = 0;
        std::vector<Extent> extents;
    };
protected:
    bool Parse(BitsStreamRange& range);

private:
    uint8_t userDefMinVersion = 0;
    uint8_t offsetSize_ = 0;
    uint8_t lengthSize_ = 0;
    uint8_t baseOffsetSize_ = 0;
    uint8_t indexSize_ = 0;
    int idatOffset_ = 0; // for write
    std::vector<Item> items_;
    mutable size_t ilocBoxStart_ = 0;
};