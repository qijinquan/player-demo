#include "Box.h"
#include <sstream>
#include <algorithm>
#include <iomanip>
using namespace std;
#define ERROR(msg) cout<<__func__<<":"<<msg<<endl


static inline std::string to_fourcc(uint32_t code)
{
    std::string str("    ");
    str[0] = static_cast<char>((code >> 24) & 0xFF);
    str[1] = static_cast<char>((code >> 16) & 0xFF);
    str[2] = static_cast<char>((code >> 8) & 0xFF);
    str[3] = static_cast<char>((code >> 0) & 0xFF);
    return str;
}

uint64_t BoxHeader::GetBoxSize() const
{
    return size_;
}

uint32_t BoxHeader::GetHeaderSize() const
{
    return headerSize_;
}

uint32_t BoxHeader::GetShortType() const
{
    return type_;
}

std::vector<uint8_t> BoxHeader::GetType() const
{
    std::vector<uint8_t> types;
    types.resize(4);
    memcpy(types.data(), &type_, sizeof(type_));
    return types;
}

std::string BoxHeader::GetTypeString() const
{
    if (type_ == FOURCC("uuid")) {
        // 8-4-4-4-12
        std::ostringstream sstr;
        sstr << std::hex;
        sstr << std::setfill('0');
        sstr << std::setw(2);

        for (int i = 0; i < 16; i++) {
            if (i == 4 || i == 6 || i == 8 || i == 10) {
                sstr << '-';
            }

            sstr << ((int)uuidType[i]);
        }

        return sstr.str();
    }
    else {
        return to_fourcc(type_);
    }
}

void BoxHeader::SetShortType(uint32_t type)
{
    type_ = type;
}

bool BoxHeader::Parse(BitsStreamRange& range)
{
    auto status = range.WaitForAvaliableBytes(8);
    if (status != StreamReader::GrowStatus::SIZE_READCHED)
    {
        ERROR("WaitForAvaliableBytes error");
        return false;
    }
    size_ = range.ReadUInt();
    type_ = range.ReadUInt();
    headerSize_ = 8;
    if (size_ == 1)
    {
        status = range.WaitForAvaliableBytes(8);
        if (status != StreamReader::GrowStatus::SIZE_READCHED)
        {
            ERROR("WaitForAvaliableBytes");
            return false;
        }
        uint64_t high = range.ReadUInt();
        uint64_t low = range.ReadUInt();
        size_ = (high<<32) | low;
        headerSize_ += 8;
    }
    if (type_ == FOURCC("uuid"))
    {
        status = range.WaitForAvaliableBytes(16);
        //range.PrepareRead(16);
        uuidType.resize(16);
        range.Read(uuidType.data(), 16);
        headerSize_ += 16;
    }
    return true;
}

void BoxHeader::ParseFullBoxHeader(BitsStreamRange& range)
{
    uint32_t data = range.ReadUInt();
    version_ = static_cast<uint8_t>(data>>24);
    flags    = data & 0x00FFFFFF;
    fullBox  = true;
    headerSize_ += 4;
}

bool Box::Read(BitsStreamRange & range, std::shared_ptr<Box>& box)
{
    BoxHeader hdr;
    if (!hdr.Parse(range))
    {
        return false;
        ERROR("header parse error");
    }
    shared_ptr<Box> tmpBox = std::make_shared<Box>(hdr);
    switch (hdr.GetShortType())
    {
    case FOURCC("ftyp"):
        tmpBox = std::make_shared<BoxFtyp>(hdr);
        break;
    case FOURCC("meta"):// media data, 包含了hdlr,pitm,iloc,iinf,iref,iprp
        tmpBox = std::make_shared<BoxMeta>(hdr);
        break;
    case FOURCC("hdlr"):
        tmpBox = std::make_shared<BoxHdlr>(hdr);
        break;
    case FOURCC("pitm"):
        break;
    case FOURCC("iloc"):
        break;
    case FOURCC("iinf"):
        break;
    case FOURCC("infe"):
        break;
    case FOURCC("iprp"):
        break;
    case FOURCC("ipco"):
        break;
    case FOURCC("ipma"):
        break;
    case FOURCC("ispe"):
        break;
    case FOURCC("auxC"):
        break;
    case FOURCC("irot"):
        break;
    case FOURCC("imir"):
        break;
    case FOURCC("iref"):
        break;
    case FOURCC("clap"):
        break;
    case FOURCC("hvcC"):
        break;
    case FOURCC("av1C"):
        break;
    case FOURCC("idat"):
        break;
    case FOURCC("grpl"):
        break;
    case FOURCC("dinf"):
        break;
    case FOURCC("dref"):
        break;
    case FOURCC("url"):
        break;
    case FOURCC("colr"):
        break;
    case FOURCC("pixi"):
        break;
    default:
        break;
    }
    cout<<hdr.GetTypeString().c_str()<<endl;
    uint64_t boxSizeNoH = hdr.GetBoxSize() - hdr.GetHeaderSize();
    auto status = range.WaitForAvaliableBytes(boxSizeNoH);
    if (status != StreamReader::GrowStatus::SIZE_READCHED)
    {
        ERROR("WaitForAvaliableBytes ret error");
        return false;
    }
    box = std::move(tmpBox);
    
    BitsStreamRange boxRange(range.GetStreamer(), boxSizeNoH, &range);
    box->Parse(boxRange);
    //boxRange.SkipToEndOfBox();
    return true;
}

bool Box::Parse(BitsStreamRange & range)
{
    if (GetBoxSize() == 0)
    {
        range.SkipToEndOfFile();
    }
    else
    {
        uint64_t contentSize = GetBoxSize() - GetHeaderSize();
        range.PrepareRead(contentSize);
        range.GetStreamer()->SeekCur(contentSize);
    }
    return false;
}

bool Box::ReadChildRen(BitsStreamRange & range, int number)
{
    int count = 0;
    while (!range.Eof())
    {
        std::shared_ptr<Box> box;
        if (!Box::Read(range, box))
        {
            break;
        }
        children_.push_back(std::move(box));
        count++;
    }
    //todo number
    return true;
}

bool BoxFtyp::HasBrand(uint32_t brand) const
{
    for (size_t i = 0; i < brands_.size(); i++)
    {
        if (brands_[i] == brand)
        {
            return true;
        }
    }
    return false;
}

bool BoxFtyp::Parse(BitsStreamRange & range)
{
    major_ = range.ReadUInt();
    minor_ = range.ReadUInt();
    //check size TODO
    uint32_t brandCount = (GetBoxSize() - GetHeaderSize() - 8)/4;
    for (uint32_t i = 0; i < brandCount; i++)
    {
        brands_.push_back(range.ReadUInt());
    }
    return true;
}

bool BoxMeta::Parse(BitsStreamRange & range)
{
    ParseFullBoxHeader(range);// 先读full
    return ReadChildRen(range);
}

uint32_t BoxHdlr::GetHandlerType() const
{
    return handleType_;
}

bool BoxHdlr::Parse(BitsStreamRange & range)
{
    ParseFullBoxHeader(range);// 先读full
    preDefined_ = range.ReadUInt();
    handleType_ = range.ReadUInt();
    for (size_t i = 0; i < 3; i++)
    {
        reserved_[i] = range.ReadUInt();
    }
    name_ = range.ReadStr();
    return false;
}

bool BoxPitm::Parse(BitsStreamRange & range)
{
    ParseFullBoxHeader(range);// 先读full
    if (GetVersion() == 0) {
        itemId_ = range.ReadUShort();
    }
    else {
        itemId_ = range.ReadUInt();
    }
    return true;
}

bool BoxIloc::Parse(BitsStreamRange & range)
{
    ParseFullBoxHeader(range);
    uint16_t vals = range.ReadUShort();
    int offsetSize = (vals >> 12 ) &0xF;
    int lengthSize = (vals >> 8) & 0xF;
    int baseOffSetSize = (vals >> 4) & 0xF;
    int indexSize = 0;
    if (GetVersion() > 1)
    {
        indexSize = (vals & 0xF);
    }

    return false;
}
